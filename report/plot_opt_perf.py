#!/usr/bin/env python
#
# Program which plots relative performance of programs against one another, with
# different optimizations applied, as a bar graph

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import re
import sys
import math
from procsim_data import *


def printBadUsage():
    print("ERROR BAD USAGE");
    print(
        "%s <OUTPUT_FILE> <PROG_A> <PROG_B> ... '---' <SERIES_NAME, STAT_PATTERN_A> <SERIES_NAME, STAT_PATTERN_B> ..." %
        sys.argv[0]
    )
    sys.exit(1)

if len(sys.argv) < 5:
    printBadUsage()

FILE_NAME     = sys.argv[1]
PROGRAM_NAMES = []
OPTIMIZATIONS = []

argv_idx = 2

while argv_idx < len(sys.argv):
    val = sys.argv[argv_idx]
    argv_idx += 1
    if val == '---':
        break
    PROGRAM_NAMES.append(val)
while argv_idx < len(sys.argv):
    val = sys.argv[argv_idx].split(',')
    OPTIMIZATIONS.append((val[0].strip(), val[1].strip()))
    argv_idx += 1

if len(PROGRAM_NAMES) * len(OPTIMIZATIONS) == 0:
    printBadUsage()

print(PROGRAM_NAMES)
print(OPTIMIZATIONS)

#PROGRAM_NAMES = [
#    'optimal2.exe',
#    'optimal4.exe',
#    'optimal6.exe',
#]
#
#OPTIMIZATIONS = [
#    ('None',             '6_96_32_6_64_0_0_0_0'),
#    ('Rename',           '6_96_32_6_64_1_0_0_0'),
#    ('OO Exec',          '6_96_32_6_64_2_0_0_0'),
#    ('OO Exec + Rename', '6_96_32_6_64_3_0_0_0'),
#    ('All',              '6_96_32_6_64_3_1_1_3'),
    #]

###############################################################
# Load Data
raw_data = [ [ [] for o in OPTIMIZATIONS  ] for p in PROGRAM_NAMES ]
data = [ [ [] for o in OPTIMIZATIONS  ] for p in PROGRAM_NAMES ]
for prog_idx, prog in enumerate(PROGRAM_NAMES):
    for opt_idx, opt in enumerate(OPTIMIZATIONS):
        stats = loadProgramStatFile(prog, opt[1])
        raw_data[prog_idx][opt_idx] = stats
        data[prog_idx][opt_idx] = stats['port_utalization']
###############################################################



###############################################################
# Calculate graph wide data
max_stack_count = max([ max([ len(d) for d in p ]) for p in data ])
prog_max_ticks  = [ max([ sum(d) for d in p ]) for p in data ]
prog_throughput = [
    [ raw_data[p][i]['instruction_exec_throughput'] for i in range(len(raw_data[p])) ]
    for p in range(len(data))
]
###############################################################



###############################################################
# Create Figure
fig_size = (20.0/2.5,9.0/2.5)
if len(PROGRAM_NAMES) == 1:
    fig_size = (16.0/2.5,10.0/2.5)
fig, axes = plt.subplots(1, len(PROGRAM_NAMES), figsize=fig_size)
legend_entries = None
###############################################################



###############################################################
# Plot data
for prog_idx, prog_name in enumerate(PROGRAM_NAMES):
    opt_colors = plt.cm.rainbow(np.linspace(0,1,len(OPTIMIZATIONS)))

    prog_name = prettifyProgramName(prog_name)

    ax = plt.subplot(1, len(PROGRAM_NAMES), prog_idx+1)
    plt.title(prog_name)

    xs = range(len(OPTIMIZATIONS))
    if len(PROGRAM_NAMES) > 1:
        plt.xticks(xs, [ '' for _ in OPTIMIZATIONS])
    else:
        plt.xticks(xs, [ opt_name for (opt_name, _) in OPTIMIZATIONS], rotation=90)

    plt.ylim((0, prog_max_ticks[prog_idx] * 1.15))
    plt.yticks([], [''])

    prog_data = data[prog_idx]

    bottoms = np.zeros(len(prog_data))

    for stack_idx in range(max_stack_count):
        colors = opt_colors.copy()
        for i in range(len(colors)):
            colors[i][3] *= ((float(max_stack_count - stack_idx) / (float(max_stack_count)))+0.5)/1.5

        stack_data = [
            prog_data[i][stack_idx] if i < len(prog_data) else 0
            for i in range(len(OPTIMIZATIONS))
        ]
        bars = plt.bar(xs, stack_data,
                color = colors,
                bottom = bottoms,
                edgecolor = 'black',
        )
        bottoms += stack_data

        if stack_idx == 0:
            legend_entries = bars.patches

        for bar in bars:
            cx = bar.get_x() + 0.5 * bar.get_width()
            cy = bar.get_y() + 0.5 * bar.get_height()
            if float(bar.get_height()) / float(prog_max_ticks[prog_idx]) > 0.03:
                plt.text(cx, cy, str(stack_idx),
                         horizontalalignment='center',
                         verticalalignment='center')

    for opt_idx in range(len(OPTIMIZATIONS)):
        plt.text(
            opt_idx, bottoms[opt_idx] + 0.1* prog_max_ticks[prog_idx],
            str(int(bottoms[opt_idx])),
            horizontalalignment='center',
            verticalalignment='center'
        )

        plt.text(
            opt_idx, bottoms[opt_idx] + 0.05* prog_max_ticks[prog_idx],
            "%.2f" % prog_throughput[prog_idx][opt_idx],
            horizontalalignment='center',
            verticalalignment='center'
        )
###############################################################



###############################################################
# Add legend (if multiple subplots)
if len(PROGRAM_NAMES) > 1:
    legend_ncols  = 5
    legend_nrows  = math.ceil(float(len(OPTIMIZATIONS)) / legend_ncols)

    fig.subplots_adjust(top = 0.93,
                        left = 0.03,
                        right = 0.97,
                        bottom=0.05 + 0.05*legend_nrows,
                        wspace=0.05)

    if legend_nrows > 1:
        legend_order  = [ int((i * legend_ncols) % len(OPTIMIZATIONS)) for i in range(len(OPTIMIZATIONS)) ]
    else:
        legend_order  = range(len(OPTIMIZATIONS))
    plt.figlegend(
        [ legend_entries[i]    for i in legend_order ],
        [ OPTIMIZATIONS [i][0] for i in legend_order ],
        'lower center',
        ncol = legend_ncols,
    )
else:
    plt.tight_layout()
###############################################################



###############################################################
# Save File
containing_dir = os.path.dirname(FILE_NAME)
if not os.path.exists(containing_dir):
    os.makedirs(containing_dir)
plt.savefig(FILE_NAME)
###############################################################
