all: experiment_dispatch_window
experiment_dispatch_window: \
	$$(call zipUnderscore,figures/dispatch_window/ports,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/dispatch_window/ticks,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/dispatch_window/ticks_stacked,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/dispatch_window/waste_ratio,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/dispatch_window/throughput,$(PROGS_SVG)) \
	figures/dispatch_window/ticks_all.svg

genDispatchWindowDeps = $(call genStatFileNames,\
	$(1),\
	6,\
	512,\
	32,\
	6,\
	6 7 8 9 10 16 20 24 28 32 36 40 44 48 52 56 60 64 80 96 112 128 144 160 172,\
	3,\
	1,\
	1,\
	3\
)

figures/dispatch_window/ticks_%.svg: $$(call genDispatchWindowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_512_32_6_*_3_1_1_3" \
		"Ticks Elapsed vs Dispatch Window Size - $*.exe" \
		"ticks" \
		$@

figures/dispatch_window/ticks_stacked_%.svg: $$(call genDispatchWindowDeps,%.exe)
	./plot_stacked_line.py \
		"$*.exe" "6_512_32_6_*_3_1_1_3" \
		"Ticks Elapsed vs Dispatch Window Size - $*.exe" \
		$@

figures/dispatch_window/ticks_all.svg: $$(call genDispatchWindowDeps,$(PROGS_EXE))
	./plot_graphs.py \
		"*" "6_512_32_6_*_3_1_1_3" \
		"Ticks Elapsed vs Dispatch Window Size" \
		"ticks" \
		$@ 0 1

figures/dispatch_window/ports_%.svg: $$(call genDispatchWindowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_512_32_6_*_3_1_1_3" \
		"Instruction Througput vs Dispatch Window Size - $*.exe" \
		"port_utalization" \
		$@

figures/dispatch_window/throughput_%.svg: $$(call genDispatchWindowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_512_32_6_*_3_1_1_3" \
		"Port Utalization vs Dispatch Window Size - $*.exe" \
		"instruction_exec_throughput" \
		$@

figures/dispatch_window/waste_ratio_%.svg: $$(call genDispatchWindowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_512_32_6_*_3_1_1_3" \
		"Waste Ratio vs Dispatch Window Size - $*.exe" \
		"instruction_waste_ratio" \
		$@
