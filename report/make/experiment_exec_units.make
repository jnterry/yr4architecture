all: experiment_exec_units
experiment_exec_units: \
	$$(call zipUnderscore,figures/exec_units/ticks,$(addsuffix _low.svg,$(PROGS))) \
	$$(call zipUnderscore,figures/exec_units/ticks_stacked,$(addsuffix _low.svg,$(PROGS))) \
	$$(call zipUnderscore,figures/exec_units/ticks,$(addsuffix _high.svg,$(PROGS))) \
	$$(call zipUnderscore,figures/exec_units/ticks_stacked,$(addsuffix _high.svg,$(PROGS))) \
	$$(call zipUnderscore,figures/exec_units/waste_ratio,$(addsuffix _low.svg,$(PROGS))) \
	$$(call zipUnderscore,figures/exec_units/instruction_exec_throughput,$(addsuffix _low.svg,$(PROGS))) \

genExecUnitLowDeps = $(call genStatFileNames,\
	$(1),\
	16,\
	96,\
	32,\
	1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16,\
	64,\
	3,\
	1,\
	1,\
	3\
)

genExecUnitHighDeps = $(call genStatFileNames,\
	$(1),\
	64,\
	96,\
	32,\
	1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 24 28 32 40 48 56 64,\
	64,\
	3,\
	1,\
	1,\
	3\
)

figures/exec_units/ticks_%_low.svg: $$(call genExecUnitLowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "16_96_32_*_64_3_1_1_3" \
		"Ticks Elapsed vs Exec Unit Count - $*.exe" \
		"ticks" \
		$@

figures/exec_units/ticks_stacked_%_low.svg: $$(call genExecUnitLowDeps,%.exe)
	./plot_stacked_line.py \
		"$*.exe" "16_96_32_*_64_3_1_1_3" \
		"Ticks Elapsed vs Exec Unit Count - $*.exe" \
		$@

figures/exec_units/ticks_%_high.svg: $$(call genExecUnitHighDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "64_96_32_*_64_3_1_1_3" \
		"Ticks Elapsed vs Exec Unit Count - $*.exe" \
		"ticks" \
		$@

figures/exec_units/ticks_stacked_%_high.svg: $$(call genExecUnitHighDeps,%.exe)
	./plot_stacked_line.py \
		"$*.exe" "64_96_32_*_64_3_1_1_3" \
		"Ticks Elapsed vs Exec Unit Count - $*.exe" \
		$@


figures/exec_units/waste_ratio_%_low.svg: $$(call genExecUnitLowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "16_96_32_*_64_3_1_1_3" \
		"Wasted Percent vs Exec Unit Count - $*.exe" \
		"instruction_waste_ratio" \
		$@

figures/exec_units/instruction_exec_throughput_%_low.svg: $$(call genExecUnitLowDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "16_96_32_*_64_3_1_1_3" \
		"Exec Throughput vs Exec Unit Count - $*.exe" \
		"instruction_exec_throughput" \
		$@
