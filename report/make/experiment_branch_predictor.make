all: experiment_branch_predictor
experiment_branch_predictor: \
	$$(addprefix figures/branch_predictor/ticks/,$(PROGS_SVG)) \
	$$(addprefix figures/branch_predictor/missed_percent/,$(PROGS_SVG)) \
	figures/branch_predictor/missed_percent/all.svg \
	$$(addprefix figures/branch_predictor/throughput_exec/,$(PROGS_SVG)) \
	figures/branch_predictor/throughput_exec/all.svg \
	$$(addprefix figures/branch_predictor/throughput_commit/,$(PROGS_SVG)) \
	figures/branch_predictor/throughput_commit/all.svg \
	$$(addprefix figures/branch_predictor/waste_ratio/,$(PROGS_SVG)) \
	$$(addprefix figures/branch_predictor/wasted/,$(PROGS_SVG)) \
	$$(addprefix figures/branch_predictor/port_utalization/,$(PROGS_SVG)) \

genBranchPredictorDeps = $(call genStatFileNames,\
	$(1),\
	6,\
	96,\
	32,\
	6,\
	64,\
	3,\
	0 1,\
	1,\
	0 1 2 3\
)

figures/branch_predictor/ticks/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_3_?_1_*" \
		"Ticks Elapsed vs Branch Predictor Configuration - $*.exe" \
		"ticks" \
		$@

figures/branch_predictor/missed_percent/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_3_?_1_*" \
		"Branches Missed Percentage vs Branch Predictor Configuration - $*.exe" \
		"branches_missed_%" \
		$@

figures/branch_predictor/missed_percent/all.svg: $$(call genBranchPredictorDeps,$(PROGS_EXE))
	./plot_graphs.py \
		"*" "6_96_32_6_64_3_?_1_*" \
		"Branches Missed Percentage vs Branch Predictor Configuration" \
		"branches_missed_%" \
		$@

figures/branch_predictor/waste_ratio/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_3_?_1_*" \
		"Instruction Waste Ratio vs Branch Predictor Configuration - $*.exe" \
		"instruction_waste_ratio" \
		$@

figures/branch_predictor/wasted/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_3_?_1_*" \
		"Instructions Wasted vs Branch Predictor Configuration - $*.exe" \
		"instructions_wasted" \
		$@

figures/branch_predictor/throughput_exec/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_3_?_1_*" \
		"Instruction Throughput vs Branch Predictor Configuration - $*.exe" \
		"instruction_exec_throughput" \
		$@ 1

figures/branch_predictor/throughput_exec/all.svg: $$(call genBranchPredictorDeps,$(PROGS_EXE))
	./plot_graphs.py \
		"*" "6_96_32_6_64_3_?_1_*" \
		"Instruction Throughput vs Branch Predictor Configuration" \
		"instruction_exec_throughput" \
		$@ 1

figures/branch_predictor/throughput_commit/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_1_?_1_*" \
		"Instruction Throughput vs Branch Predictor Configuration - $*.exe" \
		"instruction_commit_throughput" \
		$@ 1

figures/branch_predictor/throughput_commit/all.svg: $$(call genBranchPredictorDeps,$(PROGS_EXE))
	./plot_graphs.py \
		"*" "6_96_32_6_64_1_?_1_*" \
		"Instruction Throughput vs Branch Predictor Configuration" \
		"instruction_commit_throughput" \
		$@ 1

figures/branch_predictor/port_utalization/%.svg: $$(call genBranchPredictorDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_1_?_1_*" \
		"Port Utalization vs Branch Predictor Configuration - $*.exe" \
		"port_utalization" \
		$@ 1
