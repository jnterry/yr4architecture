all: experiment_register_renaming
experiment_register_renaming: \
	$$(call zipUnderscore,figures/register_renaming/all_opts/ports,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/register_renaming/no_opts/ports,$(PROGS_SVG)) \

genRegisterRenamingAllOptsDeps = $(call genStatFileNames,\
	$(1),\
	6,\
	96,\
	32,\
	6,\
	64,\
	0 1 2 3,\
	1,\
	1,\
	3\
)

figures/register_renaming/all_opts/ports_%.svg: $$(call genRegisterRenamingAllOptsDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_*_1_1_3" \
		"Port Utalization vs Register Renaming - $*.exe" \
		"port_utalization" \
		$@ 1


genRegisterRenamingNoOptsDeps = $(call genStatFileNames,\
	$(1),\
	6,\
	96,\
	32,\
	6,\
	64,\
	0 1 2 3,\
	0,\
	0,\
	0\
)

figures/register_renaming/no_opts/ports_%.svg: $$(call genRegisterRenamingNoOptsDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_32_6_64_*_0_0_0" \
		"Port Utalization vs Register Renaming - $*.exe" \
		"port_utalization" \
		$@ 1
