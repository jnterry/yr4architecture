all: experiment_reorder_buffer
experiment_reorder_buffer: \
	$$(call zipUnderscore,figures/reorder_buffer/ticks_stacked,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/reorder_buffer/frontend_6/ticks,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/reorder_buffer/frontend_32/ticks,$(PROGS_SVG)) \
	$$(call zipUnderscore,figures/reorder_buffer/dispatch_64/ticks,$(PROGS_SVG)) \

genReorderBufferDeps = $(call genStatFileNames,\
	$(1),\
	6 32,\
	6 7 8 10 11 14 15 16 32 48 64 96 128 144 160,\
	32,\
	6,\
	6 16 32 64 128 256,\
	3,\
	1,\
	1,\
	3\
)

figures/reorder_buffer/ticks_stacked_%.svg: $$(call genReorderBufferDeps,%.exe)
	./plot_stacked_line.py \
		"$*.exe" "6_*_32_6_128_3_1_1_3" \
		"Ticks Elapsed vs Reorder Buffer Size - $*.exe" \
		$@

figures/reorder_buffer/frontend_6/ticks_%.svg: $$(call genReorderBufferDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_*_32_6_?_3_1_1_3" \
		"Ticks Elapsed vs Reorder Buffer Size - $*.exe" \
		"ticks" \
		$@

figures/reorder_buffer/frontend_32/ticks_%.svg: $$(call genReorderBufferDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "32_*_32_6_?_3_1_1_3" \
		"Ticks Elapsed vs Reorder Buffer Size - $*.exe" \
		"ticks" \
		$@

figures/reorder_buffer/dispatch_64/ticks_%.svg: $$(call genReorderBufferDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "?_*_32_6_64_3_1_1_3" \
		"Ticks Elapsed vs Reorder Buffer Size - $*.exe" \
		"ticks" \
		$@
