###################################################################
# Contains helper functions that are useful when defining experiements
# by generating all possible csv file names that represent all possible
# combinations of some set of processor model(s) and program(s)
###################################################################

# Helper function for genModelStrings
# Finds all combinatons of 2 lists and joins with underscore
# 1 2,a b  ---becomes---> 1_a 1_b 2_a 2_b
zipUnderscore = $(foreach i,$(2),$(addsuffix _$(i),$(1)))

# Creates a model string by combining all combinations of 9 input lists for
# each model parameter
# For example:
# $(call mkModelString,6,   128,   32,   6,   64,   1,   0 1,   0,   0 1 2 3)
# yeilds the 8 combinations:
# 6_128_32_6_64_1_0_0_0
# 6_128_32_6_64_1_1_0_0
# 6_128_32_6_64_1_0_0_1
# 6_128_32_6_64_1_1_0_1
# 6_128_32_6_64_1_0_0_2
# 6_128_32_6_64_1_1_0_2
# 6_128_32_6_64_1_0_0_3
# 6_128_32_6_64_1_1_0_3
genModelStrings = $(call zipUnderscore,$(1),$(call zipUnderscore,$(2),$(call zipUnderscore,$(3),$(call zipUnderscore,$(4),$(call zipUnderscore,$(5),$(call zipUnderscore,$(6),$(call zipUnderscore,$(7),$(call zipUnderscore,$(8),$(9)))))))))

# Generate stat csv file name for dependencies for some set of programs and
# processor model set, for example:
# $(call genModelStrings,bubble_sort.exe gcd.exe,6,   128,   32,   6,   64,   1,   0 1,   0,   0 1 2 3)
# Would produce:
# stats/bubble_sort.exe/???.csv
# stats/gcd.exe/???.csv
# Where ??? is replaced by all combinations produced by genModelStrings
genStatFileNames = $(foreach prog,$(1),$(addsuffix .csv,$(addprefix stats/$(prog)/,$(call genModelStrings,$(2),$(3),$(4),$(5),$(6),$(7),$(8),$(9),$(10)))))
