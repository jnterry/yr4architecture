###################################################################
# Contains targets to generate particular stat csv files
# by first building simulator with appropriate model
# and then running it against the progam in question
#
# For example the target:
# stats/bubble_sort.exe/6_128_32_6_64_8_0_0_0.csv
# Will cause the corresponding model of processor to be compiled
# and then run the bubble_sort.exe program to generate the csv
# file
###################################################################

# Generate cmake files for some configuration of repsim
# Config string should be of the form:
# 6_128_32_6_64_8_0_0_0
# Where:
# [0] ->   6   -> FRONTEND_WIDTH
# [1] -> 128   -> REORDER_BIFFER_SIZE
# [2] ->  32   -> MEMORY_BUFFER_SIZE
# [3] ->   6   -> NUM_EXEC_UNITS
# [4] ->  64   -> DISPTCH_WINDOW_SIZE
# [5] -> 0/1   -> REGISTER_RENAMING
# [6] -> 0/1   -> PARTIAL_ROLLBACK
# [7] -> 0/1   -> PIPELINED_EXEC_UNITS
# [8] -> [0-3] -> BRANCH_PREDICTOR
build/%:
	mkdir -p build/$*
	cd build/$* && cmake ../../.. \
		-DPROCSIM_BIN_DIR=$(shell pwd)/bin \
		-DPROCSIM_SIM_EXE_NAME:STRING=$* \
		-DPROCSIM_USE_SYSTEM_SDL:BOOL=1 \
		-DPROCSIM_MODEL_TYPE=3 \
		-DPROCSIM_MODEL_FRONTEND_WIDTH=$(word 1,$(subst _, ,$*)) \
		-DPROCSIM_MODEL_REORDER_BUFFER_SIZE=$(word 2,$(subst _, ,$*)) \
		-DPROCSIM_MODEL_MEMORY_BUFFER_SIZE=$(word 3,$(subst _, ,$*)) \
		-DPROCSIM_MODEL_NUM_EXEC_UNITS=$(word 4,$(subst _, ,$*)) \
		-DPROCSIM_MODEL_DISPATCH_WINDOW=$(word 5,$(subst _, ,$*)) \
		-DPROCSIM_OPT_REGISTER_RENAMING=$(word 6,$(subst _, ,$*)) \
		-DPROCSIM_OPT_PARTIAL_ROLLBACK=$(word 7,$(subst _, ,$*)) \
		-DPROCSIM_OPT_PIPELINED_EXEC_UNITS=$(word 8,$(subst _, ,$*)) \
		-DPROCSIM_OPT_BRANCH_PREDICTOR=$(word 9,$(subst _, ,$*)) \

# Build some configuration of repsim (using generated cmake files)
bin/%: build/%
	mkdir -p ./bin
	cd build/$* && $(MAKE) simulator

# Run a particular repsim build against a particular program to
# produce a stats csv
# Wildcard string should be of the form:
# program.exe/6_128_32_6_64_8_0_0_0
# to indicate both the program and processor model to run against
stats/%.csv: $$(addprefix bin/,$$(word 2, $$(subst /, ,%)))
	mkdir -p stats
	./bin/$(word 2, $(subst /, ,$*)) ../bin/programs/$(word 1, $(subst /, ,$*))
