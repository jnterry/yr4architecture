all: experiment_memory_buffer
experiment_memory_buffer: \
	$$(call zipUnderscore,figures/memory_buffer/ticks,$(PROGS_SVG))

genMemoryBufferDeps = $(call genStatFileNames,\
	$(1),\
	6,\
	96,\
	6 7 8 9 10 11 12 13 14 15 16 20 24 28 32 40 48 56 64,\
	6,\
	64,\
	3,\
	1,\
	1,\
	3\
)

figures/memory_buffer/ticks_%.svg: $$(call genMemoryBufferDeps,%.exe)
	./plot_graphs.py \
		"$*.exe" "6_96_*_6_64_3_1_1_3" \
		"Ticks Elapsed vs Memory Buffer Size - $*.exe" \
		"ticks" \
		$@
