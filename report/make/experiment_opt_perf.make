all: experiment_opt_perf
experiment_opt_perf: \
	$$(addprefix figures/opt_perf/by_prog_1_all/,      $$(addsuffix .svg,$(PROGS))) \
	$$(addprefix figures/opt_perf/by_prog_2_oo_rename/,$$(addsuffix .svg,$(PROGS))) \
	$$(addprefix figures/opt_perf/by_prog_3_branch/,   $$(addsuffix .svg,$(PROGS))) \
	$$(addprefix figures/opt_perf/by_prog_4_dense/,    $$(addsuffix .svg,$(PROGS))) \
	figures/opt_perf/by_opt/oo_optimal.svg \
	figures/opt_perf/by_opt/oo_other.svg \
	figures/opt_perf/by_opt/branch_other.svg \
	figures/opt_perf/by_opt/branch_misc.svg \



genOptPerfDeps = \
	stats/$(1)/6_96_32_6_64_0_0_0_0.csv \
	stats/$(1)/6_96_32_6_64_1_0_0_0.csv \
	stats/$(1)/6_96_32_6_64_2_0_0_0.csv \
	stats/$(1)/6_96_32_6_64_3_0_0_0.csv \
	stats/$(1)/6_96_32_6_64_3_1_0_0.csv \
	stats/$(1)/6_96_32_6_64_3_1_1_0.csv \
	stats/$(1)/6_96_32_6_64_3_0_1_0.csv \
	stats/$(1)/6_96_32_6_64_3_1_1_0.csv \
	stats/$(1)/6_96_32_6_64_3_1_1_1.csv \
	stats/$(1)/6_96_32_6_64_3_1_1_2.csv \
	stats/$(1)/6_96_32_6_64_3_1_1_3.csv \
	stats/$(1)/6_96_32_6_64_3_0_1_3.csv \

opt_perf_data: $$(addprefix stats/, $$(addprefix PROG_NAMES_EXE,OPT_PERF_DATA_STATS))


figures/opt_perf/by_prog_1_all/%.svg: $$(call genOptPerfDeps,%.exe)
	./plot_opt_perf.py $@ $*.exe '---' \
		'None,          6_96_32_6_64_0_0_0_0' \
		'Rename,        6_96_32_6_64_1_0_0_0' \
		'OO Exec,       6_96_32_6_64_2_0_0_0' \
		'OO + Rename,   6_96_32_6_64_3_0_0_0' \
		'Partial Roll,  6_96_32_6_64_3_1_0_0' \
		'Piped,         6_96_32_6_64_3_0_1_0' \
		'Partial+Piped, 6_96_32_6_64_3_1_1_0' \
		'Branch 1,      6_96_32_6_64_3_1_1_1' \
		'Branch 2,      6_96_32_6_64_3_1_1_2' \
		'Branch 3,      6_96_32_6_64_3_1_1_3'

figures/opt_perf/by_prog_2_oo_rename/%.svg: $$(call genOptPerfDeps,%.exe)
	./plot_opt_perf.py $@ $*.exe '---' \
		'None,          6_96_32_6_64_0_0_0_0' \
		'Rename,        6_96_32_6_64_1_0_0_0' \
		'OO Exec,       6_96_32_6_64_2_0_0_0' \
		'OO + Rename,   6_96_32_6_64_3_0_0_0' \

figures/opt_perf/by_prog_3_branch/%.svg: $$(call genOptPerfDeps,%.exe)
	./plot_opt_perf.py $@ $*.exe '---' \
		'OO + Rename,   6_96_32_6_64_3_0_0_0' \
		'Piped,         6_96_32_6_64_3_0_1_0' \
		'Partial+Piped, 6_96_32_6_64_3_1_1_0' \
		'Branch 1,      6_96_32_6_64_3_1_1_1' \
		'Branch 2,      6_96_32_6_64_3_1_1_2' \
		'Branch 3,      6_96_32_6_64_3_1_1_3'

figures/opt_perf/by_prog_4_dense/%.svg: $$(call genOptPerfDeps,%.exe)
	./plot_opt_perf.py $@ $*.exe '---' \
		'None,               6_96_32_6_64_0_0_0_0' \
		'Rename,             6_96_32_6_64_1_0_0_0' \
		'OO Exec,            6_96_32_6_64_2_0_0_0' \
		'OO + Rename,        6_96_32_6_64_3_0_0_0' \
		'+ Sub Pipeline,     6_96_32_6_64_3_0_1_0' \
		'+ Branch Pred,      6_96_32_6_64_3_0_1_3' \
		'+ Partial Flush ,   6_96_32_6_64_3_1_1_3' \

########################################################

figures/opt_perf/by_opt/oo_optimal.svg: \
		$$(call genOptPerfDeps,optimal6.exe) \
		$$(call genOptPerfDeps,optimal4.exe) \
		$$(call genOptPerfDeps,optimal2.exe)
	./plot_opt_perf.py $@ \
		optimal6.exe \
		optimal4.exe \
		optimal2.exe \
		'---' \
		'None,          6_96_32_6_64_0_0_0_0' \
		'Rename,        6_96_32_6_64_1_0_0_0' \
		'OO Exec,       6_96_32_6_64_2_0_0_0' \
		'OO + Rename,   6_96_32_6_64_3_0_0_0' \

figures/opt_perf/by_opt/oo_other.svg: \
		$$(call genOptPerfDeps,optimal6.exe) \
		$$(call genOptPerfDeps,optimal4.exe) \
		$$(call genOptPerfDeps,optimal2.exe)
	./plot_opt_perf.py $@ \
		matrix_multiply.exe \
		balls.exe \
		array_process.exe \
		'---' \
		'None,          6_96_32_6_64_0_0_0_0' \
		'Rename,        6_96_32_6_64_1_0_0_0' \
		'OO Exec,       6_96_32_6_64_2_0_0_0' \
		'OO + Rename,   6_96_32_6_64_3_0_0_0' \


figures/opt_perf/by_opt/branch_other.svg: \
		$$(call genOptPerfDeps,matrix_multiply.exe) \
		$$(call genOptPerfDeps,balls.exe) \
		$$(call genOptPerfDeps,gcd.exe) \
		$$(call genOptPerfDeps,array_process.exe)
	./plot_opt_perf.py $@ \
		matrix_multiply.exe \
		balls.exe \
		array_process.exe \
		'---' \
		'OO + Rename,        6_96_32_6_64_3_0_0_0' \
		'+ Sub Pipelining,   6_96_32_6_64_3_0_1_0' \
		'+ Branch Predictor, 6_96_32_6_64_3_0_1_3' \
		'+ Partial Flush,    6_96_32_6_64_3_1_1_3'

figures/opt_perf/by_opt/branch_misc.svg: \
		$$(call genOptPerfDeps,matrix_multiply.exe) \
		$$(call genOptPerfDeps,balls.exe) \
		$$(call genOptPerfDeps,gcd.exe) \
		$$(call genOptPerfDeps,array_process.exe)
	./plot_opt_perf.py $@ \
		bubble_sort.exe \
		fib.exe \
		'---' \
		'OO + Rename,        6_96_32_6_64_3_0_0_0' \
		'+ Sub Pipelining,   6_96_32_6_64_3_0_1_0' \
		'+ Branch Predictor, 6_96_32_6_64_3_0_1_3' \
		'+ Partial Flush,    6_96_32_6_64_3_1_1_3'
