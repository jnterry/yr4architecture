#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import re
import sys
import math
from procsim_data import *

#########################################################################
# Program "main"
if len(sys.argv) < 5:
    print("ERROR: Bad usage, try:")
    print("%s <PROGRAM_NAME> <SERIES_MATCHER> <GRAPH_TITLE> <OUT_FILE>" % sys.argv[0])
    sys.exit(1)

CLI_PROG_REGEX      = sys.argv[1]
CLI_SERIES_MATCHER  = sys.argv[2]
CLI_TITLE           = sys.argv[3]
CLI_OUT_FILE        = sys.argv[4]

if ((not re.match('((\d+|\*)_){9}', CLI_SERIES_MATCHER + '_')) or
    CLI_SERIES_MATCHER.count('*') > 1):
    print("ERROR: Invalid series matcher")
    sys.exit(2)


series = loadSeries(sys.argv[1], sys.argv[2])

if(len(list(series.keys())) != 1):
    print("ERROR: Expected to load single data series only!")
    sys.exit(3)

data = series[series.keys()[0]]
(xs,ys) = getSeriesDataArrays(data, "port_utalization")

(_,ys_committed)  = getSeriesDataArrays(data, "instructions_committed")
(_,ys_throughput) = getSeriesDataArrays(data, "instruction_exec_throughput")

max_port_count = max([len(pu) for pu in ys])
colors = plt.cm.rainbow(np.linspace(0,1, max_port_count))

fig = plt.figure()
ax  = plt.gca()

plt.title(CLI_TITLE)

bottoms = np.zeros(len(xs))
for stack_idx in range(max_port_count):
    tops = ys[:,stack_idx]
    tops = bottoms + tops
    ax.fill_between(xs, bottoms, tops,
                    facecolor=colors[stack_idx],
                    edgecolor='black',
                    label='%i ports' % stack_idx
    )
    bottoms = tops

#ax.plot(xs, ys_committed / ys_throughput, linestyle='dashed', color='black', linewidth=5)

if(max_port_count <= 20):
    plt.legend(ncol=4)

plt.tight_layout()

containing_dir = os.path.dirname(os.path.abspath(CLI_OUT_FILE))
if not os.path.exists(containing_dir):
    os.makedirs(containing_dir)
plt.savefig(CLI_OUT_FILE)
