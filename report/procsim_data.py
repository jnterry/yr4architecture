####################################################################
# Various helper functions for loading procsim data files
####################################################################

import numpy as np
import os
import re

# the names of the variables found in the model string
# such as 6_128_32_6_64_8_0_0_0
MODEL_NAMES = [
    "Frontend Width",
    "Reorder Buffer Size",
    "Memory Buffer Size",
    "Exec Unit Count",
    "Dispatch Window Size",
    "Register Renaming",
    "Partial Rollback",
    "Pipelined Exec Units",
    "Branch Predictor",
]

# Converts string such as "bubble_sort.exe" to "Bubble Sort"
def prettifyProgramName(prog_name):
    if(prog_name.endswith(".exe")):
        prog_name = prog_name[:len(prog_name)-4]

    prog_name = " ".join([ x.capitalize() for x in prog_name.split('_')])

    return prog_name

def loadStatFile(filename):
    lines = []
    with open(filename, "r") as fin:
        for line in fin:
            lines.append([int(x.strip()) for x in line.split(',')[1:]])

    if(len(lines) != 4):
        raise ValueError("Stat file must contain 4 lines")

    return {
        'ticks'                   : lines[0][0],
        'instructions_execed'     : lines[1][0],
        'instructions_committed'  : lines[1][1],
        'instructions_committed_%': (float(lines[1][1]) / float(lines[1][0])) * 100.0,
        'instructions_wasted'     : lines[1][2],
        'instruction_waste_ratio' : (float(lines[1][2]) / float(lines[1][1])),
        'port_utalization'        : lines[2],
        'port_utalization_%'      : [ (float(x) / float(lines[0][0])) * 100.0 for x in lines[2] ],
        'branches_total'          : lines[3][0],
        'branches_missed'         : lines[3][1],
        'branches_missed_%'       : (((float(lines[3][1]) / float(lines[3][0])) * 100.0)
                                     if float(lines[3][0]) > 0 else 0),

        'instruction_commit_throughput': (float(lines[1][1]) / float(lines[0][0])),
        'instruction_exec_throughput'  : (float(lines[1][0]) / float(lines[0][0])),
    }

def loadProgramStatFile(prog_name, stat_matcher):
    if not stat_matcher.endswith('.csv'):
        stat_matcher += '.csv'
    return loadStatFile("stats/%s/%s" % (prog_name, stat_matcher))

# stat_matcher specifies the models we are interested in
# Should be of the form:
# 6_128_32_6_64_8_0_0_*
# Where the stared element indicates the x-axis variable
#
# If one (or more) of the elements is replaced by ? then all
# such stat files will be loaded, but these will be treated as
# seperate data series
def loadProgramSeries(prog_name, stat_matcher):
    stat_regex = stat_matcher.replace('*', '\d+').replace('?', '\d+')

    files = os.listdir("stats/" + prog_name)
    files = [ f for f in files if re.match(stat_regex, f) ]

    raw_data = [ loadProgramStatFile(prog_name, f) for f in files ]

    # Count number of variable values that can be used to split into multiple
    # data series
    var_indicies = [ i for i, x in enumerate(stat_matcher.split('_')) if x == '?']
    var_names    = [ MODEL_NAMES[i] for i in var_indicies ]

    data = {}

    for i in range(0, len(files)):
        series_name = prettifyProgramName(prog_name)

        regex = stat_matcher.replace('*', '\d+').replace('?', '(\d+)')
        match = re.match(regex, files[i])
        for v in range(len(var_indicies)):
            series_name += ", " + var_names[v] + "=" + match.group(v+1)

        if not series_name in data:
            data.update({ series_name : { 'x': [], 'y': [] } })

        x_regex = stat_matcher.replace('*', '(\d+)').replace('?', '\d+')
        match = re.match(x_regex, files[i])
        data[series_name]['x'].append(int(match.group(1)))
        data[series_name]['y'].append(raw_data[i])

    return data

def loadSeries(prog_matcher, stat_matcher):
    progs = os.listdir("stats/")

    if(prog_matcher != "*"):
        progs = [ p for p in progs if re.match(prog_matcher, p) ]

    print("Loading data for programs: " + str(progs))

    data = {}

    for p in progs:
        data.update(loadProgramSeries(p, stat_matcher))

    print("Loaded data series: " + str(list(data.keys())))

    return data


def getSeriesDataArrays(series, metric):
    xs = series['x']


    ys = [ series['y'][i][metric] for i in range(len(series['y'])) ]

    if hasattr(ys[0], '__len__'):
        # Then data is a list, make sure all have same length
        length = max([len(d) for d in ys])
        ys = [ np.pad(d, (0, length - len(d)), 'constant') for d in ys ]

    order = np.argsort(xs)
    xs = np.array(xs)[order]
    ys = (np.array(ys)[order]).astype(float)
    return (xs, ys)
