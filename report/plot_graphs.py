#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import re
import sys
from procsim_data import *

#########################################################################
# Program "main"
if len(sys.argv) < 5:
    print("ERROR: Bad usage, try:")
    print("%s <PROGRAM_REGEX> <SERIES_MATCHER> <GRAPH_TITLE> <METRIC> <OUT_FILE> [BARS] [LOG_Y_SCALE]" % sys.argv[0])
    sys.exit(1)

CLI_PROG_REGEX      = sys.argv[1]
CLI_SERIES_MATCHER  = sys.argv[2]
CLI_TITLE           = sys.argv[3]
CLI_METRIC          = sys.argv[4]
CLI_OUT_FILE        = sys.argv[5]
CLI_USE_BARS        = int(sys.argv[6]) if len(sys.argv) > 6 else False
CLI_LOG_SCALE       = int(sys.argv[7]) if len(sys.argv) > 7 else False

if ((not re.match('((\d+|\?|\*)_){9}', CLI_SERIES_MATCHER + '_')) or
    CLI_SERIES_MATCHER.count('*') > 1):
    print("ERROR: Invalid series matcher")
    sys.exit(2)


series = loadSeries(sys.argv[1], sys.argv[2])

if(len(list(series.keys())) == 0):
    print("ERROR: No data series were loaded")
    sys.exit(3)

fig = plt.figure()
ax  = plt.gca()

plt.title(CLI_TITLE)

if(CLI_LOG_SCALE):
    ax.set_yscale("log")

if CLI_USE_BARS:
    series_names = series.keys()
    bar_width = 1.0 / float(len(series_names)+1)

    (xs,_) = getSeriesDataArrays(series[series_names[0]], CLI_METRIC)

    plt.xticks(xs)

    x_offset = 0
    if len(series_names) % 2 == 0:
        x_offset -= bar_width * 0.5

    colors = iter(plt.cm.rainbow(np.linspace(0,1,len(series_names))))

    for i in range(len(series_names)):
        (xs,ys) = getSeriesDataArrays(series[series_names[i]], CLI_METRIC)

        bottoms = np.zeros(len(xs))

        xs = xs.astype(float)
        xs -= (bar_width)*(i - float(len(series_names))/2.0)
        xs += x_offset

        ys = [ (np.array(y) if hasattr(y, '__len__') else [y]) for y in ys ]
        max_stack_count = max([len(y) for y in ys])

        base_color = next(colors)

        for s in range(max_stack_count):
            col = base_color
            col[3] *= ((float(max_stack_count - s) / (float(max_stack_count)) + 1) / 2)
            ys_stack = [ (y[s] if s < len(y) else 0) for y in ys ]
            bars = plt.bar(xs, ys_stack,
                    align="center",
                    width  = bar_width,
                    label  = series_names[i] if s == 0 else None,
                    bottom = bottoms,
                    color  = col,
                    edgecolor='black'

            )
            bottoms += ys_stack

            if max_stack_count > 1:
                for bar in bars:
                    cx = bar.get_x() + 0.5 * bar.get_width()
                    cy = bar.get_y() + 0.5 * bar.get_height()
                    if bar.get_height() > 10:
                        plt.text(cx, cy, str(s),
                                 horizontalalignment='center',
                                 verticalalignment='center')

else:
    for k in series.keys():
        (xs,ys) = getSeriesDataArrays(series[k], CLI_METRIC)
        plt.plot(xs, ys, label=k)

if(len(series.keys()) > 1):
    plt.legend()

containing_dir = os.path.dirname(os.path.abspath(CLI_OUT_FILE))
if not os.path.exists(containing_dir):
    os.makedirs(containing_dir)
plt.savefig(CLI_OUT_FILE)
