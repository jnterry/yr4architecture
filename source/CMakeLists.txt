set(PROCSIM_COMMON_SOURCE
	${CMAKE_CURRENT_LIST_DIR}/common/procsim/instruction.hpp
	${CMAKE_CURRENT_LIST_DIR}/common/procsim/instruction.cpp
	${CMAKE_CURRENT_LIST_DIR}/common/procsim/register.hpp
	${CMAKE_CURRENT_LIST_DIR}/common/procsim/register.cpp

	${CMAKE_CURRENT_LIST_DIR}/common/procsim/types.hpp
)

set(PROCSIM_SIMULATOR_SOURCE
	${CMAKE_CURRENT_LIST_DIR}/simulator/device.hpp

	${CMAKE_CURRENT_LIST_DIR}/simulator/main.cpp

	${CMAKE_CURRENT_LIST_DIR}/simulator/processor.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/model.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/types.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/processor.cpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/memory_controller.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/memory_controller.cpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/branch_predictor.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/branch_predictor.cpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/processor/processor.cpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/display.hpp
	${CMAKE_CURRENT_LIST_DIR}/simulator/display.cpp
)

add_executable            (simulator ${PROCSIM_SIMULATOR_SOURCE} ${PROCSIM_COMMON_SOURCE})
target_link_libraries     (simulator xen-core)
target_include_directories(simulator PRIVATE "${CMAKE_CURRENT_LIST_DIR}/common/")

if(${PROCSIM_USE_SYSTEM_SDL})
	find_package(SDL2 REQUIRED)
	message("Using system provided SDL2, location: ${SDL2_INCLUDE_DIRS}")
	target_link_libraries     (simulator ${SDL2_LIBRARIES})
	target_include_directories(simulator PRIVATE ${SDL2_INCLUDE_DIRS})
else()
	target_link_libraries     (simulator SDL2)
	target_include_directories(simulator PRIVATE "${CMAKE_CURRENT_LIST_DIR}/../extlibs/SDL2-2.0.8/include/")
endif()

set(PROCSIM_SIM_EXE_NAME "simulator" CACHE STRING "Name of simulator executable file")
set_target_properties(simulator PROPERTIES OUTPUT_NAME "${PROCSIM_SIM_EXE_NAME}")

message("=== OPTIMIZATIONS ==========================")
set(PROCSIM_OPT_REGISTER_RENAMING "" CACHE STRING "Whether to use register renaming")
if("${PROCSIM_OPT_REGISTER_RENAMING}" STREQUAL "")
	message("Using default code-defined register renaming setting")
else()
	message("Using register renaming unit: ${PROCSIM_OPT_REGISTER_RENAMING}")
	add_definitions(-DOPT_REGISTER_RENAMING=${PROCSIM_OPT_REGISTER_RENAMING})
endif()

set(PROCSIM_OPT_PARTIAL_ROLLBACK "" CACHE STRING "Whether to use partial rollback")
if("${PROCSIM_OPT_PARTIAL_ROLLBACK}" STREQUAL "")
	message("Using default code-defined partial rollback setting")
else()
	message("Using partial rollback: ${PROCSIM_OPT_PARTIAL_ROLLBACK}")
	add_definitions(-DOPT_PARTIAL_ROLLBACK=${PROCSIM_OPT_PARTIAL_ROLLBACK})
endif()

set(PROCSIM_OPT_PIPELINED_EXEC_UNITS "" CACHE STRING "Whether to use pipelined exec units")
if("${PROCSIM_OPT_PIPELINED_EXEC_UNITS}" STREQUAL "")
	message("Using default code-defined pipelined exec unit setting")
else()
	message("Using pipelined exec unit: ${PROCSIM_OPT_PIPELINED_EXEC_UNITS}")
	add_definitions(-DOPT_PIPELINED_EXEC_UNITS=${PROCSIM_OPT_PIPELINED_EXEC_UNITS})
endif()

set(PROCSIM_OPT_BRANCH_PREDICTOR "" CACHE STRING "Level of branch prediction to use")
if("${PROCSIM_OPT_BRANCH_PREDICTOR}" STREQUAL "")
	message("Using default code-defined branch prediction level")
else()
	message("Using branch predictor level ${PROCSIM_OPT_BRANCH_PREDICTOR}")
	add_definitions(-DOPT_BRANCH_PREDICTOR=${PROCSIM_OPT_BRANCH_PREDICTOR})
endif()
message("============================================")

set(PROCSIM_OPT_PIPELINED_EXEC_UNITS "" CACHE STRING "Whether to use pipelined exec units")
message("=== MODEL ==================================")
if("${PROCSIM_MODEL_TYPE}" STREQUAL "")
	message("Using default code-defined processor model")
else()
	add_definitions(-DMODEL_TYPE=${PROCSIM_MODEL_TYPE})
	if("${PROCSIM_MODEL_TYPE}" LESS 3)
		message("Using model ${PROCSIM_MODEL_TYPE}")
	else()
		message("Using custom model")
		add_definitions(-DFRONTEND_WIDTH=${PROCSIM_MODEL_FRONTEND_WIDTH})
		add_definitions(-DREORDER_BUFFER_SIZE=${PROCSIM_MODEL_REORDER_BUFFER_SIZE})
		add_definitions(-DMEMORY_BUFFER_SIZE=${PROCSIM_MODEL_MEMORY_BUFFER_SIZE})
		add_definitions(-DNUM_EXEC_UNITS=${PROCSIM_MODEL_NUM_EXEC_UNITS})
		add_definitions(-DDISPATCH_WINDOW=${PROCSIM_MODEL_DISPATCH_WINDOW})
	endif()
endif()
message("============================================")

set(PROCSIM_ASSEMBER_SOURCE
	${CMAKE_CURRENT_LIST_DIR}/assembler/types.hpp
	${CMAKE_CURRENT_LIST_DIR}/assembler/main.cpp
	${CMAKE_CURRENT_LIST_DIR}/assembler/parser.hpp
	${CMAKE_CURRENT_LIST_DIR}/assembler/parser.cpp
	${CMAKE_CURRENT_LIST_DIR}/assembler/writer.hpp
	${CMAKE_CURRENT_LIST_DIR}/assembler/writer.cpp
)
add_executable            (assembler ${PROCSIM_ASSEMBER_SOURCE} ${PROCSIM_COMMON_SOURCE})
target_link_libraries     (assembler xen-core)
target_include_directories(assembler PRIVATE "${CMAKE_CURRENT_LIST_DIR}/common/")
