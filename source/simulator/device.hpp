////////////////////////////////////////////////////////////////////////////
/// \brief Contains the device type
///
/// The simulator uses memory-mapped IO for communication between devices,
/// for example, the system may consist of two devices - the CPU and the Display
/// unit.
///
/// The CPU can write to the memory address range owned by the display unit in
/// order to interact with it. Hence each device
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_DEVICE_HPP
#define PROCSIM_SIMULATOR_DEVICE_HPP

#include <xen/core/array_types.hpp>
#include <xen/core/intrinsics.hpp>

#include <procsim/types.hpp>

struct System;

struct Device {
	/// \brief The system this device is a part of
	System* system;

	/// \brief Pointer to the internal state of the device - this
	/// will be initialised by a init function, used by tick() to
	/// advance the state of the simulated device, and deallocated
	/// by shutdown
	void* state;

	/// \brief The first simulated physical address in the memory owned by
	/// this device
	u64 memory_start;

	/// \brief The last simulated physical address in the memory owned by
	/// this device
	u64 memory_end;

	/// \brief Pointer to the buffer representing the range of
	/// memory addresses owned by the device
	void* memory;
};

/// \brief Represents a full system of interconnected devices
struct System {
	Device* devices;
	u32     device_count;

	bool halted;
};

inline void* resolveSimulatorAddress(System* system, u64 address){
	for(u64 i = 0; i < system->device_count; ++i){
		if(address >= system->devices[i].memory_start &&
		   address <= system->devices[i].memory_end){
			return &(((u08*)system->devices[i].memory)[address - system->devices[i].memory_start]);
		}
	}

	return nullptr;
}
template<typename T>
T readMemory(System* system, u64 address){
	void* addr = resolveSimulatorAddress(system, address);

	if(addr == nullptr){
		printf("Attempt made to read from memory address %18p, however this address is not within the memory map of any device\n", (void*)address);
		system->halted = true;
		return {0};
	}

	return *((T*)addr);
}

template<typename T>
void writeMemory (System* system, u64 address, T value){
	void* addr = resolveSimulatorAddress(system, address);

	if(addr == nullptr){
		printf("Attempt made to write to memory address %18p, however this address is not within the memory map of any device\n", (void*)address);
		system->halted = true;
		return;
	}

	*((T*)(addr)) = value;
}

#endif
