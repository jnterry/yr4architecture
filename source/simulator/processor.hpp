////////////////////////////////////////////////////////////////////////////
/// \brief Contains the interface to the simulator
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_PROCESSOR_HPP
#define PROCSIM_SIMULATOR_PROCESSOR_HPP

#include <procsim/types.hpp>
#include "device.hpp"

/// \brief Initialises some processor, loading the specified file into
/// memory at the start address
bool initProcessor    (System* system, Device* device, const char* memory_file);

/// \brief Frees device specific data for the processor
void shutdownProcessor(Device* device, const char* memory_file);

/// \brief Advances the state of the processor by a single tick
void tickProcessor    (Device* device);


#endif
