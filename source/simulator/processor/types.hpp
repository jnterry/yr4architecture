////////////////////////////////////////////////////////////////////////////
/// \brief Contains various types for defining the processor
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_PROCESSOR_TYPES_HPP
#define PROCSIM_SIMULATOR_PROCESSOR_TYPES_HPP

#include <procsim/instruction.hpp>
#include <procsim/register.hpp>
#include <xen/core/RingBuffer.hpp>
#include <xen/core/array.hpp>

#include "model.hpp"

typedef u64 SequenceId;

using RegisterFile = Word64[Register::COUNT];

/// \brief Represents an instruction loaded from memory along with
/// some meta data
struct LoadedInstruction {
 	u64         pc;
	SequenceId  seq_id;
	Instruction instruction;
};

using FetchedInstructionBuffer = xen::RingBuffer<LoadedInstruction>;

/// \brief Unit responsible for loading instructions from main memory,
/// assigning them a sequence id, and buffering the loaded instructions
/// until decode unit is ready to accept them
struct FetchUnit {
	/// \brief The next address to be fetched from
	u64 next_pc;

	/// \brief The next sequence id for the fetch unit to assign
	/// to a loaded instruction
	u64 next_seq_id;

	/// \brief Whether the fetch unit has retrieved a "halt" instruction
	bool halted;

	/// \brief Buffer that instructions are loaded into until
	/// decode unit is ready to take them
	FetchedInstructionBuffer buffer;
};

// Disable gcc's warning about anonymous structs in unions temporarily...
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

/// \brief Represents the output of the decode stage
struct DecodedInstruction {
	struct Argument {
		/// \brief Whether this argument is a register or immediate
		bool is_immediate;

		union {
			/// \brief Immediate value of the argument
			Word64 immediate;

			struct {
				/// \brief The register to read from
				Register source_register;

				/// \brief The offset to apply to the register
				s16      offset;
			};
		};
	};

	SequenceId            seq_id;
	u64                   pc;

	/// \brief The instruction to perform
	Instruction::Opcode opcode;

	/// \brief The architectural register into which the instruction's result
	/// should eventually be written
	Register reg_dest;

	/// \brief The arguments for this instruction
	Argument args[2];
};

#pragma GCC diagnostic pop

struct InstructionResult {
	enum State {
		/// \brief The `value` field contains the SequenceId of the previous
		/// instruction that wrote to the same register
		UNKNOWN,

		/// \brief The `value` field is set to that produced by the previous
		/// instruction that wrote to the same register
		OLD,

		/// \brief The 'value' field is set to that which was produced by
		/// the instruction being executed
		NEW,
	};

	/// \brief The value produced by the instruction
	Word64 value;

	/// \brief The
	State state;
};

// Disable gcc's warning about anonymous structs in unions temporarily...
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

/// \brief Represents some promise of a value which will be produced in the
/// future as the output of some instruction, or a value which is already
/// known
struct ValuePromise {
	bool ready;
	union {
		/// \brief The actual resolved value, valid once ready is set
	  Word64 value;

		struct {
			/// \brief The offset to apply to the value upon resolution
			s16 offset;

			/// \brief Which instruction is going to produce the value
			SequenceId waiting_for;
		};
	};
};

/// \brief Represents some instruction sitting in the reorder buffer
struct ReorderBufferEntry {

	/// \brief The program counter for this instruction
	u64        pc;

	/// \brief The SequenceId of this instruction
	SequenceId seq_id;

	/// \brief Current state of this entry
	bool dispatched;

	/// \brief The operation to be performed
	Instruction::Opcode opcode;

	/// \brief The destination architectural register that the result
	/// of this instruction will eventually be written to
	Register reg_dest;

	/// \brief The arguments for this instruction
	ValuePromise args[2];

	/// \brief The computed result of this instruction - only valid once COMPLETE
	/// flag is set within STATE
	InstructionResult result;
};
#pragma GCC diagnostic pop

using ReorderBuffer            = xen::RingBuffer<ReorderBufferEntry, true>;
using LastWriteTable           = SequenceId[Register::LAST_GP+1];
using DecodedInstructionBuffer = xen::RingBuffer<DecodedInstruction>;

struct WorkEntry {
	/// \brief The SequenceId of the instruction being processed
	SequenceId seq_id;

	/// Opcode of instruction being processed
	Instruction::Opcode opcode;

	/// \brief LHS argument for the instruction being processed
	Word64 arg_a;

	/// \brief RHS argument for the instruction being processed
	Word64 arg_b;

	/// \brief The result produced by the instruction being executed
	InstructionResult result;

	/// \brief How many more ticks are required to compute the result
	/// of this operation
	int ticks_remaining;
};

/// \brief Execution unit simulates a pipeline of work to be performed
/// We'll just use a RingBuffer of WorkEntrys representing the jobs
/// currently in the pipeline
using ExecutionUnit = xen::StretchyArray<WorkEntry>;

struct ProcessorMetrics {
	/// \brief The ticks elapsed
	u64 tick;

	/// \brief The number of instructions for which exec is completed
	u64 instructions_execed;

	/// \brief The number of instructions for which commit has been completed
	u64 instructions_commited;

	/// \brief Contains counts for the number of ports utilised
	u64 port_utilization[NUM_EXEC_UNITS + 1];

	/// \brief Number of times pipeline was rolled back due to branch misprediction
	u64 pipeline_rollbacks;

	/// \brief Number of times program counter is directly written to
	u64 pc_writes;
};

#endif
