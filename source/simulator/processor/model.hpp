////////////////////////////////////////////////////////////////////////////
//@ * Overview
//@
//@ This file contains various #define's for changing the processor model
//@ (for example type of branch prediction, number of execution units, size of
//@ reorder buffer) as well as enabling and disabling various debugging options
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_PROCESSOR_MODEL_HPP
#define PROCSIM_SIMULATOR_PROCESSOR_MODEL_HPP

#include <cstdio>
#include <procsim/instruction.hpp>

///////////////////////////////////////////////////////////////
//@ * Debugging options

//@ If set then pipeline is ran in order from top to bottom in order to aid
//@ debugging (as it is much easier to follow the execution of a single
//@ instruction by stepping through the code). Note however this isn't realistic
//@ since effectively it means there is zero latency when pipeline is flushed
#define DEBUG_PIPELINE_ORDERING 0

////////////////
//@ The options below enable or disable various debug ~printf~ statements
//@ in different categories to make debugging a particular aspect of the
//@ processor simpler
#if 0
	#define DEBUG_PIPELINE(...) printf(__VA_ARGS__)
#else
	#define DEBUG_PIPELINE(...)
#endif

#if 0
	#define DEBUG_TICK(...) printf(__VA_ARGS__)
#else
	#define DEBUG_TICK(...)
#endif

#if 0
	#define DEBUG_MEM(...) printf(__VA_ARGS__)
#else
	#define DEBUG_MEM(...)
#endif

#define DO_DEBUG_REORDER 0
#define DO_DEBUG_PORTS   0
#define DO_DEBUG_ASM     0
#define DO_DEBUG_BRANCH  0
///////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
//@ * Optimizations
//@
//@ The simulator can support enabling or disabling various optimizations
//@ (note that these are simulated hardware optimizations, such as branch
//@ prediction, some may in fact make the simulator run slower (IE: less
//@ clock cycles per second). These "optimizations" instead aim to reduce
//@ the number of cycles the simulator requires to execute a particular
//@ program

//@ ** Register Renaming and Out of Order Execution
//@
//@ This option allows for controlling whether to allow for multiple in flight
//@ instructions that are targeting the same destination register (in which case
//@ register renaming must be utilised)
//@
//@ A highly related feature, out of order execution, is also controlled here.
//@ If disabled, independent instructions may still be executed in parallel but
//@ no instruction after one which is waiting (eg, arguments not yet available)
//@ can be executed.
//@
//@ The values of this flag and corresponding behaviour is outlined below:
//@
//@ | Dec | Bin | Out of Order Execution | Register Renaming |
//@ |-----|-----|------------------------|-------------------|
//@ |   0 |  00 | \xmark                 | \xmark            |
//@ |   1 |  01 | \xmark                 | \cmark            |
//@ |   2 |  10 | \cmark                 | \xmark            |
//@ |   3 |  11 | \cmark                 | \cmark            |
#ifndef OPT_REGISTER_RENAMING
#define OPT_REGISTER_RENAMING 3
#endif

//@ ** Register Renaming and Out of Order Execution
//@
//@ Whether to do partial pipeline rollbacks, 0 or 1
//@
//@ If disabled mis-predictions are noticed when the commit unit attempts
//@ to commit the side-effects of an instruction. If the PC of the instruction
//@ to be committed does not match that of the architectural PC, then the pipeline
//@ is flushed.
//@ This causes a 3 cycle stall as instructions must be fetched, decoded and
//@ enqueued before any may be dispatched to execution units
//@
//@ If enabled then during writeback if it is noticed that an instruction is
//@ writing to the program counter, and the next item in the reorder buffer has
//@ the wrong PC, then only instructions after the misprediction are removed.
//@ This means instructions preceding the misprediction may continue to execute
//@ during the 3 cycle stall while subsequent instructions are fetched, etc.
#ifndef OPT_PARTIAL_ROLLBACK
#define OPT_PARTIAL_ROLLBACK 1
#endif

//@ ** Pipelined Execution Units
//@
//@ Since some instructions can take multiple cycles sub-pipelining of
//@ execution units is required in order to allow for 1 instruction to be
//@ dispatched to each execution unit on every cycle, as without sub-pipeling
//@ the unit will be "full" of the previously dispatched instruction for
//@ as many cycles as it takes to execute.
//@
//@ Note this is simulated as just a list of instructions in each unit, there
//@ is no actual simulation of sub "steps" for executing a high latency
//@ instruction
#ifndef OPT_PIPELINED_EXEC_UNITS
#define OPT_PIPELINED_EXEC_UNITS 1
#endif

//@ ** Branch Predictor
//@
//@ This flag may be set to one of the following values to control the type of
//@ branch prediction performed by the fetch unit. Implementations may be found
//@ within [[file:branch_predictor.hpp]]
//@
//@ - 0 - *None*
//@   - Increment fetch address by 4 each time
//@ - 1 - *Deterministic Follow*
//@   - Increment by 4, except for instruction of form  "pc = uset [immediate]"
//@     in which case use the immediate
//@ - 2 - *Static Branch Predictor*
//@   - As with 1, but also for instruction "pc = cset [immediate]" assume
//@     taken if immediate is less than current pc, else assume not taken
//@ - 3 - *Dynamic Branch Predictor*
//@   - As with 2, but use saturating 2 bit counter to determine if branch is
//@     taken or not
#ifndef OPT_BRANCH_PREDICTOR
#define OPT_BRANCH_PREDICTOR 3
#endif
///////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
//@ * Processor Model

//@ There exist 3 "presets" of parameters for the processor model
//@
//@ | 0 | scalar       | 1 instruction per cycle at all pipeline stages |
//@ | 1 | superscalar  | "realistic" 6 way superscalar architecture     |
//@ | 2 | stupidscalar |  Practically unlimited pipeline resources      |
#ifndef MODEL_TYPE
#define MODEL_TYPE 1
#endif

//@ The parameters affected are as follows:
//@ | FRONTEND_WIDTH      | Number of instructions to fetch, decode and enqueue per cycle                |
//@ | REORDER_BUFFER_SIZE | Length of the reorder buffer                                                 |
//@ | MEMORY_BUFFER_SIZE  | Number of pending writes that can be tracked before stall required           |
//@ | NUM_EXEC_UNITS      | How many execution units are available                                       |
//@ | DISPATCH_WINDOW     | How many instructions from front of reorder buffer to consider for execution |

//@ ** Scalar
#if MODEL_TYPE == 0

#define FRONTEND_WIDTH       1
#define REORDER_BUFFER_SIZE  8
#define MEMORY_BUFFER_SIZE  32
#define NUM_EXEC_UNITS       1
#define DISPATCH_WINDOW      8

//@ ** Superscalar
#elif MODEL_TYPE == 1

#define FRONTEND_WIDTH        6
#define REORDER_BUFFER_SIZE  96
#define MEMORY_BUFFER_SIZE   32
#define NUM_EXEC_UNITS        6
#define DISPATCH_WINDOW      64

//@ ** Stupid Scalar
#elif MODEL_TYPE == 2

#define FRONTEND_WIDTH        512
#define REORDER_BUFFER_SIZE  1024
#define MEMORY_BUFFER_SIZE    512
#define DISPATCH_WINDOW       256
#define NUM_EXEC_UNITS        128

#endif
///////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
//@ * Instruction Latencies
//@
//@ Table of latencies to use for each instruction opcode

#if 1
const u64 INSTRUCTION_LATENCIES[Instruction::Opcode::COUNT] = {
	// "halt",
	0,

	//"sadd", "uadd", "fadd",
	//"ssub", "usub", "fsub",
	//"smul", "umul", "fmul",
	//"sdiv", "udiv", "fdiv",
	0, 0, 1,
	0, 0, 1,
	2, 2, 2,
	2, 2, 2,

	//"shl", "shr",
	0, 0,

	//"and", "nand", "or", "nor", "xor", "xnor", "inv",
	0, 0, 0, 0, 0, 0, 0,

	//"ifeq", "ifne",
	//"ifule", "iful", "ifuge", "ifug",
	//"ifsle", "ifsl", "ifsge", "ifsg",
	//"iffle", "iffl", "iffge", "iffg",
	0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	1, 1, 1, 1,

	//"uset", "sset", "fset",
	0, 0, 0,

	//"cset"
	0,

	//"load", "stora", "storb",
	4, 4, 4,

	//"ftos", "stof",
	1, 1,
};
#else
const u64 INSTRUCTION_LATENCIES[Instruction::Opcode::COUNT] = {
	// "halt",
	0,

	//"sadd", "uadd", "fadd",
	//"ssub", "usub", "fsub",
	//"smul", "umul", "fmul",
	//"sdiv", "udiv", "fdiv",
	0, 0, 0,
	0, 0, 0,
	0, 0, 0,
	0, 0, 0,

	//"shl", "shr",
	0, 0,

	//"and", "nand", "or", "nor", "xor", "xnor", "inv",
	0, 0, 0, 0, 0, 0, 0,

	//"ifeq", "ifne",
	//"ifule", "iful", "ifuge", "ifug",
	//"ifsle", "ifsl", "ifsge", "ifsg",
	//"iffle", "iffl", "iffge", "iffg",
	0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,

	//"uset", "sset", "fset",
	0, 0, 0,

	//"cset"
	0,

	//"load", "stora", "storb",
	0, 0, 0,

	//"ftos", "stof",
	0, 0,
};
#endif
///////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
//@ * Port Capabilities
//@
//@ Each port of the processor may execute different subsets of the instruction
//@ set, as defined here
#if NUM_EXEC_UNITS == 6 && FRONTEND_WIDTH == 6

const constexpr u64 EXEC_UNIT_CAPABILITIES[NUM_EXEC_UNITS] = {
	OPCODE_TYPE_ALU | OPCODE_TYPE_CMP,
	OPCODE_TYPE_ALU | OPCODE_TYPE_CMP,
	OPCODE_TYPE_ALU | OPCODE_TYPE_FPU,
	OPCODE_TYPE_ALU | OPCODE_TYPE_FPU,
	OPCODE_TYPE_LSU,
	OPCODE_TYPE_LSU,
};

#else

const constexpr u64 EXEC_UNIT_CAPABILITIES[] = {
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
	OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL, OPCODE_TYPE_ALL,
};

#endif

#endif
