////////////////////////////////////////////////////////////////////////////
/// \brief Contains the interface to the memory controller
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_MEMORYCONTROLLER_HPP
#define PROCSIM_SIMULATOR_MEMORYCONTROLLER_HPP

#include <xen/core/array.hpp>
#include <procsim/types.hpp>
#include "types.hpp"
#include "model.hpp"

struct System;

struct PendingMemoryWrite {
	/// \brief The sequence id of the instruction doing the write
	SequenceId seq_id;

	/// \brief Which address will be written to
	u64 address;

	/// \brief The value to be written
	Word64 value;

	/// \brief Whether the value to be written is known yet
	bool value_known;
};

/////////////////////////////////////////////////////////////////////
/// \brief The MemoryController is a on processor die buffer which stores
/// any speculatively executed memory operations that can then either be
/// committed or rolled back at a later point
/////////////////////////////////////////////////////////////////////
struct MemoryController {
	System* system;

	PendingMemoryWrite _pending_writes_buffer[MEMORY_BUFFER_SIZE];

	xen::StretchyArray<PendingMemoryWrite> pending_writes;
};

/////////////////////////////////////////////////////////////////////
/// \brief Initialises some MemoryController
/////////////////////////////////////////////////////////////////////
void memctlInit(System* system, MemoryController& memctl);

/////////////////////////////////////////////////////////////////////
/// \brief Marks a memory location as being written by some instruction
/// but for which we do not yet know the actual value that will be written
/////////////////////////////////////////////////////////////////////
bool memctlMark(MemoryController& ctrl, SequenceId seq_id, u64 address);

/////////////////////////////////////////////////////////////////////
/// \brief Writes some data to some memory location
/////////////////////////////////////////////////////////////////////
void memctlWrite(MemoryController& ctrl, SequenceId seq_id, u64 address, Word64 data);

/////////////////////////////////////////////////////////////////////
/// \brief Attempts to read some memory location, writing the result into
/// passed data object
/// Returns false (and makes no change to data) if there is a pending write
/// operation, but we do not yet know what the value to be written is
/////////////////////////////////////////////////////////////////////
bool memctlRead(MemoryController& ctrl, SequenceId seq_id, u64 address, Word64& data);

/////////////////////////////////////////////////////////////////////
/// \brief Commits all buffered memory operations occurring on or before
/// some sequence id
/////////////////////////////////////////////////////////////////////
void memctlCommit(MemoryController& ctrl, SequenceId seq_id);

/////////////////////////////////////////////////////////////////////
/// \brief Discards all buffered memory operations occurring on or after
/// some sequence id
/////////////////////////////////////////////////////////////////////
void memctlDiscard(MemoryController& ctrl, SequenceId seq_id);


#endif
