////////////////////////////////////////////////////////////////////////////
/// \brief Contains the interface to the memory controller
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_MEMORYCONTROLLER_CPP
#define PROCSIM_SIMULATOR_MEMORYCONTROLLER_CPP

#include "memory_controller.hpp"
#include "../device.hpp"

void memctlInit(System* system, MemoryController& memctl) {
	memctl.system = system;

	memctl.pending_writes.capacity = XenArrayLength(memctl._pending_writes_buffer);
	memctl.pending_writes.elements = memctl._pending_writes_buffer;
}

bool memctlMark(MemoryController& ctrl, SequenceId seq_id, u64 address){
	for(int i = 0; i < xen::size(ctrl.pending_writes); ++i){
		PendingMemoryWrite* it = &ctrl.pending_writes[i];
		if(it->seq_id == seq_id && it->address == address){
			// Then this read was already marked
			return true;
		}
	}


	if(!xen::hasSpace(ctrl.pending_writes)){
		DEBUG_MEM("No space to place mark on %p for %lu\n", address, seq_id);
		return false;
	}

	DEBUG_MEM("Marking %p for %lu\n", address, seq_id);

	xen::pushBack(ctrl.pending_writes, { seq_id, address, 0, false });
	return true;
}

void memctlWrite(MemoryController& ctrl, SequenceId seq_id, u64 address, Word64 data){
	for(int i = 0; i < xen::size(ctrl.pending_writes); ++i){
		PendingMemoryWrite* it = &ctrl.pending_writes[i];
		if(it->seq_id == seq_id && it->address == address){
			DEBUG_MEM("Setting write value for previous mark %p for %lu, value: %p\n",
			          address, seq_id, (void*)data.u);
			it->value       = data;
			it->value_known = true;
			return;
		}
	}

	XenAssert(false, "Memory must be marked before it may be written");
}

bool memctlRead(MemoryController& ctrl, SequenceId seq_id, u64 address, Word64& data){
	for(int i = 0; i < xen::size(ctrl.pending_writes); ++i){
		PendingMemoryWrite* it = &ctrl.pending_writes[i];
		if(it->address == address){
			// If this is a load before the first write then read as normal
			if(seq_id < it->seq_id){ continue; }

			// ...otherwise intercept the read request
			if(!it->value_known){
				DEBUG_MEM("Attempted read on marked memory %p but not value available\n", address);
				return false;
			}
			DEBUG_MEM("Intercepted read for marked memory %p\n", address);
			data = it->value;
			return true;
		}
	}

	DEBUG_MEM("Passing through memory reading for %p\n", address);
	data = readMemory<Word64>(ctrl.system, address);
	return true;
}

void memctlCommit(MemoryController& ctrl, SequenceId seq_id){
	PendingMemoryWrite& op = xen::peakFront<PendingMemoryWrite>(ctrl.pending_writes);

	while(xen::size(ctrl.pending_writes) && op.seq_id <= seq_id){
		DEBUG_MEM("Committing memory write to %p, value: %p\n", op.address, (void*)op.value.u);
		XenAssert(op.value_known, "Value of write operation must be known by the time it is committed!");
		writeMemory<Word64>(ctrl.system, op.address, op.value);
		xen::popFront(ctrl.pending_writes);
		op = xen::peakFront<PendingMemoryWrite>(ctrl.pending_writes);
	}
}

void memctlDiscard(MemoryController& ctrl, SequenceId seq_id){
	for(int i = 0; i < xen::size(ctrl.pending_writes); ++i){
		if(ctrl.pending_writes[i].seq_id >= seq_id){
			DEBUG_MEM("Discarding memory write to %p\n", xen::peakBack(ctrl.pending_writes).address);
			xen::remove(ctrl.pending_writes, i);
			--i;
		}
	}
}


#endif
