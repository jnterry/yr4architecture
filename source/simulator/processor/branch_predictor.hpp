////////////////////////////////////////////////////////////////////////////
/// \brief Contains the interface to the branch predictor
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_BRANCHPREDICTOR_HPP
#define PROCSIM_SIMULATOR_BRANCHPREDICTOR_HPP

#include <xen/core/intrinsics.hpp>
#include "model.hpp"

struct BranchPredictor;

/////////////////////////////////////////////////////////////////////
/// \brief Retrieves the predicted next PC value for some current PC
/// and instruction
/////////////////////////////////////////////////////////////////////
u64 branchGetPrediction(BranchPredictor& pred, u64 cur_pc, Instruction& ins);

/////////////////////////////////////////////////////////////////////
/// \brief Notifies the branch predictor of the actual next_pc taken
/// for some current pc
/////////////////////////////////////////////////////////////////////
void branchNotifyNextPc(BranchPredictor& pred, u64 cur_pc, u64 next_pc);

////////////////////////////////////////////////

#if OPT_BRANCH_PREDICTOR >= 0 && OPT_BRANCH_PREDICTOR <= 2

struct BranchPredictor { };

#elif OPT_BRANCH_PREDICTOR == 3

struct BranchPredictorCounter {
	enum State : s08 {
		STRONGLY_NOT_TAKEN = -2,
		WEAKLY_NOT_TAKEN   = -1,
		WEAKLY_TAKEN       =  0,
		STRONGLY_TAKEN     =  1,

		MIN             = STRONGLY_NOT_TAKEN,
		MAX             = STRONGLY_TAKEN,
		TAKEN_THRESHOLD = WEAKLY_TAKEN,
	};

	u64  pc;
  u16  age;
	s08  value;
	bool in_use;
};

struct BranchPredictor {
	BranchPredictorCounter slots[32];
};

#else
#error "OPT_BRANCH_PREDICTOR must be assigned a value between 0 and 3"
#endif

#endif
