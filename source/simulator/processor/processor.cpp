////////////////////////////////////////////////////////////////////////////
/// \brief Main implementation file for the processor device
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_SIMULATOR_CPP
#define PROCSIM_SIMULATOR_SIMULATOR_CPP

#include "utilities.cpp"

#include "../processor.hpp"
#include <procsim/register.hpp>
#include "memory_controller.hpp"
#include "branch_predictor.hpp"
#include "model.hpp"
#include "types.hpp"

#include <xen/core/memory/utilities.hpp>
#include <xen/core/memory/ArenaLinear.hpp>
#include <xen/core/File.hpp>

#include <cstdlib>
#include <limits>

#include <sys/stat.h>
 #include <errno.h>

/// \brief Full state of the processor
struct ProcessorState {
	/// \brief The current state of the architectural registers of the processor
	RegisterFile regs;

	/// \brief Full state of the fetch unit
	FetchUnit fetch;

	/// \brief Set of decoded instructions that have not yet been enqueued
	/// into the Reorder Buffer
	DecodedInstructionBuffer decoded_instruction_buffer;

	/// \brief Highest sequence id of an instruction in the reorder buffer that
	/// is going to write to each register. This is used for register renaming
	/// in order to map from an architectural register to the entry in the
	/// reorder buffer which will eventually contain the value
  LastWriteTable last_write_table;

	/// \brief In flight instructions that have been queued for execution, but may
	/// be in various states:
	/// - Waiting for arguments to become available
	/// - Dispatched to some exec unit, waiting for result to be produced
	/// - Result has been produced, waiting for commit
	ReorderBuffer reorder_buffer;

	/// \brief The execution units of the processor
	ExecutionUnit exec_units[NUM_EXEC_UNITS];

	/// \brief The memory controller for the processor which should be used to
	/// coordinate all access to memory in order to ensure ordering is consistent
	MemoryController memctl;

	/// \brief The current state of the branch predictor
	BranchPredictor branch_predictor;

	/// \brief Debug metrics concerning processor performance
	ProcessorMetrics metrics;
};

void rollbackPipeline(ProcessorState& proc, SequenceId next_seq_id, u64 next_pc) {
	++proc.metrics.pipeline_rollbacks;

	memctlDiscard(proc.memctl, next_seq_id-1);

	while(!xen::isEmpty(proc.fetch.buffer) &&
	      xen::peakBack(proc.fetch.buffer).seq_id >= next_seq_id){
		xen::popBack(proc.fetch.buffer);
	}

	while(!xen::isEmpty(proc.decoded_instruction_buffer) &&
	      xen::peakBack(proc.decoded_instruction_buffer).seq_id >= next_seq_id){
		xen::popBack(proc.decoded_instruction_buffer);
	}

	while(!xen::isEmpty(proc.reorder_buffer) &&
	      xen::peakBack(proc.reorder_buffer).seq_id >= next_seq_id){
		xen::popBack(proc.reorder_buffer);
	}

	if(xen::isEmpty(proc.reorder_buffer)){
		// Pipeline is empty so we can reset the sequence_id for free
		// :TODO: enable
		//next_seq_id = 1;

		proc.reorder_buffer.first_index = next_seq_id;
	}

	proc.fetch.next_pc     = next_pc;
	proc.fetch.next_seq_id = next_seq_id;
	proc.fetch.halted      = false;

	xen::clearToZero(&proc.last_write_table[0], sizeof(proc.last_write_table));
	for(auto it = xen::iterateFront(proc.reorder_buffer); it; ++it){
		if(it->reg_dest < XenArrayLength(proc.last_write_table)){
			proc.last_write_table[it->reg_dest] = it->seq_id;
		}
	}

	for(int i = 0; i < NUM_EXEC_UNITS; ++i){
		for(int j = 0; j < proc.exec_units[i].size; ++j){
			if(proc.exec_units[i][j].seq_id >= next_seq_id){
				xen::remove(proc.exec_units[i], j);
				--j;
			}
		}
	}
}

void flushPipeline(ProcessorState& proc, SequenceId next_seq_id, u64 next_pc){
	DEBUG_PIPELINE("### FLUSH PIPELINE, NEXT SEQ ID: %lu, NEXT PC: %lu ###\n", next_seq_id, next_pc);

	rollbackPipeline(proc, next_seq_id, next_pc);

	proc.regs[Register::PC].u = next_pc;
}

bool initProcessor(System* system, Device* device, const char* memory_file){
	u64 mem_size = xen::megabytes(4);

	xen::ArenaLinear arena(malloc(mem_size), mem_size);
	xen::clearToZero(arena.start, mem_size);

	ProcessorState* proc = xen::reserveType<ProcessorState>(arena);
	device->state = proc;

	u64 device_memory_size = device->memory_end - device->memory_start;
	xen::clearToZero(device->memory, device_memory_size);

	if(memory_file != nullptr) {
		xen::ArenaLinear memory_arena(device->memory, device_memory_size);
		xen::FileData fdata = xen::loadFileAndNullTerminate(memory_arena, memory_file);
		if(fdata.size == 0){
			printf("Failed to load initial memory contents from file: %s\n", memory_file);
			return false;
		}
	}

	memctlInit(system, proc->memctl);

	proc->fetch.buffer.elements    = xen::reserveTypeArray<LoadedInstruction>(arena, FRONTEND_WIDTH);
	proc->fetch.buffer.capacity    = FRONTEND_WIDTH;

	proc->decoded_instruction_buffer.elements   = xen::reserveTypeArray<DecodedInstruction>(arena, FRONTEND_WIDTH);
	proc->decoded_instruction_buffer.capacity   = FRONTEND_WIDTH;

	proc->reorder_buffer.elements = xen::reserveTypeArray<ReorderBufferEntry>(arena, REORDER_BUFFER_SIZE);
	proc->reorder_buffer.capacity = REORDER_BUFFER_SIZE;

	u64 max_latency = 1;
	#if OPT_PIPELINED_EXEC_UNITS
	for(int i = 0; i < Instruction::Opcode::COUNT; ++i){
		max_latency = xen::max(max_latency, INSTRUCTION_LATENCIES[i]+1);
	}
	#endif
	for(int i = 0; i < NUM_EXEC_UNITS; ++i){
		proc->exec_units[i].elements = xen::reserveTypeArray<WorkEntry>(arena, max_latency);
		proc->exec_units[i].capacity = max_latency;
	}

	// reset the pipeline state to ensure everything has been setup correctly
  flushPipeline(*proc, 1, 0);

  // ensure nothing we have done has affected the metrics (like a pipeline flush)
  xen::clearToZero(&proc->metrics);

	return true;
}

void shutdownProcessor(Device* device, const char* memory_file_name){
	ProcessorState* proc = (ProcessorState*)device->state;

	printf("=======================================\n");
	printf("Processor has halted, final pc: %lu\n", proc->regs[Register::PC].u);
  printf("=======================================\n");
  printProcessorMetrics(proc->metrics);
	printf("=======================================\n");
	printf("Final register state:\n");
	for(int i = 0; i < Register::COUNT; ++i){
		printWord(i, proc->regs[i]);
	}
	printf("=======================================\n");

	printf("Data after program:\n");
	u64 data_base = proc->regs[Register::PC].u;
	for(int i = 0; i < 30; ++i){
		Word64 word = readMemory<Word64>(device->system, data_base + i*8);
		printWord(i, word);
	}
	printf("=======================================\n");

	const char* c;
	for(c = memory_file_name; *c != '\0'; ++c){
		if(*c == '/'){
			memory_file_name = &c[1];
		}
	}

	char stat_file_name[512];
	snprintf(stat_file_name, 512,
	         "stats/%s", memory_file_name);
	int dir_res = mkdir(stat_file_name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if(dir_res != 0 && errno != EEXIST){
		fprintf(stderr, "FAILED TO CREATE DIRECTORY FOR STATS CSV: %s\n", stat_file_name);
		free(device->state);
		return;
	}
	snprintf(stat_file_name, 512,
	         "stats/%s/%i_%i_%i_%i_%i_%i_%i_%i_%i.csv",
	         memory_file_name,
	         FRONTEND_WIDTH,
	         REORDER_BUFFER_SIZE,
	         MEMORY_BUFFER_SIZE,
	         NUM_EXEC_UNITS,
	         DISPATCH_WINDOW,
	         OPT_REGISTER_RENAMING,
	         OPT_PARTIAL_ROLLBACK,
	         OPT_PIPELINED_EXEC_UNITS,
	         OPT_BRANCH_PREDICTOR
	        );

	FILE* fout = fopen(stat_file_name, "w");
	if(fout == nullptr){
		fprintf(stderr, "FAILED TO OPEN STATS CSV: %s\n", stat_file_name);
		free(device->state);
		return;
	}

	fprintf(fout, "ticks,%lu\n", proc->metrics.tick);

	fprintf(fout, "instructions(exed/commit/waste),%lu,%lu,%lu\n",
	        proc->metrics.instructions_execed,
	        proc->metrics.instructions_commited,
	        proc->metrics.instructions_execed - proc->metrics.instructions_commited
	       );

	fprintf(fout, "port_utilisation");
	for(int i = 0; i < NUM_EXEC_UNITS+1; ++i){
		fprintf(fout, ",%lu", proc->metrics.port_utilization[i]);
	}
	fprintf(fout, "\n");

	fprintf(fout, "branches (total/missed),%lu,%lu\n",
	        proc->metrics.pc_writes,
	        proc->metrics.pipeline_rollbacks
	       );
	fclose(fout);

	free(device->state);
}

void evaluateOpcode(SequenceId seq_id,
                    MemoryController& memctl,
                    InstructionResult& result,
                    Instruction::Opcode opcode,
                    const Word64 arg_a,
                    const Word64 arg_b
                   ){

	InstructionResult::State old_state = result.state;
	result.state = InstructionResult::NEW;

	switch(opcode){
	case Instruction::HALT:
		// Nothing to do here, halt is done as part of commit
		// since then we guarantee it occurs in order
		break;

	case Instruction::SADD: result.value.s = arg_a.s + arg_b.s; break;
	case Instruction::UADD: result.value.u = arg_a.u + arg_b.u; break;
	case Instruction::FADD: result.value.f = arg_a.f + arg_b.f; break;

	case Instruction::SSUB: result.value.s = arg_a.s - arg_b.s; break;
	case Instruction::USUB: result.value.u = arg_a.u - arg_b.u; break;
	case Instruction::FSUB: result.value.f = arg_a.f - arg_b.f; break;

	case Instruction::SMUL: result.value.s = arg_a.s * arg_b.s; break;
	case Instruction::UMUL: result.value.u = arg_a.u * arg_b.u; break;
	case Instruction::FMUL: result.value.f = arg_a.f * arg_b.f; break;

		// Simulated processor may execute div instructions speculatively and then
		// get a divide by 0 issue -> we don't want to crash, and our simulated
		// processor doesn't have interrupts to raise in this event, so just
		// set to well defined value
	case Instruction::SDIV:
		if(arg_b.s == 0) {
			result.value.s = (2^31)-1;
		} else {
			result.value.s = arg_a.s / arg_b.s;
		}
		break;
	case Instruction::UDIV:
		if(arg_b.u == 0) {
			result.value.u = ~0;
		} else {
			result.value.u = arg_a.u / arg_b.u;
		}
		break;
	case Instruction::FDIV:
		if(arg_b.f == 0.0) {
			result.value.f = std::numeric_limits<double>::quiet_NaN();
		} else {
			result.value.f = arg_a.f / arg_b.f;
		}
		break;

	case Instruction::SHL: result.value.u  = arg_a.u << arg_b.u; break;
	case Instruction::SHR: result.value.u  = arg_a.u >> arg_b.u; break;

	case Instruction::AND : result.value.u =  (arg_a.u & arg_b.u); break;
	case Instruction::NAND: result.value.u = ~(arg_a.u & arg_b.u); break;
	case Instruction::OR  : result.value.u =  (arg_a.u | arg_b.u); break;
	case Instruction::NOR : result.value.u = ~(arg_a.u | arg_b.u); break;
	case Instruction::XOR : result.value.u =  (arg_a.u ^ arg_b.u); break;
	case Instruction::XNOR: result.value.u = ~(arg_a.u ^ arg_b.u); break;
	case Instruction::INV : result.value.u = ~(arg_b.u); break;

	case Instruction::IFEQ : result.value.u = (arg_a.u == arg_b.u ? 1 : 0); break;
	case Instruction::IFNE : result.value.u = (arg_a.u != arg_b.u ? 1 : 0); break;

	case Instruction::IFULE: result.value.u = (arg_a.u <= arg_b.u ? 1 : 0); break;
	case Instruction::IFUL : result.value.u = (arg_a.u <  arg_b.u ? 1 : 0); break;
	case Instruction::IFUGE: result.value.u = (arg_a.u >= arg_b.u ? 1 : 0); break;
	case Instruction::IFUG : result.value.u = (arg_a.u >  arg_b.u ? 1 : 0); break;

	case Instruction::IFSLE: result.value.u = (arg_a.s <= arg_b.s ? 1 : 0); break;
	case Instruction::IFSL : result.value.u = (arg_a.s <  arg_b.s ? 1 : 0); break;
	case Instruction::IFSGE: result.value.u = (arg_a.s >= arg_b.s ? 1 : 0); break;
	case Instruction::IFSG : result.value.u = (arg_a.s >  arg_b.s ? 1 : 0); break;

	case Instruction::IFFLE: result.value.u = (arg_a.f <= arg_b.f ? 1 : 0); break;
	case Instruction::IFFL : result.value.u = (arg_a.f <  arg_b.f ? 1 : 0); break;
	case Instruction::IFFGE: result.value.u = (arg_a.f >= arg_b.f ? 1 : 0); break;
	case Instruction::IFFG : result.value.u = (arg_a.f >  arg_b.f ? 1 : 0); break;

	case Instruction::LOAD:
		if(!memctlRead(memctl, seq_id, arg_a.u + arg_b.u, result.value)){
			result.state = old_state;
		}
		break;
	case Instruction::STORA :
		memctlWrite(memctl, seq_id, arg_b.u, arg_a);
		result.value.u = arg_b.u + 8;
		break;
	case Instruction::STORB:
		memctlWrite(memctl, seq_id, arg_a.u, arg_b);
		result.value.u = arg_a.u + 8;
		break;

	case Instruction::USET:
	case Instruction::SSET:
	case Instruction::FSET:
		result.value = arg_b;
		break;
	case Instruction::CSET:
		XenAssert(old_state == InstructionResult::OLD,
		          "cset instructions can only be evaluated once old value is known"
		         );
		if(arg_a.u) { result.value = arg_b; }
		break;
	case Instruction::FTOS: result.value.s = (u64)arg_b.f; break;
	case Instruction::STOF: result.value.f = (r64)arg_b.s; break;

	default:
		XenInvalidCodePath("Unhandled opcode");
		break;
	}
}

void phaseFetch(System* system, BranchPredictor& predictor, FetchUnit& fetch){
	for(int i = 0;
	    (i < FRONTEND_WIDTH &&
	     fetch.buffer.size < fetch.buffer.capacity &&
	     !fetch.halted
	    );
	    ++i
	    ){

		DEBUG_PIPELINE("Fetching %lu from address %lu\n",
		               fetch.next_seq_id, fetch.next_pc);

	  LoadedInstruction li;
		li.seq_id          = fetch.next_seq_id;
		li.pc              = fetch.next_pc;
		li.instruction.raw = readMemory<u32>(system, fetch.next_pc);

		fetch.halted = li.instruction.opcode == Instruction::HALT;

		xen::pushBack(fetch.buffer, li);

		fetch.next_pc = branchGetPrediction(predictor, fetch.next_pc, li.instruction);
	  ++fetch.next_seq_id;
	}
}

void phaseDecode(FetchedInstructionBuffer& fib, DecodedInstructionBuffer& dib) {
	for(int i = 0;
	    i < NUM_EXEC_UNITS && !xen::isEmpty(fib) && xen::hasSpace(dib);
	    ++i
	    ){

		LoadedInstruction li = xen::popFront(fib);

		DEBUG_PIPELINE("Decoding %lu\n", li.seq_id);

		// Perform instruction decode, this is pretty much just a case
		// of unpacking the dense 32 bit instruction into a wider
		// format, taking particular care to perform any necessary
		// sign extensions for immediate/offset values when the number
		// of bits are increased

		DecodedInstruction di;
		xen::clearToZero(&di);

		di.pc       = li.pc;
		di.seq_id   = li.seq_id;
		di.opcode   = (Instruction::Opcode)li.instruction.opcode;
		di.reg_dest = (Register)li.instruction.rdest;

		XenAssert(di.opcode < Instruction::Opcode::COUNT, "Expected valid opcode");

		switch(li.instruction.source_a){
		case Register::RNULL:
			di.args[0].is_immediate = true;
			di.args[0].immediate.u  = 0;
			break;
		case Register::PC:
			di.args[0].is_immediate = true;
			di.args[0].immediate.u  = li.pc;
			break;
		default:
			di.args[0].is_immediate    = false;
			di.args[0].offset          = 0;
			di.args[0].source_register = (Register)li.instruction.source_a;
		}

		switch(li.instruction.source_b){
		case Register::RNULL:
			di.args[1].is_immediate = true;
			di.args[1].immediate.s  = 0 + getSignExtendedOffset(li.instruction);
			break;
		case Register::PC:
			di.args[1].is_immediate = true;
			di.args[1].immediate.u  = li.pc + getSignExtendedOffset(li.instruction);
			break;
		default:
			if(li.instruction.b_is_reg){
				di.args[1].is_immediate    = false;
				di.args[1].offset          = getSignExtendedOffset(li.instruction);
				di.args[1].source_register = (Register)li.instruction.source_b;
			} else {
				di.args[1].is_immediate = true;
				di.args[1].immediate    = getSignExtendedImmediate(li.instruction);
			}
		}

		xen::pushBack(dib, di);
	}
}

void phaseEnqueue(DecodedInstructionBuffer& dib,
                  RegisterFile&             regs,
                  LastWriteTable&           last_writes,
                  ReorderBuffer&            rob){
	for(int i = 0;
	    i < FRONTEND_WIDTH && !xen::isEmpty(dib) && xen::hasSpace(rob);
	    ++i
	   ){

		#if (OPT_REGISTER_RENAMING & 1) == 0
		// We simulate no register renaming by just refusing to enqueue while there
		// is an outstanding write to the same destination
		DecodedInstruction& di_peak = xen::peakFront(dib);
		if(di_peak.reg_dest < XenArrayLength(last_writes) &&
		   last_writes[di_peak.reg_dest] != 0){
			DEBUG_PIPELINE("Cannot enqueue %lu or subsequent instructions due to contention over destination register %s\n",
			               di_peak.seq_id, REGISTER_NAMES[di_peak.reg_dest]);
			return;
		}
		#endif

		DecodedInstruction di = xen::popFront(dib);

		///////////////////////////////////////////////////////////////////
		// Do the enqueue (generating a ReorderBufferEntry)
		DEBUG_PIPELINE("Enqueuing %lu\n", di.seq_id);

		ReorderBufferEntry rbe;
		xen::clearToZero(&rbe);

		//////////////////////////////////////
		// Copy over common data
		rbe.pc       = di.pc;
		rbe.seq_id   = di.seq_id;
		rbe.opcode   = di.opcode;
		rbe.reg_dest = di.reg_dest;

		//////////////////////////////////////
		// Setup source arguments
		for(int i = 0; i < 2; ++i){
			Instruction::ArgType arg_type = Instruction::ARG_TYPES[i][di.opcode];
			if(arg_type == Instruction::ArgType::NONE){
				rbe.args[i].ready = true;
			} else if(di.args[i].is_immediate){
				rbe.args[i].value = di.args[i].immediate;
				rbe.args[i].ready = true;
			} else {
				Register source_reg = di.args[i].source_register;
				XenAssert(source_reg <= Register::LAST_GP, "Must be GP register");

				rbe.args[i].offset      = di.args[i].offset;
				rbe.args[i].waiting_for = source_reg < XenArrayLength(last_writes) ? last_writes[source_reg] : 0;

				if(rbe.args[i].waiting_for == 0){
					// then no in flight instructions that will write to this register,
					// fetch the value immediately
					resolveValuePromise(arg_type, rbe.args[i], regs[source_reg]);
				} else if (rob[rbe.args[i].waiting_for].result.state == InstructionResult::NEW){
					// Then the dependent instruction has been completed, use its result
					resolveValuePromise(arg_type, rbe.args[i], rob[rbe.args[i].waiting_for].result.value);
				} else {
					// Then the dependent instruction is still in flight
					rbe.args[i].ready = false;
				}
			}
		}
		//////////////////////////////////////

		//////////////////////////////////////
		// Setup result
		switch(rbe.reg_dest){
		case Register::RNULL:
			rbe.result.value.u = 0;
			rbe.result.state   = InstructionResult::OLD;
			break;
		case Register::PC:
			rbe.result.value.u = rbe.pc + 4;
			rbe.result.state   = InstructionResult::OLD;
			break;
		default: {
			SequenceId waiting_for = di.reg_dest < XenArrayLength(last_writes) ? last_writes[di.reg_dest] : 0;
			if(waiting_for == 0){
				rbe.result.value = regs[rbe.reg_dest];
				rbe.result.state = InstructionResult::OLD;
			} else {
				switch(rob[waiting_for].result.state){
				case InstructionResult::NEW:
					// Then the dependent instruction has been completed, use its result
					rbe.result.value = rob[waiting_for].result.value;
					rbe.result.state = InstructionResult::OLD;
					break;
				default:
					// Then instruction result has not yet been completed
					rbe.result.value.u = waiting_for;
					rbe.result.state   = InstructionResult::UNKNOWN;
					break;
				}
			}
			break;
		}
		}
		//////////////////////////////////////

		// Update the last write for the destination register
		if(di.reg_dest < XenArrayLength(last_writes)){
			last_writes[di.reg_dest] = rbe.seq_id;
		}

		xen::pushBack(rob, rbe);
	}
}

void phaseDispatch(ProcessorMetrics& metrics,
                   MemoryController& memctl,
                   ReorderBuffer& rob,
                   ExecutionUnit exec_units[NUM_EXEC_UNITS]){

	u64 available_count = 0;
	bool port_used[NUM_EXEC_UNITS] = {0};
	for(u64 i = 0; i < NUM_EXEC_UNITS; ++i){
		if(xen::hasSpace(exec_units[i])){
			++available_count;
		} else {
			port_used[i] = true;
		}
	}

	const u64 original_available_count = available_count;

	bool mem_store_skipped = false;

	#if (OPT_REGISTER_RENAMING & 2) == 0
	#define SKIP_ENTRY break    // disable out-of-order execuction
	#else
	#define SKIP_ENTRY continue // enable out-of-order execution
	#endif

	for(auto rbe = xen::iterateFront(rob);
	    ( rbe &&
	      rbe - xen::iterateFront(rob) < DISPATCH_WINDOW &&
	      available_count
	    );
	    ++rbe
	   ){

		if(rbe->dispatched) { SKIP_ENTRY; }

		if(rbe->opcode == Instruction::STORA){
			if(!rbe->args[1].ready){
				mem_store_skipped = true;
			} else if(!memctlMark(memctl, rbe->seq_id, rbe->args[1].value.u)){
				DEBUG_PIPELINE("Cannot dispatch %lu since memory controller is full\n", rbe->seq_id);
				mem_store_skipped = true;
			  SKIP_ENTRY;
			}
		} else if(rbe->opcode == Instruction::STORB){
			if(!rbe->args[0].ready){
				mem_store_skipped = true;
			} else if(!memctlMark(memctl, rbe->seq_id, rbe->args[0].value.u)){
				DEBUG_PIPELINE("Cannot dispatch %lu since memory controller is full\n", rbe->seq_id);
				mem_store_skipped = true;
			  SKIP_ENTRY;
			}
		} else if(rbe->opcode == Instruction::LOAD && mem_store_skipped){
			DEBUG_PIPELINE("Cannot dispatch %lu as load op and a preceding store has not been marked yet\n", rbe->seq_id);
		  SKIP_ENTRY;
		}

		if(!rbe->args[0].ready || !rbe->args[1].ready){
			DEBUG_PIPELINE("Cannot dispatch %lu since arguments not ready\n", rbe->seq_id);
			continue;
		}

		if(Instruction::MAY_NOOP[rbe->opcode] &&
		   rbe->result.state == InstructionResult::UNKNOWN){
			DEBUG_PIPELINE("Cannot dispatch %lu since may no-op and old result not known\n", rbe->seq_id);
			SKIP_ENTRY;
		}

		int unit_index = -1;
		for(int i = 0; i < NUM_EXEC_UNITS; ++i){
			// check if already issued to this port this cycle
			if(port_used[i]){ continue; }

			// check if port supports the instruction we are interested in
			if(EXEC_UNIT_CAPABILITIES[i] & (1l << rbe->opcode)){
				unit_index = i;
				port_used[i] = true;
				break;
			}
		}
		if(unit_index < 0){
			DEBUG_PIPELINE("Cannot dispatch %lu as no available port for %s\n",
			               rbe->seq_id, Instruction::NAMES[rbe->opcode]);
			SKIP_ENTRY;
		}

		DEBUG_PIPELINE("Dispatching %lu to port %3i\n", rbe->seq_id, unit_index);

		rbe->dispatched = true;
		--available_count;

		////////////////////////////////////////////////
		// Setup the work entry
		WorkEntry work;
		work.seq_id          = rbe->seq_id;
		work.opcode          = rbe->opcode;
		work.arg_a           = rbe->args[0].value;
		work.arg_b           = rbe->args[1].value;
		work.ticks_remaining = INSTRUCTION_LATENCIES[rbe->opcode];
		work.result          = rbe->result;
		xen::pushBack(exec_units[unit_index], work);
	}

	///////////////////////////////////////
	// Update port utilisation metric
	int use_count = original_available_count - available_count;
	++metrics.port_utilization[use_count];
}

void phaseExecute(ProcessorState&   proc,
                  ExecutionUnit     exec_units[NUM_EXEC_UNITS]
                 ){
	for(u08 unit_index = 0; unit_index < NUM_EXEC_UNITS; ++unit_index){
		ExecutionUnit& unit = exec_units[unit_index];
		for(u64 i = 0; i < xen::size(unit); ++i){
			WorkEntry& work = unit[i];

			if(work.ticks_remaining > 0){ --work.ticks_remaining; }

			if(work.result.state != InstructionResult::NEW){
				evaluateOpcode(work.seq_id, proc.memctl,
				               work.result,
				               work.opcode, work.arg_a, work.arg_b
				               );
			}

			if(work.ticks_remaining == 0){
				DEBUG_PIPELINE("Execute completed for %lu in exec unit %u, result: %li\n",
				               work.seq_id, unit_index, work.result.value.s);
				++proc.metrics.instructions_execed;
			}
		}
	}
}

void phaseWriteback(ProcessorState& proc,
                    ExecutionUnit exec_units[NUM_EXEC_UNITS],
                    ReorderBuffer& rob){
	for(int unit_index = 0; unit_index < NUM_EXEC_UNITS; ++unit_index){
		ExecutionUnit& unit = exec_units[unit_index];
		for(u64 i = 0; i < xen::size(unit); ++i){
			WorkEntry& work = unit[i];

			if(work.result.state != InstructionResult::NEW ||
			   work.ticks_remaining > 0
			   ){ continue; }

			DEBUG_PIPELINE("Performing writeback for instruction %lu\n", work.seq_id);

			///////////////////////////////////////////////
			// Broadcast the result to all waiting entries in the reorder buffer
			for(auto rbe = xen::iterateFront(rob); rbe; ++rbe){
				for(int i = 0; i < 2; ++i){
					if(!rbe->args[i].ready && rbe->args[i].waiting_for == work.seq_id){
						resolveValuePromise(Instruction::ARG_TYPES[i][rbe->opcode],
						                    rbe->args[i], work.result.value);
					}
				}

				if(rbe->result.state == InstructionResult::UNKNOWN &&
				   rbe->result.value.u == work.seq_id){
					rbe->result.value = work.result.value;
					rbe->result.state = InstructionResult::OLD;
				}

				if(rbe->seq_id == work.seq_id){
					rbe->result = work.result;

					proc.metrics.pc_writes += rbe->reg_dest == Register::PC;

					#if OPT_PARTIAL_ROLLBACK
					if(rbe->reg_dest == Register::PC &&
					   rbe[1] && rbe[1]->pc != work.result.value.u){
						// Then we've identified a branch misprediction early, discard
						// everything after this point
						rollbackPipeline(proc, rbe->seq_id + 1, work.result.value.u);
						return;
					}
					#endif

				}
			}

			xen::remove(unit, i);
			--i; // repeat this index, since its been removed
		}
	}
}

void phaseCommit(System* system, ProcessorState& proc){
	u64 cur_front = xen::iterateFront(proc.reorder_buffer).index;
	for(u64 i = cur_front;
	    i < cur_front + NUM_EXEC_UNITS && xen::isIndexValid(proc.reorder_buffer, i);
	    ++i
	   ){
		ReorderBufferEntry& entry = proc.reorder_buffer[i];

		if(entry.result.state != InstructionResult::NEW){ break; }


		#if DO_DEBUG_ASM
		printf("Performing commit for %lu, result %16li |",
		       entry.seq_id, entry.result.value);
		printInstructionAsm(entry);
		#else
		DEBUG_PIPELINE("Performing commit for %lu, result %16li\n",
		               entry.seq_id, entry.result.value);
		#endif

		// Notify dispatch that there are no more pending writes to this register
		if(entry.reg_dest < XenArrayLength(proc.last_write_table) &&
		   proc.last_write_table[entry.reg_dest] == entry.seq_id){
			proc.last_write_table[entry.reg_dest] = 0;
		}

		if(proc.regs[Register::PC].u != entry.pc){
			// Then we had a branch misprediction...
			xen::popFront(proc.reorder_buffer);
			DEBUG_PIPELINE("Commit got instruction with mismatched PC, wanted instruction %lu, got instruction %lu\n",
			               proc.regs[Register::PC].u, entry.pc
			              );
			flushPipeline(proc, entry.seq_id + 1, proc.regs[Register::PC].u);
			break;
		}

		++proc.metrics.instructions_commited;

		// Advance PC
		proc.regs[Register::PC].u = entry.pc + 4;

		if(entry.opcode == Instruction::HALT){
			DEBUG_PIPELINE("Commit encountered halt instruction, seqid: %lu\n", entry.seq_id);
			system->halted = true;
			return;
		}

		// Commit results to actual register file
		proc.regs[entry.reg_dest] = entry.result.value;

		// Notify memory controller that any pending operations before
		// this instruction may now be committed
		memctlCommit(proc.memctl, entry.seq_id);

		// Notify branch predictor of what actually happened
		if(entry.reg_dest == Register::PC){
			branchNotifyNextPc(proc.branch_predictor, entry.pc, proc.regs[Register::PC].u);
		}

		xen::popFront(proc.reorder_buffer);
	}
}

void stepProcessor(System* system, ProcessorState& proc){
	DEBUG_TICK("\n\n=== TICK: %9lu ====================================\n", proc.metrics.tick);

	#if DEBUG_PIPELINE_ORDERING
	// get instructions from memory
	phaseFetch  (system, proc.branch_predictor, proc.fetch);

	// decode instructions
	phaseDecode (proc.fetch.buffer, proc.decoded_instruction_buffer);

	// queue up instructions for execution
	phaseEnqueue(proc.decoded_instruction_buffer,
	             proc.regs,
	             proc.last_write_table,
	             proc.reorder_buffer);

	// Dispatch enqueued instructions to execution units
	phaseDispatch(proc.metrics, proc.memctl, proc.reorder_buffer, proc.exec_units);

	debugReorderBuffer(proc.reorder_buffer);

	// Do execution of instructions
	phaseExecute(proc, proc.exec_units);

	debugExecutionUnits(proc.exec_units);

	// Write back execution unit results to reorder buffer
	phaseWriteback(proc, proc.exec_units, proc.reorder_buffer);

	// Commit completed instructions to architectural registers
	phaseCommit(system, proc);
	#else
	debugReorderBuffer(proc.reorder_buffer);

	phaseCommit(system, proc);
	phaseWriteback(proc, proc.exec_units, proc.reorder_buffer);

	debugExecutionUnits(proc.exec_units);

	phaseExecute(proc, proc.exec_units);

	phaseDispatch(proc.metrics, proc.memctl, proc.reorder_buffer, proc.exec_units);
	phaseEnqueue(proc.decoded_instruction_buffer,
	             proc.regs,
	             proc.last_write_table,
	             proc.reorder_buffer);
	phaseDecode (proc.fetch.buffer, proc.decoded_instruction_buffer);
	phaseFetch  (system, proc.branch_predictor, proc.fetch);

	#endif

	++proc.metrics.tick;
}

void tickProcessor(Device* device){
	stepProcessor(device->system, *(ProcessorState*)device->state);
}

#endif
