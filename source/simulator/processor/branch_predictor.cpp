////////////////////////////////////////////////////////////////////////////
/// \brief Contains the definition of the branch predictor
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_BRANCHPREDICTOR_CPP
#define PROCSIM_SIMULATOR_BRANCHPREDICTOR_CPP

#include <procsim/instruction.hpp>
#include <procsim/register.hpp>
#include "branch_predictor.hpp"
#include "model.hpp"

#if DO_DEBUG_BRANCH
	#define DEBUG_BRANCH(...) printf(__VA_ARGS__)
#else
	#define DEBUG_BRANCH(...)
#endif

u64 branchGetPrediction(BranchPredictor& pred, u64 cur_pc, Instruction& ins){
  if(ins.rdest != Register::PC || ins.b_is_reg){
		return cur_pc + 4;
	}

	#if OPT_BRANCH_PREDICTOR >= 1 // DETERMINISTIC FOLLOWER
  if(ins.opcode == Instruction::Opcode::USET){
	  DEBUG_BRANCH("Branching - following %lu to %lu\n", cur_pc, (u64)ins.immediate);
	  return (u64)ins.immediate;
  }
  #endif

  #if OPT_BRANCH_PREDICTOR >= 2
  if(ins.opcode == Instruction::Opcode::CSET){
	  bool taken = (u64)ins.immediate < cur_pc; // static prediction
	  DEBUG_BRANCH("Branching - Static prediction for %lu, taken: %i\n", cur_pc, taken);

		#if OPT_BRANCH_PREDICTOR >= 3
	  for(int i = 0; i < XenArrayLength(pred.slots); ++i){
		  pred.slots[i].age = xen::max(pred.slots[i].age+1, 65500);
		  if(pred.slots[i].in_use && pred.slots[i].pc == cur_pc){
			  taken = pred.slots[i].value >= BranchPredictorCounter::TAKEN_THRESHOLD;
			  DEBUG_BRANCH("Branching - Using dynamic prediction for %lu, taken: %i\n",
			               cur_pc, taken);
			  break;
		  }
	  }
		#endif

	  return taken ? (u64)ins.immediate : cur_pc + 4;
  }
  #endif

  return cur_pc + 4;
}

void branchNotifyNextPc(BranchPredictor& pred, u64 cur_pc, u64 next_pc){
	#if OPT_BRANCH_PREDICTOR >= 3

	bool taken = ((cur_pc + 4) != next_pc);

	u16 max_age        =  0;
  int max_age_index  = -1;
  int matching_index = -1;
  int free_slot      = -1;
	for(int i = 0; i < XenArrayLength(pred.slots); ++i){
		if(!pred.slots[i].in_use){
			free_slot = i;
		} else {
			if(pred.slots[i].pc == cur_pc){
				matching_index = i;
				break;
			}

			if(pred.slots[i].age > max_age){
				max_age       = pred.slots[i].age;
				max_age_index = i;
			}
		}
	}

	if(matching_index >= 0){ // update existing entry
		BranchPredictorCounter& slot = pred.slots[matching_index];

		int old_value = slot.value;

		slot.age    = 0;
		slot.value += taken ? 1 : -1;
		if(slot.value < BranchPredictorCounter::MIN){
			slot.value = BranchPredictorCounter::MIN;
			DEBUG_BRANCH("Dynamic branch predictor entry for %lu saturated at %i\n",
			             cur_pc, slot.value);
		} else if(slot.value > BranchPredictorCounter::MAX){
				slot.value = BranchPredictorCounter::MAX;
				DEBUG_BRANCH("Dynamic branch predictor entry for %lu saturated at %i\n",
			             cur_pc, slot.value);
		} else {
			DEBUG_BRANCH("Updating dynamic branch predictor entry for %lu from %i to %i\n",
			             cur_pc, old_value, slot.value);
		}
		return;
	}

	if(free_slot < 0){ // then evict oldest
		DEBUG_BRANCH("Evicting dynamic branch predictor entry for %lu\n", pred.slots[max_age_index]);
		free_slot = max_age_index;
	}

	BranchPredictorCounter& slot = pred.slots[free_slot];
	DEBUG_BRANCH("Inserting dynamic branch predictor entry for %lu\n", cur_pc);
	slot.in_use = true;
	slot.age    = 0;
	slot.pc     = cur_pc;
	if(taken){
		slot.value = BranchPredictorCounter::WEAKLY_NOT_TAKEN;
	} else {
		slot.value = BranchPredictorCounter::WEAKLY_TAKEN;
	}
	return;
	#endif
}

#endif
