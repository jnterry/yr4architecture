////////////////////////////////////////////////////////////////////////////
/// \brief Contains various utility functions for declaring the processor
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_PROCESSOR_UTILITIES_CPP
#define PROCSIM_SIMULATOR_PROCESSOR_UTILITIES_CPP

#include "types.hpp"
#include "model.hpp"

void printWord(u32 index, Word64 word){
	float f = word.f;
	if(f > 9999999999999.0){
		printf(" %2i: %20lu | %20li | %20s | %18p\n",
		       index, word.u, word.s, "+ve BIG", (void*)word.u);
	} else if (f < -9999999999999.0){
		printf(" %2i: %20lu | %20li | %20s | %18p\n",
		       index, word.u, word.s, "-ve BIG", (void*)word.u);
	} else {
		printf(" %2i: %20lu | %20li | %20f | %18p\n",
		       index, word.u, word.s, word.f, (void*)word.u);
	}
}

void resolveValuePromise(Instruction::ArgType type, ValuePromise& vp, Word64 value){
	switch(type){
	case Instruction::ArgType::UNSIGNED:
	case Instruction::ArgType::ANY:
	case Instruction::ArgType::NONE:
		vp.value.u = value.u + vp.offset;
		break;
	case Instruction::ArgType::SIGNED:
		vp.value.s = value.s + vp.offset;
		break;
	case Instruction::ArgType::FLOAT:
		vp.value.f = value.f + vp.offset;
		break;
	default:
		XenInvalidCodePath("Missing switch case");
	}
	vp.ready = true;
}

void printInstructionAsm(ReorderBufferEntry& entry){
	printf("%4s = %s ",
	       REGISTER_NAMES[entry.reg_dest],
	       Instruction::NAMES[entry.opcode]
	      );

	for(int i = 0; i < 2; ++i){
		switch(Instruction::ARG_TYPES[i][entry.opcode]){
		case Instruction::ArgType::NONE:
			printf("--- ");
			break;
		case Instruction::ArgType::UNSIGNED:
			printf("%lu ", entry.args[i].value.u);
			break;
		case Instruction::ArgType::ANY:
			printf("%p ", (void*)entry.args[i].value.u);
			break;
		case Instruction::ArgType::SIGNED:
			printf("%li ", entry.args[i].value.s);
			break;
		case Instruction::ArgType::FLOAT:
			printf("%f ", entry.args[i].value.f);
			break;
		}
	}

	printf("\n");
}

void debugReorderBuffer(ReorderBuffer& rob){
	#if DO_DEBUG_REORDER
	auto front = xen::iterateFront(rob);

  printf("|--------------------------------------------------------------------------------------------------------------------------------------");
	for(int base = 0; front[base] && base < DISPATCH_WINDOW; base += 20){
		printf("\n");
		printf("| SeqId  |");
		for(int i = base; front[i] && i < base+20 && i < DISPATCH_WINDOW; ++i) {
			printf("%05lu|", front[i]->seq_id);
		}
		if(front[base+20]){ printf("..."); }


		printf("\n| Opcode |");
		for(int i = base; front[i] && i < base+20 && i < DISPATCH_WINDOW; ++i) {
			printf("%5s|", Instruction::NAMES[front[i]->opcode]);
		}
	  if(front[base+20]){ printf("..."); }


		printf("\n| ReadyA |");
		for(int i = base; front[i] && i < base+20 && i < DISPATCH_WINDOW; ++i) {
			if(front[i]->args[0].ready){
				printf("     |");
			} else {
				printf("%05lu|", front[i]->args[0].waiting_for);
			}
		}
		if(front[base+20]){ printf("..."); }


		printf("\n| ReadyB |");
		for(int i = base; front[i] && i < base+20 && i < DISPATCH_WINDOW; ++i) {
			if(front[i]->args[1].ready){
				printf("     |");
			} else {
				printf("%05lu|", front[i]->args[1].waiting_for);
			}
		}
	  if(front[base+20]){ printf("..."); }


		printf("\n| Execed |");
		for(int i = base; front[i] && i < base+20 && i < DISPATCH_WINDOW; ++i) {
			if(front[i]->result.state == InstructionResult::NEW){
				printf(" YES |");
			} else if(front[i]->dispatched){
				printf(" ... |");
			} else {
				printf("     |");
			}
		}
		if(front[base+20]){ printf("..."); }


		printf("\n");
	}
	printf("|--------------------------------------------------------------------------------------------------------------------------------------\n");
  #endif
}

void debugExecutionUnits(ExecutionUnit eu[NUM_EXEC_UNITS]){
	#if DO_DEBUG_PORTS

	printf("|= PORTS ===========================================================================|\n");

	for(int eui = 0; eui < NUM_EXEC_UNITS; ++eui){
		printf("| %3u |", eui);

		for(int i = 0; i < eu[eui].size; ++i){
			printf("%05lu - %5s - %u |",
			       eu[eui][i].seq_id,
			       Instruction::NAMES[eu[eui][i].opcode],
			       eu[eui][i].ticks_remaining
			      );
		}
		printf("\n");
	}
	printf("|==================================================================================|\n");

	#endif
}

void printProcessorMetrics(ProcessorMetrics& metrics){
	printf("Ticks elapsed: %lu\n", metrics.tick);


	printf("---------------------------------------\n");
	printf("Instructions executed  : %lu (%.2f per cycle - %.2f throughput)\n",
	       metrics.instructions_execed,
	       (double)metrics.instructions_execed / (double)metrics.tick,
	       (double)metrics.instructions_execed / ((double)metrics.tick-6)
	      );
	printf("             committed : %lu (%.2f per cycle - %.2f throughput)\n",
	       metrics.instructions_commited,
	       (double)metrics.instructions_commited / (double)metrics.tick,
	       (double)metrics.instructions_commited / ((double)metrics.tick-6)
	       );
	u64 wasted_count = metrics.instructions_execed - metrics.instructions_commited;
	printf("             wasted    : %lu (%.2f%% of those execed)\n",
	       wasted_count,
	       ((double)wasted_count / (double)metrics.instructions_execed) * 100.0);


	printf("---------------------------------------\n");
	printf("Port utilization\n");
	for(int i = 0; i < xen::min<int>(XenArrayLength(metrics.port_utilization), 15); ++i){
		printf("%3i : %10lu (%5.2f%%)\n", i,
		       metrics.port_utilization[i],
		       ((double)metrics.port_utilization[i] / (double)metrics.tick) * 100.0);
	}

	printf("---------------------------------------\n");
	printf("Total branches  : %6lu\n", metrics.pc_writes);
	printf("Mispredictions  : %6lu (%.2f%%)\n",
	       metrics.pipeline_rollbacks,
	       (metrics.pc_writes != 0
	        ? ((double)metrics.pipeline_rollbacks / (double)metrics.pc_writes)*100.0f
	        : 0
	        )
	       );
}

#endif
