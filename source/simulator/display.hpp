////////////////////////////////////////////////////////////////////////////
/// \brief Contains the interface to the display module
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_DISPLAY_HPP
#define PROCSIM_SIMULATOR_DISPLAY_HPP

#include <SDL.h>

#include <procsim/types.hpp>
#include "device.hpp"

/// \brief Initialises the display module
bool initDisplay(Device* device, u32 size_x, u32 size_y);

/// \brief Shuts down the display module
void shutdownDisplay(Device* device);

/// \brief Ticks the display module
void tickDisplay(Device* device);

#endif
