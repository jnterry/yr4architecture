////////////////////////////////////////////////////////////////////////////
/// \brief Entrypoint to simulator program
////////////////////////////////////////////////////////////////////////////

#include <procsim/instruction.hpp>
#include <procsim/types.hpp>

#include "processor.hpp"
#include "display.hpp"
#include "processor/model.hpp"

#include <cstring>

#define DEVICE_PROCESSOR 0
#define DEVICE_DISPLAY   1
#define DEVICE_COUNT     2

int main(int argc, const char** argv) {
	////////////////////////////////
	// Check usage
	if(argc != 2){
		printf("BAD USAGE, try: %s <MEMORY_FILE>\n", argv[0]);
		return 1;
	}

	////////////////////////////////
	// Print model that this simulator was built with
	if(strcmp(argv[1], "model") == 0){
		printf("Processor Model:\n");
		printf("  FRONTEND_WIDTH       : %i\n", FRONTEND_WIDTH);
		printf("  REORDER_BUFFER_SIZE  : %i\n", REORDER_BUFFER_SIZE);
		printf("  MEMORY_BUFFER_SIZE   : %i\n", MEMORY_BUFFER_SIZE);
		printf("  NUM_EXEC_UNITS       : %i\n", NUM_EXEC_UNITS);
		printf("  DISPATCH_WINDOW      : %i\n", DISPATCH_WINDOW);
		printf("Optimizations:\n");
		printf("  REGISTER_RENAMING    : %i\n", OPT_REGISTER_RENAMING);
		printf("  Partial Rollback     : %i\n", OPT_PARTIAL_ROLLBACK);
		printf("  Pipelined Exec units : %i\n", OPT_PIPELINED_EXEC_UNITS);
		printf("  Branch Predictor     : %i\n", OPT_BRANCH_PREDICTOR);
		return 0;
	}

	////////////////////////////////
	// Setup memory map
	Device devices[2];
	devices[DEVICE_PROCESSOR].memory_start = 0x0000000000000000;
	devices[DEVICE_PROCESSOR].memory_end   = 0x0000000002000000; // 32mb main memory
	devices[DEVICE_DISPLAY  ].memory_start = 0xff0000dd00000000;
	devices[DEVICE_DISPLAY  ].memory_end   = 0xff0000dd02000000; // 32mb video ram

	////////////////////////////////
	// Allocate storage for device memory
	for(int i = 0; i < DEVICE_COUNT; ++i){
		devices[i].memory = malloc(devices[i].memory_end - devices[i].memory_start);
	}

	////////////////////////////////
	// Make system
	System system;
	system.devices      = devices;
	system.device_count = DEVICE_COUNT;
	system.halted       = false;
	for(int i = 0; i < DEVICE_COUNT; ++i){
		devices[i].system = &system;
	}

	////////////////////////////////
	// Initialize devices
	bool init_done = true;
	init_done &= initProcessor(&system, &devices[DEVICE_PROCESSOR], argv[1]);
	init_done &= initDisplay  (&devices[DEVICE_DISPLAY  ], 256, 256);

	if(!init_done){
		fprintf(stderr, "Initialisation failed, terminating...\n");
		return 2;
	}

	////////////////////////////////
	// Run simulation
	printf("Beginning simulation...\n");
	while(!system.halted){
		tickProcessor(&devices[0]);
		tickDisplay  (&devices[1]);
	}

	////////////////////////////////
	// Shutdown devices
	shutdownProcessor(&devices[0], argv[1]);
	shutdownDisplay  (&devices[1]);

	////////////////////////////////
	// Free device memory
	for(int i = 0; i < DEVICE_COUNT; ++i){
		free(devices[i].memory);
	}

	return 0;
}
