////////////////////////////////////////////////////////////////////////////
/// \brief Implementation file for the display device
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_SIMULATOR_DISPLAY_CPP
#define PROCSIM_SIMULATOR_DISPLAY_CPP

#include "display.hpp"

#include <xen/core/memory/utilities.hpp>
#include <cstring>

struct DisplayState {
	SDL_Window*   window;
	SDL_Renderer* renderer;
	SDL_Texture*  texture;
};

/////////////////////////////////////////////////////////////////////
/// \brief The memory map presented to the processor to interact with
/// the display module, will be mounted into address space at
/////////////////////////////////////////////////////////////////////
#pragma pack(push,1)
struct DisplayMemory {
	/// \brief Read only value indicating the size of the display in x
	u64 size_x;

	/// \brief Read only value indicating the size of the display in y
	u64 size_y;

	/// \brief When non-zero indicates that the display module should
	/// present the current frame buffer
	u64 swap_request;

	/// \brief Pointer to first byte of the framebuffer that should be
	/// being written to currently. Size is size_x * size_y * 4
	/// (IE: 4 bytes per pixel)
	u64 framebuffer_address;
};
#pragma pack(pop)

bool initDisplay(Device* device, u32 size_x, u32 size_y){

	u64 memory_size = device->memory_end - device->memory_start;

	u64 required_space = (2 * sizeof(u32) * size_x * size_y) + sizeof(DisplayMemory);
	if(memory_size < required_space){
		printf("Not enough memory for display device\n");
		return false;
	}

	xen::clearToZero(device->memory, memory_size);

	DisplayMemory* memory = (DisplayMemory*)device->memory;
	memory->size_x = size_x;
	memory->size_y = size_y;
	memory->framebuffer_address = device->memory_start + sizeof(DisplayMemory);

	device->state = malloc(sizeof(DisplayState));
	DisplayState* state = (DisplayState*)device->state;
	xen::clearToZero(state);

	if(SDL_Init(SDL_INIT_VIDEO) != 0){
		printf("Failed to initialize SDL\n");
		return false;
	}

	state->window = SDL_CreateWindow("ProcSim",
	                                 SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
	                                 size_x, size_y, SDL_WINDOW_OPENGL);
	if(state->window == nullptr){
		printf("Failed to create SDL window\n");
		return false;
	}

	u64 flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
	state->renderer = SDL_CreateRenderer(state->window, -1, flags);
	if(state->renderer == 0){
		printf("Failed to create SDL renderer\n");
		return false;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	SDL_RenderSetLogicalSize(state->renderer, size_x, size_y);

	state->texture = SDL_CreateTexture(state->renderer,
	                                   SDL_PIXELFORMAT_RGBA8888,
	                                   SDL_TEXTUREACCESS_STREAMING,
	                                   size_x, size_y);
	if(state->texture == nullptr){
		printf("Failed to create SDL texture\n");
		return false;
	}

	return true;
}

void shutdownDisplay(Device* device){
	DisplayState* state = (DisplayState*)device->state;

	SDL_DestroyTexture (state->texture);
	SDL_DestroyRenderer(state->renderer);
	SDL_DestroyWindow  (state->window);
	SDL_Quit();

	free(device->state);
}

void tickDisplay(Device* device){
	DisplayMemory* memory = (DisplayMemory*)device->memory;
	DisplayState* state = (DisplayState*)device->state;

	if(memory->swap_request == 0){
		return;
	}

	u64 framebuffer_0 = device->memory_start + sizeof(DisplayMemory);
  u64 framebuffer_1 = framebuffer_0 + (memory->size_x * memory->size_y * sizeof(u32));

  u64 active_buffer;
  if(memory->framebuffer_address == framebuffer_0){
	  active_buffer = 0;
  } else if (memory->framebuffer_address == framebuffer_1) {
	  active_buffer = 1;
  } else {
	  printf("Framebuffer value is invalid, resetting to buffer 0!\n");
	  memory->framebuffer_address = framebuffer_0;
	  return;
  }

  printf("Displaying buffer: %lu\n", active_buffer);

	SDL_UpdateTexture(state->texture,
	                  NULL,
	                  resolveSimulatorAddress(device->system, memory->framebuffer_address),
	                  memory->size_x*sizeof(u32));
  SDL_RenderCopy(state->renderer, state->texture, NULL, NULL);
  SDL_RenderPresent(state->renderer);


  if(active_buffer == 0){
	  memory->framebuffer_address = framebuffer_1;
  } else {
	  memory->framebuffer_address = framebuffer_0;
  }
  memory->swap_request = 0;

  printf("Framebuffer address is now: %p\n", memory->framebuffer_address);

  // Clear to black
  memset(resolveSimulatorAddress(device->system, memory->framebuffer_address),
         0, memory->size_x * memory->size_y * sizeof(u32));
}

#endif
