////////////////////////////////////////////////////////////////////////////
/// \brief Contains implementation instruction data types
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_INSTRUCTION_CPP
#define PROCSIM_INSTRUCTION_CPP

#include <procsim/instruction.hpp>

const char* const Instruction::NAMES[Opcode::COUNT] = {
	"halt",

	"sadd", "uadd", "fadd",
	"ssub", "usub", "fsub",
	"smul", "umul", "fmul",
	"sdiv", "udiv", "fdiv",

	"shl", "shr",

	"and", "nand",
	"or", "nor",
	"xor", "xnor",
	"inv",

	"ifeq", "ifne",
	"ifule", "iful", "ifuge", "ifug",
	"ifsle", "ifsl", "ifsge", "ifsg",
	"iffle", "iffl", "iffge", "iffg",

	"uset", "sset", "fset", "cset",

	"load", "stora", "storb",

	"ftos", "stof",
};

const Instruction::ArgType Instruction::ARG_TYPES[2][Instruction::Opcode::COUNT] = {
	{
		// "halt",
		Instruction::ArgType::NONE,

		//"sadd", "uadd", "fadd",
		//"ssub", "usub", "fsub",
		//"smul", "umul", "fmul",
		//"sdiv", "udiv", "fdiv",
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,

		//"shl", "shr",
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,

		//"and", "nand",
		//"or", "nor",
		//"xor", "xnor",
		//"inv",
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::NONE,

		//"ifeq", "ifne",
		//"ifule", "iful", "ifuge", "ifug",
		//"ifsle", "ifsl", "ifsge", "ifsg",
		//"iffle", "iffl", "iffge", "iffg",
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED,
		Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED,
		Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT,

		//"uset", "sset", "fset",
		Instruction::ArgType::NONE, Instruction::ArgType::NONE, Instruction::ArgType::NONE,

		//"cset"
		Instruction::ArgType::ANY,

		//"load", "stora", "storb",
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::ANY, Instruction::ArgType::UNSIGNED,

		//"ftos", "stof",
		Instruction::ArgType::NONE, Instruction::ArgType::NONE,
	},
	{
		// "halt",
		Instruction::ArgType::UNSIGNED,

		//"sadd", "uadd", "fadd",
		//"ssub", "usub", "fsub",
		//"smul", "umul", "fmul",
		//"sdiv", "udiv", "fdiv",
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,
		Instruction::ArgType::SIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::FLOAT,

		//"shl", "shr",
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED,

		//"and", "nand",
		//"or", "nor",
		//"xor", "xnor",
		//"inv",
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::ANY,

		//"ifeq", "ifne",
		//"ifule", "iful", "ifuge", "ifug",
		//"ifsle", "ifsl", "ifsge", "ifsg",
		//"iffle", "iffl", "iffge", "iffg",
		Instruction::ArgType::ANY, Instruction::ArgType::ANY,
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED,
		Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED, Instruction::ArgType::SIGNED,
		Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT, Instruction::ArgType::FLOAT,

		//"uset", "sset", "fset",
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::SIGNED, Instruction::ArgType::FLOAT,

		// "cset"
		Instruction::ArgType::ANY,

		//"load", "stora", "storb",
		Instruction::ArgType::UNSIGNED, Instruction::ArgType::UNSIGNED, Instruction::ArgType::ANY,

		//"ftos", "stof",
		Instruction::ArgType::FLOAT, Instruction::ArgType::SIGNED,
	}
};

const bool Instruction::MAY_NOOP[Instruction::Opcode::COUNT] = {
	// "halt",
	false,

	//"sadd", "uadd", "fadd",
	//"ssub", "usub", "fsub",
	//"smul", "umul", "fmul",
	//"sdiv", "udiv", "fdiv",
	false, false, false,
	false, false, false,
	false, false, false,
	false, false, false,

	//"shl", "shr",
	false, false,

	//"and", "nand",
	//"or", "nor",
	//"xor", "xnor",
	//"inv",
	false, false,
	false, false,
	false, false,
	false,

	//"ifeq", "ifne",
	//"ifule", "iful", "ifuge", "ifug",
	//"ifsle", "ifsl", "ifsge", "ifsg",
	//"iffle", "iffl", "iffge", "iffg",
	false, false,
	false, false, false, false,
	false, false, false, false,
	false, false, false, false,

	//"uset", "sset", "fset",
	false, false, false,

	//"cset"
	true,

	//"load", "stora", "storb",
	false, false, false,

	//"ftos", "stof",
	false, false,
};

s16 getSignExtendedOffset(Instruction ins) {
	if((1 << (Instruction::OFFSET_BITS-1)) & ins.immediate){
		// Then offset is negative, sign extend...
		return (u16)ins.offset | ~((u16)((1<<Instruction::OFFSET_BITS)-1));
	} else {
		return ins.offset;
	}
}

Word64 getSignExtendedImmediate(Instruction ins) {
	Word64 result;

	switch(Instruction::ARG_TYPES[1][ins.opcode]){
	case Instruction::ArgType::UNSIGNED:
	case Instruction::ArgType::ANY:
	case Instruction::ArgType::NONE:
		result.u = ins.immediate;
		break;
	case Instruction::ArgType::SIGNED:
		if((1 << (Instruction::IMMEDIATE_BITS-1)) & ins.immediate){
			// Then arg is negative, sign extend...
		  result.u = (u64)ins.immediate | ~((u64)((1<<Instruction::IMMEDIATE_BITS)-1));
		} else {
		  result.u = ins.immediate;
		}
		break;
	case Instruction::ArgType::FLOAT:
		XenInvalidCodePath("Floating point immediates not supported!");
		break;
	default:
		XenInvalidCodePath("Missing switch case");
	}

	return result;
}

#endif
