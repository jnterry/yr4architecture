////////////////////////////////////////////////////////////////////////////
/// \brief Contains implementation of types related to Register
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_REGISTER_CPP
#define PROCSIM_REGISTER_CPP

#include "register.hpp"

const char* const REGISTER_NAMES[Register::COUNT] = {
	"r00",
	"r01",
	"r02",
	"r03",
	"r04",
	"r05",
	"r06",
	"r07",
	"r08",
	"r09",
	"r10",
	"r11",
	"r12",
	"r13",
	"r14",
	"r15",
	"r16",
	"r17",
	"r18",
	"r19",
	"r20",
	"r21",
	"r22",
	"r23",
	"r24",
	"r25",
	"r26",
	"r27",
	"r28",
	"r29",
	"null",
	"pc",
};

#endif
