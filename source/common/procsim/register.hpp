////////////////////////////////////////////////////////////////////////////
/// \brief Contains declaration of register enum and related types
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_REGISTER_HPP
#define PROCSIM_REGISTER_HPP

//@ * Architectural Register File
//@
//@ The simulated processor has 32 architectural registers each of 64 bits.
//@ All registers may be used by all instruction types and hence are could
//@ at any time be storing either a signed, unsigned or floating value

enum Register {

	//@ ** Register Names
	//@ The first 30 registers are general purpose and are labelled r00 to r29
	R00, R01, R02, R03, R04, R05, R06, R07, R08, R09,
	R10, R11, R12, R13, R14, R15, R16, R17, R18, R19,
	R20, R21, R22, R23, R24, R25, R26, R27, R28, R29,

	//@ Register index 30 is labelled "null". Any reads from this register will
	//@ return the constant 0, and any writes to this register are discarded.
	RNULL,

	//@ Register index 31 is the program counter, whose value may be referenced in
	//@ [[file:../../assembler][assembly]] code by using the symbol "pc".
	//@
	//@ As outlined in the [[file:instruction.hpp][instruction set architecture]],
	//@ there are no explicit branch instructions, and instead any instruction may
	//@ target (or read from) the program counter
	PC,

	////////////////////
	//@ ** Helpers

	//@ The total number of registers
	COUNT,

	//@ Alias for the general purpose register with the highest index
	LAST_GP = R29,
};


//@ * Related Types

//@ Array which maps values in the Register enum to their human readable string
//@ names. Used for both debug messages and parsing of assembly code by the
//@ [[file:../../assembler][assember]]
extern const char* const REGISTER_NAMES[Register::COUNT];

#endif
