////////////////////////////////////////////////////////////////////////////
/// \brief Contains declaration of instruction data types and related functions
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_INSTRUCTION_HPP
#define PROCSIM_INSTRUCTION_HPP

#include <xen/core/intrinsics.hpp>
#include <procsim/types.hpp>

// Disable gcc's warning about anonymous structs in unions temporarily...
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

/////////////////////////////////////////////////////////////////////
//@ * Instruction Set Architecture
//@ All instructions can be thought of as representing some operation
//@ to be performed with two 64 bit inputs and a single 64 bit output,
//@ and hence may all be encoded in the same way - a 32 bit word with
//@ the following bit pattern:
//@
//@ [[file:/docs/instruction_format.png]]
//@
//@ ~rdest~ thus represents the destination register to which the result
//@ will be written, it may be set to the null register in order to
//@ discard the result (this is only useful for representing no-ops or
//@ for instructions with side effects, such as memory stores)
//@
//@ Instructions take two values, A and B. For instructions where order
//@ matters (for example, subtraction) A is the LHS operand and B is
//@ the right.
//@
//@ A must be read from some register unaltered, however B may be represented
//@ by one of the following sources:
//@ - Value loaded from register, unaltered
//@ - Value loaded from register, plus or minus some 10 bit signed offset
//@ - 15 bit immediate value
//@ The unaltered register value is represented as the altered value with an
//@ offset of 0, hence there are only two cases, controlled by the value of
//@ bit 15.
//@
/////////////////////////////////////////////////////////////////////
struct Instruction {

	static const constexpr int IMMEDIATE_BITS = 15;
	static const constexpr int OFFSET_BITS    = 10;

	union {
		u32 raw; // raw value of the instruction
		struct {
			u32 opcode    : 6,
				  rdest     : 5,
				  source_a  : 5,
				  b_is_reg  : 1,
				  immediate : IMMEDIATE_BITS;
		};
		struct {
			u32 _dummy    : 17,
				  offset    : OFFSET_BITS,
				  source_b  : 5;
		};
	};

	//@ ** Enumeration of Available Opcodes
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | op | asm                   | Description                     | C Equivalent                     |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ |  0 | *halt* rA             | Stops the processor             | exit(rA)                         |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ |  1 | rdest = *sadd* rA vB  | Signed integer add              | rdest = rA + vB                  |
	//@ |  2 | rdest = *uadd* rA vB  | Unsigned integer add            | rdest = rA + vB                  |
	//@ |  3 | rdest = *fadd* rA vB  | Floating add                    | rdest = rA + vB                  |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ |  4 | rdest = *ssub*  rA vB | Signed integer subtract         | rdest = rA - rB                  |
	//@ |  5 | rdest = *usub*  rA vB | Unsigned integer subtract       | rdest = rA - rB                  |
	//@ |  6 | rdest = *fsub*  rA vB | Floating subtract               | rdest = rA - rB                  |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ |  7 | rdest = *smul*  rA vB | Signed integer multiply         | rdest = rA * rB                  |
	//@ |  8 | rdest = *umul*  rA vB | Unsigned integer multiply       | rdest = rA * rB                  |
	//@ |  9 | rdest = *fmul*  rA vB | Floating multiply               | rdest = rA * rB                  |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 10 | rdest = *sdiv*  rA vB | Signed integer divide           | rdest = rA / rB                  |
	//@ | 11 | rdest = *udiv*  rA vB | Unsigned integer divide         | rdest = rA / rB                  |
	//@ | 12 | rdest = *fdiv*  rA vB | Floating divide                 | rdest = rA / rB                  |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 13 | rdest = *shl* rA vB   | Shift left                      | rdest = rA << vB                 |
	//@ | 14 | rdest = *shr* rA vB   | Shift right                     | rdest = rA >> vB                 |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 15 | rdest = *and*  rA vB  | Bitwise and                     | rdest = rA & vB                  |
	//@ | 16 | rdest = *nand* rA vB  | Bitwise nand                    | rdest = ~(rA & vB)               |
	//@ | 17 | rdest = *or*   rA vB  | Bitwise or                      | rdest = rA \vert vB              |
	//@ | 18 | rdest = *nor*  rA vB  | Bitwise nor                     | rdest = ~(rA \vert vB)           |
	//@ | 19 | rdest = *xor*  rA vB  | Bitwise xor                     | rdest = rA ^ vB                  |
	//@ | 20 | rdest = *xnor* rA vB  | Bitwise xnor                    | rdest = ~(rA ^ vB)               |
	//@ | 21 | rdest = *inv*  vB     | Bitwise not                     | rdest = ~vB                      |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 22 | rdest = *ifeq* rA vB  | If bitwise equal                | rdest = rA == vB ? 1 : 0         |
	//@ | 23 | rdest = *ifne* rA vB  | If bitwise not equal            | rdest = rA != vB ? 1 : 0         |
	//@ | 24 | rdest = *ifule* rA vB | If unsigned <=                  | rdest = rA <= vB ? 1 : 0         |
	//@ | 25 | rdest = *iful*  rA vB | If unsigned <                   | rdest = rA <  vB ? 1 : 0         |
	//@ | 26 | rdest = *ifuge* rA vB | If unsigned >=                  | rdest = rA >= vB ? 1 : 0         |
	//@ | 27 | rdest = *ifug*  rA vB | If unsigned >                   | rdest = rA >  vB ? 1 : 0         |
	//@ | 28 | rdest = *ifsle* rA vB | If signed <=                    | rdest = rA <= vB ? 1 : 0         |
	//@ | 29 | rdest = *ifsl*  rA vB | If signed <                     | rdest = rA <  vB ? 1 : 0         |
	//@ | 30 | rdest = *ifsge* rA vB | If signed >=                    | rdest = rA >= vB ? 1 : 0         |
	//@ | 31 | rdest = *ifsg*  rA vB | If signed >                     | rdest = rA >  vB ? 1 : 0         |
	//@ | 32 | rdest = *iffle* rA vB | If float <=                     | rdest = rA <= vB ? 1 : 0         |
	//@ | 33 | rdest = *iffl*  rA vB | If float <                      | rdest = rA <  vB ? 1 : 0         |
	//@ | 34 | rdest = *iffge* rA vB | If float >=                     | rdest = rA >= vB ? 1 : 0         |
	//@ | 35 | rdest = *iffg*  rA vB | If float >                      | rdest = rA >  vB ? 1 : 0         |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 36 | rdest = *uset* rA vB  | Sets register to unsigned value | if(rA) { rdest = vB; }           |
	//@ | 37 | rdest = *sset* rA vB  | Sets register to signed value   | if(rA) { rdest = vB; }           |
	//@ | 38 | rdest = *fset* rA vB  | Sets register to floating value | if(rA) { rdest = vB; }           |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 39 | rdest = *load* rA vB  | Loads memory into register      | rdest = memory[rA + vB]          |
	//@ | 40 | rdest = *stora* rA vB | Stores rA to memory[vB]         | memory[vB] = rA; rdest = vB + 8  |
	//@ | 41 | rdest = *storb* rA vB | Stores vB into memory[rA]       | memory[rA] = vB; rdest = rA + 8  |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	//@ | 42 | rdest = *ftos* vB     | Converts float to signed int    | rdest = (long long)vB            |
	//@ | 43 | rdest = *stof* vB     | Converts signed to float        | rdest = (double)vB               |
	//@ |----+-----------------------+---------------------------------+----------------------------------|
	enum Opcode {
		HALT,

		SADD, UADD, FADD,
		SSUB, USUB, FSUB,
		SMUL, UMUL, FMUL,
		SDIV, UDIV, FDIV,

		SHL, SHR,

		AND, NAND,
		OR,  NOR,
		XOR, XNOR,
		INV,

		IFEQ, IFNE,
		IFULE, IFUL, IFUGE, IFUG,
		IFSLE, IFSL, IFSGE, IFSG,
		IFFLE, IFFL, IFFGE, IFFG,

		USET, SSET, FSET, CSET,
		LOAD, STORA, STORB,

		FTOS, STOF,

		COUNT,
	};

	//@ ** Helper Arrays
	//@ The following arrays map from the Instruction::Opcode enum to
	//@ some value related to that opcode

	/////////////

	//@ Represents the human readable name of the instruction - used for
	//@ debug prints and in assembly code
	static const char* const NAMES[Opcode::COUNT];

	/////////////

	//@ Enumeration of possible interpretations of the LHS and RHS operands
	//@ These are required when working out how to interpret the 15 bit immediate
	//@ value as these must be extended to full 64 bit values before they can be
	//@ used
	enum ArgType {
		NONE,     // does not use this argument
		UNSIGNED, // treats arg as 64 bit unsigned integer
		SIGNED,   // treats arg as 64 bit signed integer
		FLOAT,    // treats arg as 64 bit floating number
		ANY,      // treats arg as generic collection of bits
	};

	/////////////

	//@ Represents the type of operand A for each opcode
	static const ArgType ARG_TYPES_A[Opcode::COUNT];
	static const ArgType ARG_TYPES_B[Opcode::COUNT];
	static const ArgType ARG_TYPES[2][Opcode::COUNT];

	/////////////

	//@ Whether a particular opcode might noop. IE: not overwrite the
	//@ destination register (currently only used by ~cset~ to conditionally
	//@ overwrite the destination)
	static const bool    MAY_NOOP[Opcode::COUNT];
};
#pragma GCC diagnostic pop // re-enable -Wpedantic

//@ * Helper functions

//@ Takes a offset from an instruction and converts it into proper
//@ 16 bit signed integer, while respecting the sign bit of the offset itself
s16 getSignExtendedOffset(Instruction ins);

//@ Retrieves the immediate value from an instruction and converts
//@ in into proper 64 bit word, respecting any necessary sign extension
//@ depending on instruction type
Word64 getSignExtendedImmediate(Instruction ins);

//@ * Instruction Types

//@ The simulator's execution units only support executing particular
//@ instructions. Since opcodes are encoded using 6 bits there are only
//@ 64 possible instructions, hence we can represented the supported
//@ instructions as a 64 bit bitfield. The following values represent
//@ common "types" of instruction, for example, ALU vs LSU operations

const constexpr u64 OPCODE_TYPE_ALU =
	(1l << Instruction::Opcode::HALT |
	 1l << Instruction::Opcode::SADD |
	 1l << Instruction::Opcode::UADD |
	 1l << Instruction::Opcode::SSUB |
	 1l << Instruction::Opcode::USUB |
	 1l << Instruction::Opcode::SMUL |
	 1l << Instruction::Opcode::UMUL |
	 1l << Instruction::Opcode::SDIV |
	 1l << Instruction::Opcode::UDIV |
	 1l << Instruction::Opcode::SHL  |
	 1l << Instruction::Opcode::SHR  |
	 1l << Instruction::Opcode::AND  |
	 1l << Instruction::Opcode::NAND |
	 1l << Instruction::Opcode::OR   |
	 1l << Instruction::Opcode::NOR  |
	 1l << Instruction::Opcode::XOR  |
	 1l << Instruction::Opcode::XNOR |
	 1l << Instruction::Opcode::INV  |
	 1l << Instruction::Opcode::USET |
	 1l << Instruction::Opcode::SSET |
	 1l << Instruction::Opcode::CSET
	);

const constexpr u64 OPCODE_TYPE_FPU =
	(1l << Instruction::Opcode::FADD |
	 1l << Instruction::Opcode::FSUB |
	 1l << Instruction::Opcode::FDIV |
	 1l << Instruction::Opcode::FMUL |
	 1l << Instruction::Opcode::FTOS |
	 1l << Instruction::Opcode::STOF |
	 1l << Instruction::Opcode::FSET
	);

const constexpr u64 OPCODE_TYPE_CMP =
	(1l << Instruction::Opcode::IFEQ  |
	 1l << Instruction::Opcode::IFNE  |
	 1l << Instruction::Opcode::IFULE |
	 1l << Instruction::Opcode::IFUL  |
	 1l << Instruction::Opcode::IFUGE |
	 1l << Instruction::Opcode::IFUG  |
	 1l << Instruction::Opcode::IFSLE |
	 1l << Instruction::Opcode::IFSL  |
	 1l << Instruction::Opcode::IFSGE |
	 1l << Instruction::Opcode::IFSG  |
	 1l << Instruction::Opcode::IFFLE |
	 1l << Instruction::Opcode::IFFL  |
	 1l << Instruction::Opcode::IFFGE |
	 1l << Instruction::Opcode::IFFG
	);

const constexpr u64 OPCODE_TYPE_LSU =
	(1l << Instruction::Opcode::LOAD   |
	 1l << Instruction::Opcode::STORA  |
	 1l << Instruction::Opcode::STORB
	);

const constexpr u64 OPCODE_TYPE_ALL =
	(OPCODE_TYPE_ALU |
	 OPCODE_TYPE_FPU |
	 OPCODE_TYPE_CMP |
	 OPCODE_TYPE_LSU
	);


#endif
