/////////////////////////////////////////////////////////////////
// Recursive Fibonaci number implementation
/////////////////////////////////////////////////////////////////
//
// r00 -> function return value
// r01 -> function argument
// r02 -> temp
// r05 -> counter
// r06 -> pointer to next slot in results array
// r29 -> stack pointer
//
// Upon entering a function stack pointer (r29) points
// directly at the return address and r01 contains the function
// argument. The function should thus increment the stack pointer
// and store its argument if it needs to call sub-functions

	r29 = uset stack_pointer // Initialize stack pointer
	r05 = uset 0             // initialize counter = 0
	r06 = uset results       // load pointer to results array

loop:
	r01 = uset r05             // set function argument
	storb r29  [  pc + 8 ]     // store return address
	pc = uset fib              // call function
	stora r00 r06              // store result into array
	r06 = uadd r06 8           // increment pointer to next slot in results array
	r05 = uadd r05 1           // increment counter
	r02 = iful r06 end_results // if not yet at end of results...
	pc  = cset r02 loop        // ...then return to start of loop
	pc  = uset end_program     // jump to halt

fib: // fib(x)
	r02 = ifug r01 1         // If x > 1
	pc  = cset r02 compute   // ...jump to compute recursive section
	r00 = uset r01           // else, result = x
	pc  = load r29           // return

compute:
	// Stack layout for fib after stack pointer increment is as follows
	//  | offset | what                |
	//  |--------|-------------------- |
	//  | - 24   | return value        |
	//  | - 16   | r01                 |
	//  | -  8   | fib(x-1)            |
	//  |    0   | next return address | <---- stack_pointer (r29)
	r29 = uadd r29 24
	stora  r01  [ r29 - 16 ] // store function argument

	r01 = usub r01 1         // decrement function argument
	storb r29  [  pc + 8 ]   // store return address
	pc = uset fib            // call function
	stora r00   [ r29 - 8 ]  // store return value

	r01 = load [ r29 - 16 ]  // Load function argument
	r01 = usub r01 2         // double decrement function argument
	storb r29  [  pc + 8 ]   // store return address
	pc = uset fib            // call function

	r02 = load [ r29 - 8 ]   // load previous return value
	r00 = uadd r00 r02       // compute final result

	r01 = load [ r29 - 16 ]  // restore r01
	r29 = usub r29 24        // decrement stack pointer
	pc  = load r29           // return to caller


end_program:
	halt

results:
	0
	0
	0
	0
	0

	0
	0
	0
	0
	0

	0
	0
	0
	0
	0

	0
	0
	0
	0
	0
end_results:
	0
	0
	0
stack_pointer:
