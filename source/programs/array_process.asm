/////////////////////////////////////////////////////////////////
// Simple program which computes the sum of a list of integers
/////////////////////////////////////////////////////////////////

// Register assignments:
// r00 -> cumulative sum of a*b
// r01 -> cumulative sum of |c|

// r03 -> array max offset
// r05 -> cmp result

// r10 -> array offset

// r20 -> a current value
// r21 -> b current value
// r22 -> c current value
// r24 -> temp
// r25 -> temp

	r03 = uset array_b_start
	r03 = usub r03 array_a_start

loop:
	r20 = load r10 array_a_start // load a
	r21 = load r10 array_b_start // load b
	r22 = load r10 array_c_start // load c

	r24 = smul r20 r21  // compute a*b
	r00 = uadd r00 r24  // add a*b to total

	r25 = umul r22 -1   // compute -c
	r26 = ifsl r22 0    // if c < 0
	r25 = cset r26 r25  // then c = -c
	r01 = uadd r01 r25  // add |c| to total

	r10 = uadd r10 8    // increment element offset

	r05 = iful r10 r03  // loop if still not done
	pc = cset r05 loop

	r00 = sadd r00 r01
	halt

array_a_start:
-3
8
0
6
9
10
2
8
9
-10
-4
-2
-2
5
10
2
-1
2
9
-4
5
9
-1
7
6
1
-7
0
7
6
6
-6
-3
-4
9
-8
4
10
-2
7
8
9
-9
8
-10
-10
10
10
-3
-5
-6
-1
0
3
7
6
-9
4
-4
1
7
-7
-5
-7
3
4
1
-3
-3
9
-1
4
1
-6
-2
-1
-4
5
9
-9
10
9
-9
3
7
9
-1
-4
4
-9
6
3
-6
5
0
8
-6
2
8
10

array_b_start:
4
3
6
7
-9
6
6
-5
-5
-9
-6
3
9
-5
10
-7
9
-10
8
1
10
-6
0
-5
-6
-4
-6
-1
-9
-4
-6
9
-10
3
5
-4
-5
-9
1
0
7
-10
7
-8
2
-9
8
-6
-5
-8
4
-4
-4
-7
-5
6
9
-9
3
2
-4
2
-10
-1
-10
-7
4
10
9
10
-2
0
1
3
-1
7
-3
-6
9
-8
-2
7
4
-6
-9
4
3
-9
9
3
10
-1
-10
-8
3
-1
-3
4
-8
1

array_c_start:
-1
8
6
0
0
-8
-9
-6
-8
-3
2
2
-4
8
-1
-5
-9
-3
7
10
4
-4
-9
4
-6
6
-8
-8
-6
3
10
-4
8
4
8
2
8
-3
6
-7
-6
7
-6
4
0
-7
-4
-9
-1
2
-7
10
3
1
1
0
1
9
10
-2
3
-2
3
-1
3
-5
4
-4
7
0
9
-4
-3
-5
-4
8
8
7
-8
5
-1
-3
4
5
-4
9
-9
10
9
3
8
8
-9
9
-9
2
7
6
-2
-9
array_c_end:
