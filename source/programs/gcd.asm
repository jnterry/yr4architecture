/////////////////////////////////////////////////////////////////
// Computes the greatest common divisor of two integers a and b
//
// Register assignments:
// r00 -> a
// r01 -> b
//
// rest are temp values
//
// Result can be read out of r00 upon termination
/////////////////////////////////////////////////////////////////

	r00 = load null val_a
	r01 = load null val_b

gcd_loop:
	r03 = udiv r00 r01 // r03 = a/b
	r04 = umul r03 r01 // compute remainder...
	r05 = usub r00 r04 // ... done
	r00 = uset r01     // update a
	r01 = uset r05     // update b

	pc  = cset r01 gcd_loop  // if b is not yet 0 then restart loop

	// done
	r02 = uset r00
	halt

//val_a: 12375365206 // 1234*10028659
//val_b: 19795828    // 1234*16042

val_a: 460962 // 18*25609
val_b: 214362	// 18*11909

//val_a: 270 // 6*45
//val_b: 192 // 6*32

// two prime numbers
//val_a: 992302881197
//val_b: 8202865969
