/////////////////////////////////////////////////////////////////
// Tests displaying a pattern on the display
/////////////////////////////////////////////////////////////////

// Register assignments:
// r00 -> current iteration
// r01 -> pixel color
// r02 -> current pixel x
// r03 -> current pixel y
// r04 -> framebuffer_base address
//
// r10 -> temp pixel offset address
// r11 -> temp pixel offset address
// r12 -> temp pixel offset address
// r15 -> misc temp
// r16 -> branch cmp temp
//
// r20 -> temp color calc
// r21 -> temp color calc
//
// r25 -> screen x
// r26 -> screen y
// r27 -> disp reqeust swap address

	r25 = load disp_screen_x // load pointer to screen_x
	r25 = load r25           // load screen_x
	r26 = load disp_screen_y // load pointer to screen_y
	r26 = load r26           // load screen_y
	r27 = load disp_swap_request

for_frame:
	r03 = uset 0 // set y to 0

	r04 = load disp_framebuffer_ptr // load framebuffer**
	r04 = load r04                  // load framebuffer*

for_y:
	r02 = uset 0 // set x to 0

	// set current color to opaque black
	r01 = load color_maska

	// compute blue component based on iteration value
	r20 = umul r00 15 // work out component value
	r20 = and r20 255 // mod component_value by 255
	r21 = shl r20 32  // shift to high color place
	r21 = or  r20 r21 // or in low color value
	r21 = shl r21  8  // shift from a place to b place
	r01 = or  r01 r21 // update color's b channels

	// compute red component based on pixel y value
	r20 = umul r00   5  // work out component value
	r20 = usub r03 r20  // "      "       "
	r20 = and  r20 255  // mod component_value by 255
	r20 = shl  r20  24  // shift from a place to r place of high color
	r21 = shl  r20  32  // duplicate to high color
	r20 = or   r20 r21  // or low and high color into single 64 bit word
	r01 = or   r01 r20  // update color's r channels

for_x:
	// compute green component based on pixel x value
	r21 = load color_maskg // load g mask
	r21 = inv  r21         // invert g mask
	r01 = and  r01 r21     // clear existing g values

	r20 = umul r00  10     // work out component value
	r20 = usub r02 r20     // "    "     "    "
 	r20 = and r20  255     // mod component_value by 255
	r20 = shl r20   16     // shift from a place to g place of high color
	r21 = shl r20   32     // shift to high color
	r01 = or  r01  r20     // or in low color
	r01 = or  r01  r21     // or in high color

	// Compute target address
	r10 = umul r03 r25 // compute cur_y * screen_width
	r10 = uadd r10 r02 // add on the x coord
	r10 = umul r10 4   // mul by 4 to go from pixel index to memory address (as 4 bytes per pixel)
	r10 = uadd r10 r04 // add on the base address of the framebuffer

	// Write pixel value to screen
	stora r01 r10

	// Increment x
	r02 = uadd r02 2

	// end of for_x loop
	r16 = iful r02 r25
	pc = cset r16 for_x

	// Increment y
	r03 = uadd r03 1

	// end of for_y loop
	r16 = iful r03 r26
	pc = cset r16 for_y

	// Request display driver swap the buffers
	storb r27 1

	// Busy loop while we wait for the display driver to have updated
	// the active buffer
//wait_for_swap:
//	r15 = load r27
//	ifne r15 0
//	goto wait_for_swap

	// increment iteration (IE: frame number)
	r00 = uadd r00 1

	// if not completed all iterations
	r16 = iful r00 10
	pc = cset r16 for_frame

	halt

////////////////////////////////////////
// Values to be loaded by program
color_maskr: 0xff000000ff000000
color_maskg: 0x00ff000000ff0000
color_maskb: 0x0000ff000000ff00
color_maska: 0x000000ff000000ff

////////////////////////////////////////
// Pointers to memory locations for doing memory mapped IO
// with the display driver
disp_screen_x:         0xff0000dd00000000
disp_screen_y:         0xff0000dd00000008
disp_swap_request:     0xff0000dd00000010
disp_framebuffer_ptr:  0xff0000dd00000018
