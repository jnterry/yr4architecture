/////////////////////////////////////////////////////////////////
// Dummy program with optimal mix of instructions that can
// achieve maximum instruction throughput
/////////////////////////////////////////////////////////////////

start:
	r00 = load 0
	r01 = load 8
	r02 = uadd r02 1
	r03 = uadd r03 1
	r04 = uadd r04 1
	r05 = uadd r05 1

	r06 = load 16
	r07 = load 24
	r08 = uadd r08 1
	r09 = uadd r09 1
	r20 = iful r09 10000
	pc  = cset r20 start

	halt
