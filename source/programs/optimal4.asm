/////////////////////////////////////////////////////////////////
// Dummy program with optimal mix of instructions that can
// achieve maximum instruction throughput
/////////////////////////////////////////////////////////////////

start:
	r00 = uadd r00 1
	r01 = uadd r01 1
	r02 = uadd r03 1
	r03 = uadd r03 1
	r04 = uadd r04 1
	r05 = uadd r05 1

	r06 = uadd r06 1
	r07 = uadd r07 1
	r08 = uadd r08 1
	r09 = uadd r09 1
	r20 = iful r09 10000
	pc  = cset r20 start

	halt
