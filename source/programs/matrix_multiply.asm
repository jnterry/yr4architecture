/////////////////////////////////////////////////////////////////
// Multiplication of 2 3x3 floating matricies
//
// Designed to test if execution of multiple floating point operations
// can be performed per cycle
//
// Register assignments:
// [ r00 r01 r02 ]   [ r09 r10 r11 ]
// [ r03 r04 r05 ] x [ r12 r13 r14 ]
// [ r06 r07 r08 ]   [ r15 r16 r17 ]
//
// r20 -> mat a start address
// r21 -> mat b start address
// r22 -> mat c start address
//
// r25 -> temp a
// r26 -> temp b
// r27 -> temp c
/////////////////////////////////////////////////////////////////

// load start addresses
	r20 = uset mat_a
	r21 = uset mat_b
	r22 = uset mat_c

// load mat a
	r00 = load [ r20 +  0 ]
	r01 = load [ r20 +  8 ]
	r02 = load [ r20 + 16 ]
	r03 = load [ r20 + 24 ]
	r04 = load [ r20 + 32 ]
	r05 = load [ r20 + 40 ]
	r06 = load [ r20 + 48 ]
	r07 = load [ r20 + 56 ]
	r08 = load [ r20 + 64 ]

// load mat b
	r09 = load [ r21 +  0 ]
	r10 = load [ r21 +  8 ]
	r11 = load [ r21 + 16 ]
	r12 = load [ r21 + 24 ]
	r13 = load [ r21 + 32 ]
	r14 = load [ r21 + 40 ]
	r15 = load [ r21 + 48 ]
	r16 = load [ r21 + 56 ]
	r17 = load [ r21 + 64 ]

// result 00
	r25 = fmul r00 r09
	r26 = fmul r01 r12
	r27 = fmul r02 r15
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 +  0 ]

// result 01
	r25 = fmul r00 r10
	r26 = fmul r01 r13
	r27 = fmul r02 r16
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 +  8 ]

// result 02
	r25 = fmul r00 r11
	r26 = fmul r01 r14
	r27 = fmul r02 r17
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 16 ]

// result 10
	r25 = fmul r03 r09
	r26 = fmul r04 r12
	r27 = fmul r05 r15
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 24 ]

// result 11
	r25 = fmul r03 r10
	r26 = fmul r04 r13
	r27 = fmul r05 r16
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 32 ]

// result 12
	r25 = fmul r03 r11
	r26 = fmul r04 r14
	r27 = fmul r05 r17
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 40 ]

// result 20
	r25 = fmul r06 r09
	r26 = fmul r07 r12
	r27 = fmul r08 r15
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 48 ]

// result 21
	r25 = fmul r06 r10
	r26 = fmul r07 r13
	r27 = fmul r08 r16
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 56 ]

// result 22
	r25 = fmul r06 r11
	r26 = fmul r07 r14
	r27 = fmul r08 r17
	r25 = fadd r25 r26
	r25 = fadd r25 r27
	stora r25 [ r22 + 64 ]

	halt

///////////////////////////////////////////////
// Data for the matricies to multiply - these have been constructed
// such that expected result is
// [ 1 2 3 ]
// [ 3 2 1 ]
// [ 1 2 3 ]

mat_a:
	 1.50
	-1.00
	 3.00
	 5.50
	 1.75
	-2.25
	 1.25
	 8.50
	 3.00

padding: 0

mat_b:
	0.563798219584569800
	0.526211671612265000
	0.488625123639960360
	0.014836795252225712
	0.013847675568744028
	0.012858555885262357
	0.056379821958457000
	0.408176722716782100
	0.759973623475107100

padding: 0

mat_c:
