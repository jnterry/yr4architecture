
	// r00 -> r14 function temporary registers
	//
	// r15 -> display width
	// r16 -> display height
	// r17 -> frame pointer
	// r18 -> request swap buffer pointer
	//
	// r20 -> r25 function arguments
	// r27 -> frame counter
	// r28 -> ball address
	// r29 -> stack pointer


	r15 = load disp_screen_x        // r15 = load disp_screen_x*
	r15 = load r15                  // r15 = *r15
	r16 = load disp_screen_y        // r16 = load disp_screen_y*
	r16 = load r16                  // r16 = *r16
	r18 = load disp_swap_request    // r18 = load disp_swap_request
	r29 = uset stack_base           // load initial stack pointer

for_frame:
	// Request display driver swap the buffers
	storb r18 1

	// Wait for swap to be performed by display driver
wait_for_swap:
	r01 = load r18
	pc = cset r01 wait_for_swap

	r17 = load disp_framebuffer_ptr // r17 = load framebuffer**
	r17 = load r17                  // r17 = *r17

	///////////////////////////////////
	// Compute physics
	r20 = uset update_ballpos
	storb r29 [ pc + 8 ]   // store return address
	pc  = uset foreach_ball// call function
	///////////////////////////////////

	///////////////////////////////////
	// Render Balls
	r20 = uset plot_circle
	storb r29 [ pc + 8 ]   // store return address
	pc  = uset foreach_ball// call function
	///////////////////////////////////

	// Increment frame counter
	r27 = uadd r27 1

	// Jump to start of loop if frames remaining
	r01 = iful r27 100
	pc = cset r01 for_frame

	halt

////////////////////////////////////////////////////////////////////////////////

plot_circle:
	// r00 -> temp
	// r01 -> temp
	// r02 -> temp
	// r03 -> cur_x
	// r04 -> cur_y
	// r05 -> min_x
	// r06 -> min_y
	// r07 -> max_x
	// r08 -> max_y
	// r09 -> base address (top row)
	// r10 -> delta_y of current row
	// r11 -> delta_x_left pixel
	// r12 -> delta_x_right pixel
	// r13 -> radius squared
	// r14 -> base address (bottom row)
	// r20 -> s64 cx
	// r21 -> s64 cy
	// r22 -> s64 radius
	// r23 -> pixel color

	r00 = uset r20
	r20 = load [ r00 +  0 ]
	r21 = load [ r00 +  8 ]
	r22 = load [ r00 + 16 ]
	r23 = load [ r00 + 24 ]
	r20 = ftos r20
	r21 = ftos r21
	r22 = ftos r22

	// Find minx and miny
	r05 = ssub r20 r22 // minx = cx - radius
	r06 = ssub r21 r22 // miny = cy - radius

	// Clamp minx to be > 0
	r01 = ifsl r05 0
	r05 = cset r01 0

	// Clamp miny to be > 0
	r01 = ifsl r06 0
	r06 = cset r01 0

	// Find maxx and maxy
	r07 = sadd r20 r22 // maxx = cx + radius
	r08 = sadd r21 r22 // maxy = cy + radius

	// Clamp maxx <= screen_x
	r01 = ifsg r07 r15
	r07 = cset r01 r15

	// Clamp maxy <= screen_y
	r01 = ifsg r08 r16
	r08 = cset r01 r16

	// compute radius squared
	r13 = umul r22 r22

	// Loop over the bounding box pixels
	r04 = sset r06
plot_circle_loop_y:
	r00 = udiv r17   4 // convert framebuffer base to pixel offset

	// Compute base pixel offset for top row being written
	r09 = umul r04 r15 // compute cur_y * screen_width
	r09 = uadd r09 r00 // add on framebuffer base

	// Compute base pixel offset for bottom row being written
	// (reflection of the top being computed)
	r14 = usub r21 r04 // compute delta between row and center
	r14 = umul r14 2   // double delta
	r14 = umul r14 r15 // multiply delta by screen_width
	r14 = uadd r14 r09 // add on the standard base

	r10 = ssub r21 r04 // compute delta_y
	r10 = smul r10 r10 // delta_y = delta_y * delta_y

	r03 = sset r05
plot_circle_loop_x:
	r00 = uset r23 // set color

	/////////////////////////////////////////
	// Compute pixel mask depending on if circles are inside circle
	r11 = ssub r20 [ r03 + 0 ] // delta_x_left  = cx - (cur_x+0)
	r12 = ssub r20 [ r03 + 1 ] // delta_x_right = cx - (cur_x+1)
	r11 = smul r11 r11 // square delta_x_left
	r12 = smul r12 r12 // square delta_x_right
	r11 = sadd r11 r10 // add delta_y^{2} to get total delta
	r12 = sadd r12 r10 // add delta_y^{2} to get total delta

	r01 = ifsg r11 r13 // check if left pixel is in circle
	r02 = load pixel_mask_left
	r02 = and r00 r01  // and color with mask
	r00 = cset r01 r02

	r01 = ifsg r12 r13 // check if right pixel is in circle
	r02 = load pixel_mask_right
	r02 = and r00 r01  // and color with mask
	r00 = cset r01 r02

	// Write pixels
	r01 = uadd r09 r03 // add on the x coord to base pointer
	r01 = umul r01 4   // mul by 4 to go from pixel index to memory address (as 4 bytes per pixel)
	stora r00 r01
	r02 = uadd r14 r03 // add on the x coord to base pointer
	r02 = umul r02 4   // mul by 4 to go from pixel index to memory address (as 4 bytes per pixel)
	stora r00 r02

	// increment x and conditionally return to start of loop
	r03 = uadd r03 2
	r01 = ifule r03 r07
	pc = cset r01 plot_circle_loop_x

	// increment y and conditionally return to start of loop
	r04 = uadd r04 1
	r01 = ifule r04 r21
	pc = cset r01 plot_circle_loop_y

	// return from function
	pc = load r29

	halt
////////////////////////////////////////////////////////////////////////////////

// r20 -> function pointer for the function to call for each ball
//        at call time r20 will be set to address of ball data
foreach_ball:
	r29 = uadd r29 16
	stora r20 [ r29 - 8 ]

	r28 = uset ball_data
foreach_ball_loop:
	r20 = uset r28                    // set function argument
	storb r29 [ pc + 8 ]              // store return address
	pc  = load [ r29 - 8 ]            // call per-ball function
	r28 = uadd r28 48                 // advance to next ball
	r01 = iful r28 ball_data_end      // if not done...
	pc  = cset r01 foreach_ball_loop  // ...goto start of loop

	// return from function
	r29 = usub r29 16
	pc  = load r29

	halt
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
update_ballpos:
	r00 = uset r20
	r20 = load [ r00 +  0 ] // r20 -> ball pos x
	r21 = load [ r00 +  8 ] // r21 -> ball pos y
	r22 = load [ r00 + 16 ] // r22 -> ball radius
	r23 = load [ r00 + 32 ] // r23 -> ball vel x
	r24 = load [ r00 + 40 ] // r24 -> ball vel y

	r01 = stof r15          // r01 -> screen size x as float
	r02 = stof r16          // r02 -> screen size y as float
	r03 = load constant_neg_1

	r01 = fsub r01 r22
	r02 = fsub r02 r22

	// Compute new position
	r20 = fadd r20 r23
	r21 = fadd r21 r24

	// Update velocity
	r05 = load constant_gravity
	r24 = fadd r24 r05

	// if x is too low
	r04 = iffg r20 r22
	pc  = cset r04 [ pc + 12 ] // skip if condition not met
	r23 = fmul r23 r03         // reverse x vel
	r20 = fsub r22 r03         // clamp position

	// if x is too high
	r04 = iffl r20 r01
	pc  = cset r04 [ pc + 12 ] // skip if condition not met
	r23 = fmul r23 r03         // reverse x vel
	r20 = fadd r01 r03         // clamp position

	// if y is too low
	r04 = iffg r21 r22
	pc  = cset r04 [ pc + 12 ] // skip if condition not met
	r24 = fmul r24 r03         // reverse y vel
	r21 = fsub r22 r03         // clamp position

	// if y is too high
	r04 = iffl r21 r02
	pc  = cset r04 [ pc + 12 ] // skip if condition not met
	r24 = fmul r24 r03         // reverse y vel
	r21 = fadd r02 r03         // clamp position

	stora r20 [ r00 +  0 ]
	stora r21 [ r00 +  8 ]
	stora r23 [ r00 + 32 ]
	stora r24 [ r00 + 40 ]

	// function return
	pc = load r29

	halt
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////
// Constants required by program too large to encode as immediates
pixel_mask_left:  0xffffffff00000000
pixel_mask_right:	0x00000000ffffffff

////////////////////////////////////////
// Pointers to memory locations for doing memory mapped IO
// with the display driver
disp_screen_x:         0xff0000dd00000000
disp_screen_y:         0xff0000dd00000008
disp_swap_request:     0xff0000dd00000010
disp_framebuffer_ptr:  0xff0000dd00000018
constant_gravity:      0.05
constant_neg_1:	       -1.0

ball_data:
	100.0              // pos x
	30.0               // pos y
	12.0               // radius
	0xff0000ffff0000ff // color
	-1.0               // vel x
	-2.0               // vel y

	60.0               // pos x
	60.0               // pos y
	 8.0               // radius
	0x00ff00ff00ff00ff // color
	1.5                // vel x
	2.0                // vel y

	90.0               // pos x
	90.0               // pos y
	10.0               // radius
	0x0000ffff0000ffff // color
	0.7                // vel x
	0.0                // vel y

	30.0               // pos x
	90.0               // pos y
	15.0               // radius
	0xffffffffffffffff // color
	3.0                // vel x
	0.0                // vel y
ball_data_end:

////////////////////////////////////////
// Base of the stack pointer
stack_base:
