/////////////////////////////////////////////////////////////////
// Bubble sort with early exit
/////////////////////////////////////////////////////////////////

// Register assignments:
// r00 -> pointer to current element
// r01 -> array start address
// r02 -> array end address
// r03 -> was swap made this iteration of loop?
// r04 -> temp
// r10 -> element A (IE: *(r00+0))
// r11 -> element B (IE: *(r00+8))

	r01 = uset array_start // load pointer to array into r01
	r02 = uset array_end   // load value of array_length into r02

outer_loop:
	r03 = uset 0           // reset flag of whether we have made a swap
	r00 = uset r01         // i = 0
inner_loop:
	r10 = load [ r00 + 0 ] // load array[i+0]
	r11 = load [ r00 + 8 ] // load array[i+1]
	r04 = ifsle r10 r11    // if already in order...
	pc = cset r04 end_swap // ...skip swap sequence
	stora r11 [ r00 + 0 ]  // array[i+0] = array[i+1]
	stora r10 [ r00 + 8 ]  // array[i+1] = array[i+0]
	r03 = uset 1           // mark the fact we have swapped
end_swap:
	r00 = uadd r00 8           // increment i
	r04 = iful r00 [ r02 - 8 ] // if more elements to examine this iteration...
	pc = cset r04 inner_loop   // ...return to the start
	r02 = usub r02 8           // don't bother examining last element again
	pc = cset r03 outer_loop   // repeat if we made at least 1 swap this iteration
	halt

// numbers -30 to 30 in 5 interval steps, and [-1, -1, -1, 1, 1, 1]
// shuffled into random order
array_start:
	15
	1
	-10
	10
	5
	-1
	-1
	-20
	0
	30
	20
	-5
	-30
	1
	-25
	-1
	1
	25
	-15
array_end:
