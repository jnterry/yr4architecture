	r28 = load const_float_1
	r29 = load const_float_10000

start:
	r00 = fadd r00 r28
	r01 = fadd r01 r28
	r02 = fadd r02 r28
	r03 = fadd r03 r28
	r04 = fadd r04 r28
	r05 = fadd r05 r28

	r06 = fadd r06 r28
	r07 = fadd r07 r28
	r08 = fadd r08 r28
	r09 = fadd r09 r28
	r20 = iffl r09 r29
	pc  = cset r20 start

	halt

const_float_1:         1.0
const_float_10000: 10000.0
