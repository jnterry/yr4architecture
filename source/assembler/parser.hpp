////////////////////////////////////////////////////////////////////////////
/// \brief Contains interface to the parser module
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_ASSEMBLER_PARSER_HPP
#define PROCSIM_ASSEMBLER_PARSER_HPP

#include "types.hpp"

/// Parses AsmEntries from some input text, returns number
/// of parsed entries or 0 upon failure
int parseEntries(char* input, AsmEntry* entries);

#endif
