////////////////////////////////////////////////////////////////////////////
/// \brief Contains definition of parser
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_ASSEMBER_PARSER_CPP
#define PROCSIM_ASSEMBER_PARSER_CPP

#include "parser.hpp"
#include <procsim/register.hpp>

#include <cstring>

bool isAlphaNumeric(char c){
	return ((c >= 'a' && c <= 'z') ||
	        (c >= 'A' && c <= 'Z') ||
	        (c >= '0' && c <= '9')
	       );
}

char* eatWhitespace(char* in){
	while(*in == ' ' || *in == '\n' || *in == '\r' || *in == '\t'){
		++in;
	}
	return in;
}

bool parseConstant(u64 line_num, const char* start, Constant* result){
	result->negative = false;
	result->floating = false;

	if(*start == '-'){
		result->negative = true;
		++start;
	} else if (*start == '+'){
		++start;
	}

	if (*start == '\0'){
		printf("%5lu | Attempted to parse constant from empty string\n", line_num);
		return false;
	}

	if(start[0] == '0' && start[1] != '\0' && start[2] != '\0'){
		if(start[1] == 'x'){
			// Then parse hex constant
			result->value_int = 0;
			for(const char* cur = &start[2]; *cur != '\0'; ++cur){
				result->value_int <<= 4;
				switch(*cur){
				case '0': result->value_int |= 0x0; break;
				case '1': result->value_int |= 0x1; break;
				case '2': result->value_int |= 0x2; break;
				case '3': result->value_int |= 0x3; break;
				case '4': result->value_int |= 0x4; break;
				case '5': result->value_int |= 0x5; break;
				case '6': result->value_int |= 0x6; break;
				case '7': result->value_int |= 0x7; break;
				case '8': result->value_int |= 0x8; break;
				case '9': result->value_int |= 0x9; break;
				case 'a': result->value_int |= 0xa; break;
				case 'b': result->value_int |= 0xb; break;
				case 'c': result->value_int |= 0xc; break;
				case 'd': result->value_int |= 0xd; break;
				case 'e': result->value_int |= 0xe; break;
				case 'f': result->value_int |= 0xf; break;
				case 'A': result->value_int |= 0xa; break;
				case 'B': result->value_int |= 0xb; break;
				case 'C': result->value_int |= 0xc; break;
				case 'D': result->value_int |= 0xd; break;
				case 'E': result->value_int |= 0xe; break;
				case 'F': result->value_int |= 0xf; break;
				default:
					printf("%5lu | Invalid hex constant: %s\n", line_num, start);
					return false;
				}
			}
			return true;
		} else if (start[1] == 'b'){
			// Then parse binary constant
			result->value_int = 0;
			for(const char* cur = &start[2]; *cur != '\0'; ++cur){
				result->value_int <<= 1;
				switch(*cur){
				case '0': result->value_int |= 0; break;
				case '1': result->value_int |= 1; break;
				default:
					printf("%5lu | Invalid binary constant: %s\n", line_num, start);
					return false;
				}
			}
			return true;
		}
	}

	// parse either integer or float
	result->value_int = 0;
	const char* cur = &start[0];
	for(cur = &start[0]; *cur != '\0'; ++cur){
		if(*cur >= '0' && *cur <= '9'){
			result->value_int *= 10;
			result->value_int += *cur - '0';
		} else if (*cur == '.'){
			result->value_float = (r64)result->value_int;
			result->floating = true;
			goto parse_float_fraction;
		} else {
			printf("%5lu | Invalid numeric constant: %s\n", line_num, start);
			return false;
		}
	}

	return true;

	parse_float_fraction:
	++cur;
	if(*cur == '\0'){
		printf("%5lu | Invalid float constant - no decimal part provided: %s\n", line_num, start);
		return false;
	}
	float place = 1.0f;
	for(; *cur != '\0'; ++cur){
		place *= 0.1f;
		switch(*cur){
		case '0': result->value_float += 0.0f * place; break;
		case '1': result->value_float += 1.0f * place; break;
		case '2': result->value_float += 2.0f * place; break;
		case '3': result->value_float += 3.0f * place; break;
		case '4': result->value_float += 4.0f * place; break;
		case '5': result->value_float += 5.0f * place; break;
		case '6': result->value_float += 6.0f * place; break;
		case '7': result->value_float += 7.0f * place; break;
		case '8': result->value_float += 8.0f * place; break;
		case '9': result->value_float += 9.0f * place; break;
		default:
			printf("%5lu | Invalid float constant: %s\n", line_num, start);
			return false;
		}
	}
	return true;
}

u08 parseRegister(const char* register_name){
	for(u08 i = 0; i < Register::COUNT; ++i){
		if(strcmp(register_name, REGISTER_NAMES[i]) == 0){
			return i;
		}
	}
	return 255;
}

bool parseRegisterOffset(u64 line_num, AsmEntry* entry, const char** tokens){
	// offset is of the form [ r00 + 10 ]
	//          token index: 0  1  2  3 4

	if(strcmp(tokens[0], "[") != 0){
		printf("%5lu | Expected [ to open register offset notation, got: %s\n",
		       line_num, tokens[0]);
		return false;
	}

	if(strcmp(tokens[4], "]") != 0){
		printf("%5lu | Expected ] to close register offset notation, got: %s\n",
		       line_num, tokens[4]);
		return false;
	}

	bool minus;
	if(strcmp(tokens[2], "+") == 0){
		minus = false;
	} else if(strcmp(tokens[2], "-") == 0){
		minus = true;
	} else {
	  printf("%5lu | Expected either + or - operator in register offset notation, got: %s\n",
		       line_num, tokens[2]);
	  return false;
	}

	entry->instruction.value_register = parseRegister(tokens[1]);
	if(entry->instruction.value_register >= Register::COUNT){
	  printf("%5lu | Bad register name in register offset notation, got: %s\n",
		       line_num, tokens[1]);
	  return false;
	}

	int value = 0;
	for(const char* cur = tokens[3]; *cur != '\0'; ++cur){
		value *= 10;
		if(*cur >= '0' && *cur <= '9'){
			value += (*cur - '0');
		} else {
			printf("%5lu | Bad integer constant in register offset notation, got: %s\n",
			       line_num, tokens[3]);
			return false;
		}
	}

	entry->instruction.value_offset = ( minus ? -value : value);

	return true;
}


bool parseLine(u64 line_num, char* line_start, char* line_end, AsmEntry* entries, u64& next_entry){

	entries[next_entry].line_num = line_num;

	////////////////////////////////////////////////////////////////
	// First check if there is a label at the start of the line
	for(char* cur = line_start; cur < line_end; ++cur){
		if(isAlphaNumeric(*cur) || *cur == '_'){
			continue;
		}
		if(*cur == ':'){
			// then we've found a label
			*cur = '\0';
			entries[next_entry].type = AsmEntry::LABEL;
			entries[next_entry].label.name = line_start;
			++next_entry;

			++cur;

			while((*cur == ' ' || *cur == '\t') && cur < line_end){
				++cur;
			}

			if(cur == line_end){
				// then we're done with this line, it was just a label
				return true;
			} else {
				return parseLine(line_num, cur, line_end, entries, next_entry);
			}
		}
	}
	////////////////////////////////////////////////////////////////

	char* tokens[32];
	u32 token_count = 0;

	char* cur = line_start;
	while(cur < line_end){
		tokens[token_count] = cur;
		++token_count;

		while(*cur != ' ' && *cur != '\t' && cur < line_end){
			++cur;
		}
		*cur = '\0';
		++cur;
		while((*cur == ' ' || *cur == '\t') && cur < line_end){
			++cur;
		}
	}

	//printf("%5lu : ", line_num);
	//for(u32 i = 0; i < token_count; ++i){
	//	printf("| %s ", tokens[i]);
	//}
	//printf("|\n");

	if(token_count == 1){ // must be a single constant value, or "halt" instruction
		if(strcmp(tokens[0], Instruction::NAMES[Instruction::Opcode::HALT]) == 0){
			entries[next_entry].type = AsmEntry::INSTRUCTION;
			entries[next_entry].instruction.opcode = Instruction::Opcode::HALT;
			entries[next_entry].instruction.register_dest = Register::RNULL;
			++next_entry;
			return true;
		}

		if(!parseConstant(line_num, tokens[0], &entries[next_entry].constant)){
			return false;
		}
		entries[next_entry].type = AsmEntry::CONSTANT;
		++next_entry;
		return true;
	}

	entries[next_entry].type = AsmEntry::INSTRUCTION;
	entries[next_entry].instruction.register_dest = 255;

	int first_instruction_token = 0;

	auto& ins = entries[next_entry].instruction;

	bool errors = false;

	/////////////////////////////////////
	// parse register assignment
	if(strcmp(tokens[1], "=") != 0){
		// then this must be of the form opcode ARG ARG
	  ins.register_dest = Register::RNULL;
	} else {
		first_instruction_token = 2;
		// then this is of the form:
		// register = INSTRUCTION

	  ins.register_dest = parseRegister(tokens[0]);

		if(ins.register_dest > Register::COUNT){
			printf("%5lu | Invalid destination register name: %s\n", line_num, tokens[0]);
			errors = true;
		}
	}

	/////////////////////////////////////
	// parse opcode
    ins.opcode = Instruction::Opcode::COUNT;
	for(u08 i = 0; i < Instruction::Opcode::COUNT; ++i){
		if(strcmp(tokens[first_instruction_token], Instruction::NAMES[i]) == 0){
		  ins.opcode = (Instruction::Opcode)i;
          break;
		}
	}
	if(ins.opcode >= Instruction::Opcode::COUNT){
		printf("%5lu | Invalid opcode name: %s\n", line_num, tokens[first_instruction_token]);
		errors = true;
	}

	/////////////////////////////////////
	// parse arguments to opcode depending on number of tokens:
	// 1 -> rB/immedate/label
	// 2 -> rA (rB/immediate/label)
	// 5 -> [ rB + constant ]
	// 6 -> rA [ rB + constant ]

	switch(token_count - first_instruction_token - 1){
	case 2: {
		ins.register_source_a = parseRegister(tokens[first_instruction_token+1]);
		if(ins.register_source_a >= Register::COUNT){
			printf("%5lu | Unknown source register a: %s",
			       line_num, tokens[first_instruction_token+1]
			      );
			errors = true;
		}
	}
		goto parse_register_immediate_label;
	case 1: {
		--first_instruction_token;
		ins.register_source_a = Register::RNULL;
	parse_register_immediate_label:
		u08 reg_b = parseRegister(tokens[first_instruction_token+2]);
		if(reg_b < Register::COUNT){
			ins.value_type     = AsmEntry::VALUE_REGISTER;
			ins.value_register = reg_b;
			ins.value_offset   = 0;
		} else if((*tokens[first_instruction_token+2] >= '0' &&
		           *tokens[first_instruction_token+2] <= '9') ||
		          *tokens[first_instruction_token+2] == '-' ||
		          *tokens[first_instruction_token+2] == '+'){

			if(parseConstant(line_num,
			                 tokens[first_instruction_token+2],
			                 &ins.value_constant
			                 )){
				ins.value_type = AsmEntry::VALUE_CONSTANT;
			} else {
				errors = true;
			}
		} else {
			ins.value_type = AsmEntry::VALUE_LABEL;
			ins.value_label = tokens[first_instruction_token+2];
		}
		break;
	}
	case 5: {
		ins.register_source_a = Register::RNULL;
		ins.value_type = AsmEntry::VALUE_REGISTER;
		if(!parseRegisterOffset(line_num, &entries[next_entry],
		                        (const char**)&tokens[first_instruction_token+1])){
			errors = true;
		}
		break;
	}
	case 6: {
		ins.register_source_a = parseRegister(tokens[first_instruction_token+1]);
		if(ins.register_source_a >= Register::COUNT){
			printf("%5lu | Bad register name for source A, got: %s\n",
			       line_num, tokens[first_instruction_token+1]);
			errors = true;
		}
		ins.value_type = AsmEntry::VALUE_REGISTER;
		if(!parseRegisterOffset(line_num, &entries[next_entry],
		                        (const char**)&tokens[first_instruction_token+2])){
			errors = true;
		}
		break;
	}
	default:
		printf("%5lu | Invalid argument list format, got: ", line_num);
		for(u08 i = first_instruction_token+1; i < token_count; ++i){
			printf("| %s ", tokens[i]);
		}
		printf("|\n");
		errors = true;
		break;
	}

	if(!errors){
		++next_entry;
		return true;
	} else {
		return false;
	}
}

/// Parses AsmEntries from some input text, returns number
/// of parsed entries or 0 upon failure
int parseEntries(char* input, AsmEntry* entries){
	char* line_next_start = input;
	char* line_start;
	char* line_end;

	bool no_errors = true;

	u64 next_entry = 0;

	u64 line_num = 1;
	bool done = false;
	while(!done){
		line_start = line_next_start;

		////////////////////////////////////////
		// Advance to next non-empty line and trim leading whitespace
		while(*line_start == '\n' || *line_start == ' '  || *line_start == '\t'){
			if(*line_start == '\n'){ ++line_num; }
			++line_start;
		}
		////////////////////////////////////////


		////////////////////////////////////////
		// Setup line_end
		line_end = line_start;
		if(*line_end == '\0'){
			done = true;
			break;
		}
		////////////////////////////////////////


		////////////////////////////////////////
		// Find end of line
		while(*line_end != '\0' && *line_end != '\n' && *line_end != '\r'){
			++line_end;
		}
		done = (*line_end == '\0');
		*line_end = '\0';
		line_next_start = &line_end[1];
		////////////////////////////////////////


		////////////////////////////////////////
		// Chop off trailing comment if present
		for(char* cur = line_start; cur < line_end; ++cur){
			if(cur[0] == '/' && cur[1] == '/'){
				cur[0] = '\0';
				line_end = cur;
				break;
			}
		}
		if(line_start == line_end){
			// skip if line was just a comment with no other content
			++line_num;
			continue;
		}
		////////////////////////////////////////

		no_errors &= parseLine(line_num, line_start, line_end, entries, next_entry);

		++line_num;
	}

	if(!no_errors){
		return 0;
	} else {
		return next_entry;
	}
}

#endif
