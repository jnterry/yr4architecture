////////////////////////////////////////////////////////////////////////////
/// \brief Contains interface to the writer module. This module is responsible
/// for outputting the final executable
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_ASSEMBLER_WRITER_HPP
#define PROCSIM_ASSEMBLER_WRITER_HPP

#include "types.hpp"

#include <cstdio>

/// Writes a set of parsed AsmEntries to some file in binary
/// format
bool writeEntries(FILE* fout, AsmEntry* entries, u64 entry_count);

#endif
