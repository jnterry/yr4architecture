////////////////////////////////////////////////////////////////////////////
/// \brief Contains types related to the assembler
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_ASSEMBLER_TYPES_HPP
#define PROCSIM_ASSEMBLER_TYPES_HPP

#include <procsim/instruction.hpp>
#include <xen/core/intrinsics.hpp>

// Disable gcc's warning about anonymous structs in unions temporarily...
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

struct Constant {
	bool negative;
	bool floating;
	union {
		u64 value_int;
		r64 value_float;
	};
};

struct AsmEntry {
	enum Type {
		LABEL,
		CONSTANT,
		INSTRUCTION,
	};

	enum ValueType {
		VALUE_NONE,
		VALUE_REGISTER,
		VALUE_CONSTANT,
		VALUE_LABEL,
	};

	/// \brief The type of this entry
	Type type;

	/// \brief The address of this entry when output to the final executable
	u64  address;

	/// \brief The line num of this token in the asm source
	u64 line_num;

	union {
		struct {
			const char* name;
		} label;

		Constant constant;

		struct {
			Instruction::Opcode opcode;
			u08 register_dest;
			u08 register_source_a;

			ValueType value_type;

			union {
				Constant value_constant;

				struct {
					u08 value_register;
					s16 value_offset;
				};

				const char* value_label;
			};
		} instruction;
	};
};

#pragma GCC diagnostic pop // re-enable -Wpedantic

#endif
