////////////////////////////////////////////////////////////////////////////
/// \brief Contains definition of writer
////////////////////////////////////////////////////////////////////////////

#ifndef PROCSIM_ASSEMBER_WRITER_CPP
#define PROCSIM_ASSEMBER_WRITER_CPP

#include "writer.hpp"

#include <cstdio>
#include <cstring>

bool linkEntries(AsmEntry* entries, u64 entry_count){
    bool errors = false;

	///////////////////////////////////////////////////////////////////
	// First loop over all entries and compute where they will
	// be in the final executable
	u64 cur_address = 0;
	for(u64 i = 0; i < entry_count; ++i){
		entries[i].address = cur_address;
		switch(entries[i].type){
		case AsmEntry::INSTRUCTION : cur_address += 4; break;
		case AsmEntry::CONSTANT    : cur_address += 8; break;
		case AsmEntry::LABEL       : cur_address += 0; break;
		default:
			XenInvalidCodePath("Missing switch case");
		}
	}


	///////////////////////////////////////////////////////////////////
	// Now do a second pass and convert any label references into their actual
	// addresses
	for(u64 i = 0; i < entry_count; ++i){
		if(entries[i].type != AsmEntry::INSTRUCTION ||
		   entries[i].instruction.value_type != AsmEntry::VALUE_LABEL){
			continue;
		}

		const char* target_label = entries[i].instruction.value_label;

		if(/*entries[i].instruction.opcode == Instruction::ADV*/false){
			// Advance is a special instruction, it moves PC in positive direction
			// and can go to a non-unique label, hence just go forward in AsmEntry
			// list until we find the label in question

			bool found = false;
			for(u64 j = i+1; j < entry_count; ++j){
				if(entries[j].type == AsmEntry::LABEL &&
				   strcmp(entries[j].label.name, target_label) == 0){
					entries[i].instruction.value_type = AsmEntry::VALUE_CONSTANT;
					entries[i].instruction.value_constant.negative  = false;
					entries[i].instruction.value_constant.floating  = false;
					entries[i].instruction.value_constant.value_int = entries[j].address;
					found = true;
					break;
				}
			}
			if(!found){
				printf("%5lu | Failed to find target label of adv instruction, label: %s\n",
				       entries[i].line_num, target_label);
				errors = true;
			}
		} else {
			// Then this is an absolute label that may be before or after this
			// instruction, search for it
			bool found = false;
			for(u64 j = 0; j < entry_count; ++j){
				if(entries[j].type == AsmEntry::LABEL &&
				   strcmp(entries[j].label.name, target_label) == 0){
					if(found){
						printf("%5lu | Label ambiguous, label: %s, repeated on line: %lu\n",
						       entries[i].line_num, target_label, entries[j].line_num);
						errors = true;
					} else {
						entries[i].instruction.value_type = AsmEntry::VALUE_CONSTANT;
						entries[i].instruction.value_constant.negative  = false;
						entries[i].instruction.value_constant.floating  = false;
						entries[i].instruction.value_constant.value_int = entries[j].address;
						found = true;
					}
				}
			}
			if(!found){
				printf("%5lu | Failed to find target label: %s\n",
				       entries[i].line_num, target_label);
				errors = true;
			}
		}
	}

	return !errors;
}

template<typename T>
void writeBytes(FILE* fout, T val){
	u08* bytes = (u08*)&val;
	for(int i = 0; i < sizeof(T); ++i){
		fputc(bytes[i], fout);
	}
}

bool writeEntries(FILE* fout, AsmEntry* entries, u64 entry_count){

	printf("######| Linking entries\n");
	if(!linkEntries(entries, entry_count)){
		printf("######| Errors occurred during linking\n");
		return false;
	}


	/*for(u64 i = 0; i < entry_count; ++i){
		const char* type_name = "?";
		switch(entries[i].type){
		case AsmEntry::INSTRUCTION : type_name = "INSTRUCTION"; break;
		case AsmEntry::LABEL       : type_name = "LABEL"; break;
		case AsmEntry::CONSTANT    : type_name = "CONSTANT"; break;
		}

		printf("%5lu | %5lu | %8p | %s\n",
		       i, entries[i].line_num, (void*)entries[i].address, type_name);
		       }*/


  printf("######| Writing executable\n");

  bool errors = false;

  for(u64 i = 0; i < entry_count; ++i){

	  u64 cur_addr = ftell(fout);
	  if(cur_addr != entries[i].address){
		  printf("%5lu | Computed address and actual address have desynchronised - this is a bug in the assembler\n",
		         entries[i].line_num);
		  return false;
	  }

	  switch(entries[i].type){
	  case AsmEntry::INSTRUCTION:
		  Instruction ins;
		  ins.raw      = 0;
		  ins.opcode   = entries[i].instruction.opcode;
		  ins.rdest    = entries[i].instruction.register_dest;
		  ins.source_a = entries[i].instruction.register_source_a;

		  switch(entries[i].instruction.value_type){
		  case AsmEntry::VALUE_CONSTANT: {
			  u64 immediate_bit_mask = (1 << Instruction::IMMEDIATE_BITS)-1;

			  ins.b_is_reg = 0;
			  if(entries[i].instruction.value_constant.floating){
				  printf("%5lu | Floating point immediate values not yet supported\n",
			         entries[i].line_num);
				  errors = true;
			  } else {
				  if(entries[i].instruction.value_constant.negative){
					  s64 value = -((s64)entries[i].instruction.value_constant.value_int);
					  if(value < ((s64)(~0) ^ immediate_bit_mask)){
						  printf("%5lu | Immediate value too low to be encoded into instruction format\n",
						         entries[i].line_num);
						  errors = true;
					  }
					  ins.immediate = value & immediate_bit_mask;
				  } else {
					  u64 value = entries[i].instruction.value_constant.value_int;
					  if(value > (immediate_bit_mask >> 1)){
						  printf("%5lu | Immediate value too large to be encoded into instruction format\n",
						         entries[i].line_num);
						  errors = true;
					  } else {
						  ins.immediate = value & (immediate_bit_mask >> 1);
					  }
				  }
			  }
			  break;
		  }
		  case AsmEntry::VALUE_NONE: {
			  ins.b_is_reg  = 0;
			  ins.immediate = 0;
			  break;
		  }
		  case AsmEntry::VALUE_REGISTER: {
			  ins.b_is_reg = 1;
			  ins.source_b = entries[i].instruction.value_register;
			  int offset = entries[i].instruction.value_offset;
			  if(offset < -128 || offset > 127){
				  printf("%5lu | Register offset value too large to be encoded in instruction format\n",
			         entries[i].line_num);
				  errors = true;
			  } else {
				  ins.offset = (s08)offset;
			  }
			  break;
		  }
		  default:
			  printf("%5lu | Invalid value type for writing to binary\n",
			         entries[i].line_num);
			  errors = true;
		  }

		  writeBytes(fout, ins.raw);
		  break;
	  case AsmEntry::CONSTANT:
		  if(entries[i].constant.floating){
			  if(entries[i].constant.negative){
				  writeBytes(fout, -entries[i].constant.value_float);
			  } else {
				  writeBytes(fout, +entries[i].constant.value_float);
			  }
		  } else {
			  if(entries[i].constant.negative){
				  writeBytes(fout, -entries[i].constant.value_int);
			  } else {
				  writeBytes(fout, +entries[i].constant.value_int);
			  }
		  }
		  break;
	  case AsmEntry::LABEL:
		  break;
	  default:
		  XenInvalidCodePath("Missing switch case");
		  break;
	  }
  }

	return !errors;
}

#endif
