#include <cstdio>
#include <cstdlib>

#include <xen/core/memory/ArenaLinear.hpp>
#include <xen/core/File.hpp>

#include "types.hpp"
#include "parser.hpp"
#include "writer.hpp"

bool assemble(char* input, FILE* fout){
	u64 DATA_SIZE = sizeof(AsmEntry) * 65536;
	AsmEntry* entries = (AsmEntry*)malloc(DATA_SIZE);
	xen::clearToZero(entries, DATA_SIZE);

	printf("######| Parsing ASM...\n");
	int num_entries = parseEntries(input, entries);

	if(num_entries == 0){
		printf("######| Parse errors or empty input, terminating\n");
		free(entries);
		return false;
	}

	printf("######| Parsed program into %i asm entries\n", num_entries);

	if(!writeEntries(fout, entries, num_entries)){
		printf("Error occurred writing asm entries to binary\n");
		free(entries);
		return false;
	}

	free(entries);
	return true;
}

int main(int argc, const char** argv){
	if(argc != 3){
		printf("ERROR: Usage: %s <INPUT_FILE> <OUTPUT_FILE>\n", argv[0]);
		return 1;
	}

	XenTempArena(in_file_arena, xen::kilobytes(128));
	xen::FileData in_file = xen::loadFileAndNullTerminate(in_file_arena, argv[1]);

	if(in_file.size == 0){
		printf("Failed to load input file: %s\n", argv[1]);
		return 2;
	}

	FILE* fout = fopen(argv[2], "wb");

	if(fout == NULL){
		printf("Failed to open output file for writing\n");
		return 3;
	}

	if(!assemble((char*)&in_file[0], fout)){
		printf("Assembling failed\n");
		fclose(fout);
		xen::deleteFile(argv[2]);
		return 4;
	}

	printf("Assembling completed successfully\n");
	printf("Executable size: %lu\n", ftell(fout));
	fclose(fout);

	return 0;
}
