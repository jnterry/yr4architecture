#include "opcode_base.hpp"

TEST_CASE("shl", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::SHL;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard shl"){
		utest::getReg(0).u = 2;
		utest::getReg(1).u = 1;
		utest::getReg(2).u = 5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 32);
		CHECK(utest::getReg(1).u ==  1);
		CHECK(utest::getReg(2).u ==  5);
	}

	SECTION("Self shuft"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).u = 4;
	  utest::getReg(1).u = 5;
	  utest::getReg(2).u = 6;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 4 << 4);
		CHECK(utest::getReg(1).u == 5);
		CHECK(utest::getReg(2).u == 6);
	}

	SECTION("Shift immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 10;
	  utest::getReg(0).u = 3;
	  utest::getReg(1).u = 2;
	  utest::getReg(2).u = 5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 2 << 10);
		CHECK(utest::getReg(1).u == 2);
		CHECK(utest::getReg(2).u == 5);
	}

	SECTION("Shift register with offset"){
	  utest::instructions[0].offset = 8;
	  utest::getReg(0).u = 10;
	  utest::getReg(1).u = 20;
	  utest::getReg(2).u =  5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 20 << 13);
		CHECK(utest::getReg(1).u == 20);
		CHECK(utest::getReg(2).u ==  5);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
