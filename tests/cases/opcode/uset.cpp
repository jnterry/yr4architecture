#include "opcode_base.hpp"

static_assert(Instruction::OFFSET_BITS == 10,
              "Max/min offset test cases rely on this, if changed test cases need to be updated");

TEST_CASE("uset", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::USET;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 0x14f32abe79;
		utest::getReg(2).u = 0x328312fe31;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 0x328312fe31);
		CHECK(utest::getReg(1).u == 0x14f32abe79);
		CHECK(utest::getReg(2).u == 0x328312fe31);
	}

	SECTION("Set with +ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123;
		utest::getReg(2).u = 10;
		utest::instructions[0].offset = 10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 20);
		CHECK(utest::getReg(1).u == 123);
		CHECK(utest::getReg(2).u == 10);
	}

	SECTION("Set with -ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123;
		utest::getReg(2).u = 10;
		utest::instructions[0].offset = -5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 5);
		CHECK(utest::getReg(1).u == 123);
		CHECK(utest::getReg(2).u == 10);
	}

	SECTION("Set with max offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123;
		utest::getReg(2).u = 3;
		utest::instructions[0].offset = 511;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 514);
		CHECK(utest::getReg(1).u == 123);
		CHECK(utest::getReg(2).u ==   3);
	}

	SECTION("Set with min offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123;
		utest::getReg(2).u = 600;
		utest::instructions[0].offset = -512;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==  88);
		CHECK(utest::getReg(1).u == 123);
		CHECK(utest::getReg(2).u == 600);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
