#include "opcode_base.hpp"

TEST_CASE("halt", "[opcode]"){
	utest::initProcState();
	utest::instructions[0].opcode = Instruction::HALT;
	utest::runUntilHalt();

	for(int i = 0; i < Register::LAST_GP; ++i){
		REQUIRE(utest::getReg(i).u == 0);
	}
	REQUIRE(utest::getReg(Register::PC).u == 4);
}
