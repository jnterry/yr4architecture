#include "opcode_base.hpp"

TEST_CASE("or", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::OR;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 0x14f32abe79;
		utest::getReg(2).u = 0x328312fe31;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == (0x14f32abe79 | 0x328312fe31));
		CHECK(utest::getReg(1).u == 0x14f32abe79);
		CHECK(utest::getReg(2).u == 0x328312fe31);
	}

	SECTION("Self"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).u = 0x14f32abe79;
	  utest::getReg(1).u =   5;
	  utest::getReg(2).u =   6;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == (0x14f32abe79 | 0x14f32abe79));
		CHECK(utest::getReg(1).u == 5);
		CHECK(utest::getReg(2).u == 6);
	}

	SECTION("immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 0x13f;
	  utest::getReg(0).u = 0;
	  utest::getReg(1).u = 98765;
	  utest::getReg(2).u = 0;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == (98765 | 0x13f));
		CHECK(utest::getReg(1).u == 98765);
		CHECK(utest::getReg(2).u ==     0);
	}

	SECTION("register with offset"){
	  utest::instructions[0].offset = 2;
	  utest::getReg(0).u = 0;
	  utest::getReg(1).u = 98765;
	  utest::getReg(2).u = 0x13f;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == (98765 | (0x13f + 2)));
		CHECK(utest::getReg(1).u == 98765);
		CHECK(utest::getReg(2).u == 0x13f);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
