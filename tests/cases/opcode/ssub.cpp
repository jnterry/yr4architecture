#include "opcode_base.hpp"

TEST_CASE("ssub", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::SSUB;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard sub"){
		utest::getReg(0).s = 10;
		utest::getReg(1).s = 20;
		utest::getReg(2).s = 5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 15);
		CHECK(utest::getReg(1).s == 20);
		CHECK(utest::getReg(2).s ==  5);
	}

	SECTION("Switch sign"){
		utest::getReg(0).s = 0;
		utest::getReg(1).s = 5;
		utest::getReg(2).s = 9;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -4);
		CHECK(utest::getReg(1).s ==  5);
		CHECK(utest::getReg(2).s ==  9);
	}

	SECTION("Sub negative"){
		utest::getReg(0).s = 100;
		utest::getReg(1).s = -5;
		utest::getReg(2).s = -10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 5);
		CHECK(utest::getReg(1).s == -5);
		CHECK(utest::getReg(2).s == -10);
	}

	SECTION("Self Sub"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).s =  2;
	  utest::getReg(1).s = 10;
	  utest::getReg(2).s = 20;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  0);
		CHECK(utest::getReg(1).s == 10);
		CHECK(utest::getReg(2).s == 20);
	}

	SECTION("Sub immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 5;
	  utest::getReg(0).s = 1;
	  utest::getReg(1).s = 2;
	  utest::getReg(2).s = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -3);
		CHECK(utest::getReg(1).s ==  2);
		CHECK(utest::getReg(2).s ==  3);
	}

	SECTION("Sub register with offset"){
	  utest::instructions[0].offset = 5;
	  utest::getReg(0).s = 10;
	  utest::getReg(1).s = 20;
	  utest::getReg(2).s = 30;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -15);
		CHECK(utest::getReg(1).s == 20);
		CHECK(utest::getReg(2).s == 30);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
