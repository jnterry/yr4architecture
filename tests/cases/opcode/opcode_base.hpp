#ifndef PROCSIM_TEST_OPCODEBASE_HPP
#define PROCSIM_TEST_OPCODEBASE_HPP

#include <catch.hpp>
#include <procsim/instruction.hpp>
#include <procsim/register.hpp>
#include <processor.hpp>

namespace utest {
	void initProcState();
	void runUntilHalt();
	Word64& getReg(u64 reg_index);
	void stepProcessor();

	extern const u64 MEMORY_SIZE;
	extern u08 memory[];
	extern Instruction* instructions;
}

#endif
