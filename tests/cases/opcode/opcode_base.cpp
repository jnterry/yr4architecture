////////////////////////////////////////////////////////////////////////////
/// \brief Test cases for each opcode
////////////////////////////////////////////////////////////////////////////

#include "opcode_base.hpp"

#include <device.hpp>
#include <processor/processor.cpp>
#include <processor/memory_controller.cpp>
#include <processor/branch_predictor.cpp>
#include <xen/core/memory/utilities.hpp>

namespace utest {
	Device devices[1];
	System         test_system;
	ProcessorState* proc;
	const u64 MEMORY_SIZE = xen::kilobytes(16);
	u08 memory[MEMORY_SIZE];
	Instruction* instructions = (Instruction*)memory;

	void initProcState(){
		xen::clearToZero(&test_system);
		xen::clearToZero(memory, MEMORY_SIZE);

		devices[0].memory_start = 0;
		devices[0].memory_end   = MEMORY_SIZE;
		devices[0].state        = &proc;
		devices[0].system       = &test_system;
		devices[0].memory       = memory;

		test_system.devices      = devices;
		test_system.device_count = 1;

		initProcessor(&test_system, &devices[0], nullptr);

		for(int i = 0; i < 100; ++i){
			instructions[i].rdest = Register::RNULL;
		}

		proc = (ProcessorState*)devices[0].state;
	}

	void stepProcessor(){
		tickProcessor(&devices[0]);
	}

	void runUntilHalt(){
		while(!test_system.halted){
			tickProcessor(&devices[0]);
		}
	}

	Word64& getReg(u64 reg_index){
		return proc->regs[reg_index];
	}
}
