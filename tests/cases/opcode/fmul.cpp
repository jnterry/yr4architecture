#include "opcode_base.hpp"

TEST_CASE("fmul", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::FMUL;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard mul"){
		utest::getReg(1).f = 12.5;
		utest::getReg(2).f = 3.25;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 40.625);
		CHECK(utest::getReg(1).f == 12.50);
		CHECK(utest::getReg(2).f ==  3.25);
	}

	SECTION("Mul negative"){
		utest::getReg(0).f = 100.0;
		utest::getReg(1).f = 3.5;
		utest::getReg(2).f = -2.0;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == -7.0);
		CHECK(utest::getReg(1).f == 3.5);
		CHECK(utest::getReg(2).f == -2.0);
	}

	SECTION("Self mul"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).f = 1.25;
	  utest::getReg(1).f = 10.0;
	  utest::getReg(2).f = 20.0;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 1.5625);
		CHECK(utest::getReg(1).f == 10.0);
		CHECK(utest::getReg(2).f == 20.0);
	}

	/*
	SECTION("Sub immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 5;
		utest::getReg(0).s = 1;
		utest::getReg(1).s = 2;
		utest::getReg(2).s = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -3);
		CHECK(utest::getReg(1).s ==  2);
		CHECK(utest::getReg(2).s ==  3);
	}

	SECTION("Sub register with offset"){
	  utest::instructions[0].offset = 5;
		utest::getReg(0).s = 10;
		utest::getReg(1).s = 20;
		utest::getReg(2).s = 30;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -15);
		CHECK(utest::getReg(1).s == 20);
		CHECK(utest::getReg(2).s == 30);
	}
	*/

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
