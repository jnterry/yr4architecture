#include "opcode_base.hpp"

TEST_CASE("shr", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::SHR;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard shr"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123456;
		utest::getReg(2).u = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 123456 >> 3);
		CHECK(utest::getReg(1).u == 123456);
		CHECK(utest::getReg(2).u ==      3);
	}

	SECTION("Self shift"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).u = 123;
	  utest::getReg(1).u =   5;
	  utest::getReg(2).u =   6;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 0);
		CHECK(utest::getReg(1).u == 5);
		CHECK(utest::getReg(2).u == 6);
	}

	SECTION("Shift immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 3;
	  utest::getReg(0).u = 0;
	  utest::getReg(1).u = 98765;
	  utest::getReg(2).u = 5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 98765 >> 3);
		CHECK(utest::getReg(1).u == 98765);
		CHECK(utest::getReg(2).u == 5);
	}

	SECTION("Shift register with offset"){
	  utest::instructions[0].offset = 2;
	  utest::getReg(0).u = 1;
	  utest::getReg(1).u = 55555;
	  utest::getReg(2).u = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 55555 >> 5);
		CHECK(utest::getReg(1).u == 55555);
		CHECK(utest::getReg(2).u ==     3);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
