#include "opcode_base.hpp"

TEST_CASE("ifsl", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::IFSL;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].source_a = Register::R00;
	utest::instructions[0].source_b = Register::R01;

	utest::instructions[1].opcode    = Instruction::USET;
	utest::instructions[1].b_is_reg  = 0;
	utest::instructions[1].immediate = 123;
	utest::instructions[1].rdest     = Register::R02;

	SECTION("Register cmp, true"){
		utest::getReg(0).s = -10;
		utest::getReg(1).s = 0;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  -10);
		CHECK(utest::getReg(1).s ==   0);
		CHECK(utest::getReg(2).u == 123);
	}

	SECTION("Register cmp, false"){
		utest::getReg(0).s = -10;
		utest::getReg(1).s = -15;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  -10);
		CHECK(utest::getReg(1).s ==  -15);
		CHECK(utest::getReg(2).u ==    0);
	}

	SECTION("Register cmp self"){
		utest::getReg(0).s = 10;

		utest::instructions[0].source_b = Register::R00;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  10);
		CHECK(utest::getReg(1).s ==   0);
		CHECK(utest::getReg(2).s ==   0);
	}

	SECTION("Immediate cmp, true"){
		utest::getReg(0).s = -10;
		utest::instructions[0].b_is_reg  = false;
		utest::instructions[0].immediate = 11;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -10);
		CHECK(utest::getReg(1).u ==   0);
		CHECK(utest::getReg(2).u == 123);
	}

	SECTION("Immediate cmp, false"){
		utest::getReg(0).s = -1;
		utest::instructions[0].b_is_reg  = false;
		utest::instructions[0].immediate = (s16)-2;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -1);
		CHECK(utest::getReg(1).s ==  0);
		CHECK(utest::getReg(2).s ==  0);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 12);
}
