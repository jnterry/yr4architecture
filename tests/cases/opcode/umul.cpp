#include "opcode_base.hpp"

TEST_CASE("umul", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::UMUL;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard mul"){
		utest::getReg(0).u = 1;
		utest::getReg(1).u = 2;
		utest::getReg(2).u = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 6);
		CHECK(utest::getReg(1).u == 2);
		CHECK(utest::getReg(2).u == 3);
	}

	SECTION("Self mul"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).u = 4;
	  utest::getReg(1).u = 5;
	  utest::getReg(2).u = 6;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 16);
		CHECK(utest::getReg(1).u ==  5);
		CHECK(utest::getReg(2).u ==  6);
	}

	SECTION("Mul immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 15;
	  utest::getReg(0).u = 1;
	  utest::getReg(1).u = 3;
	  utest::getReg(2).u = 4;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 45);
		CHECK(utest::getReg(1).u ==  3);
		CHECK(utest::getReg(2).u ==  4);
	}

	SECTION("Mul register with offset"){
	  utest::instructions[0].offset = 3;
	  utest::getReg(0).u = 5;
	  utest::getReg(1).u = 6;
	  utest::getReg(2).u = 7;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 60);
		CHECK(utest::getReg(1).u ==  6);
		CHECK(utest::getReg(2).u ==  7);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
