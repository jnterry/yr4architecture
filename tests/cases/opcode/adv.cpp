#include "opcode_base.hpp"

TEST_CASE("adv", "[opcode]"){
	utest::initProcState();

	for(int i = 0; i < 10; ++i){
		utest::instructions[i].opcode = Instruction::SADD;
		utest::instructions[0].rdest  = Register::RNULL;
	}
	utest::instructions[10].opcode   = Instruction::ADV;
	utest::instructions[10].b_is_reg = 1;
	utest::instructions[10].rdest    = Register::R00;
	utest::instructions[10].source_a = Register::R01;
	utest::instructions[10].source_b = Register::R02;

	SECTION("Adv register"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 40 + 200 + 4);
	}

	SECTION("Adv register +ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[10].offset = 10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 40 + 200 + 4 + 10);
	}

	SECTION("Adv register -ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[10].offset = -10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 40 + 200 + 4 - 10);
	}

	SECTION("Adv immediate"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[10].b_is_reg  = 0;
		utest::instructions[10].immediate = 123;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 40 + 123 + 4);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
}
