#include "opcode_base.hpp"

static_assert(Instruction::OFFSET_BITS == 10,
              "Max/min offset test cases rely on this, if changed test cases need to be updated");

TEST_CASE("sset", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::SSET;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard"){
		utest::getReg(0).s = 0;
		utest::getReg(1).s = 0x14f32abe79;
		utest::getReg(2).s = 0x328312fe31;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 0x328312fe31);
		CHECK(utest::getReg(1).s == 0x14f32abe79);
		CHECK(utest::getReg(2).s == 0x328312fe31);
	}

	SECTION("Set with +ve offset"){
		utest::getReg(0).s = 0;
		utest::getReg(1).s = 123;
		utest::getReg(2).s = -30;
		utest::instructions[0].offset = 10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -20);
		CHECK(utest::getReg(1).s == 123);
		CHECK(utest::getReg(2).s == -30);
	}

	SECTION("Set with -ve offset"){
		utest::getReg(0).s = 0;
		utest::getReg(1).s = 123;
		utest::getReg(2).s = 10;
		utest::instructions[0].offset = -15;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -5);
		CHECK(utest::getReg(1).s == 123);
		CHECK(utest::getReg(2).s == 10);
	}

	SECTION("Set with max offset"){
		utest::getReg(0).s = 0;
		utest::getReg(1).s = 123;
		utest::getReg(2).s = -600;
		utest::instructions[0].offset = 511;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -89);
		CHECK(utest::getReg(1).s == 123);
		CHECK(utest::getReg(2).s == -600);
	}

	SECTION("Set with min offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 123;
		utest::getReg(2).u = 100;
		utest::instructions[0].offset = -512;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -412);
		CHECK(utest::getReg(1).s == 123);
		CHECK(utest::getReg(2).s == 100);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
