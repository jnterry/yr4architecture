#include "opcode_base.hpp"

TEST_CASE("goto", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::GOTO;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Goto register"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 204);
	}

	SECTION("Goto register +ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[0].offset = 10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 214);
	}

	SECTION("Goto register -ve offset"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[0].offset = -10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 194);
	}

	SECTION("Goto immediate"){
		utest::getReg(0).u = 0;
		utest::getReg(1).u = 100;
		utest::getReg(2).u = 200;

		utest::instructions[0].b_is_reg  = 0;
		utest::instructions[0].immediate = 123;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u ==   0);
		CHECK(utest::getReg(1).u == 100);
		CHECK(utest::getReg(2).u == 200);
		CHECK(utest::getReg(Register::PC).u == 127);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
}
