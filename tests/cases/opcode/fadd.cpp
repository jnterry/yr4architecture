#include "opcode_base.hpp"

TEST_CASE("fadd", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::FADD;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard add"){
		utest::getReg(0).f = 2.0f;
		utest::getReg(1).f = 5.5f;
		utest::getReg(2).f = 1.0f;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 6.5f);
		CHECK(utest::getReg(1).f == 5.5f);
		CHECK(utest::getReg(2).f == 1.0f);
	}

	SECTION("Add negative"){
		utest::getReg(0).f =  0.0f;
		utest::getReg(1).f =  8.0f;
		utest::getReg(2).f = -0.5f;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f ==  7.5f);
		CHECK(utest::getReg(1).f ==  8.0f);
		CHECK(utest::getReg(2).f == -0.5f);
	}

	SECTION("Self Add"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).f = 1.0f;
	  utest::getReg(1).f = 2.0f;
	  utest::getReg(2).f = 3.0f;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 2.0f);
		CHECK(utest::getReg(1).f == 2.0f);
		CHECK(utest::getReg(2).f == 3.0f);
	}

	/*SECTION("Add immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 8;
		utest::getReg(0).u = 1;
		utest::getReg(1).u = 2;
		utest::getReg(2).u = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 10);
		CHECK(utest::getReg(1).u ==  2);
		CHECK(utest::getReg(2).u ==  3);
	}

	SECTION("Add register with offset"){
	  utest::instructions[0].offset = 15;
		utest::getReg(0).s = 10;
		utest::getReg(1).s = 20;
		utest::getReg(2).s = 30;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 65);
		CHECK(utest::getReg(1).s == 20);
		CHECK(utest::getReg(2).s == 30);
		}*/

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
