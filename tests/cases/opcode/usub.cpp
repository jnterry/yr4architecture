#include "opcode_base.hpp"

TEST_CASE("usub", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::USUB;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard sub"){
		utest::getReg(0).u = 15;
		utest::getReg(1).u = 30;
		utest::getReg(2).u = 20;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 10);
		CHECK(utest::getReg(1).u == 30);
		CHECK(utest::getReg(2).u == 20);
	}

	SECTION("Self Sub"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).u = 4;
	  utest::getReg(1).u = 5;
	  utest::getReg(2).u = 6;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 0);
		CHECK(utest::getReg(1).u == 5);
		CHECK(utest::getReg(2).u == 6);
	}

	SECTION("Sub immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 8;
	  utest::getReg(0).u = 0;
	  utest::getReg(1).u = 9;
	  utest::getReg(2).u = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 1);
		CHECK(utest::getReg(1).u == 9);
		CHECK(utest::getReg(2).u == 3);
	}

	SECTION("Sub register with offset"){
	  utest::instructions[0].offset = 5;
	  utest::getReg(0).u = 10;
	  utest::getReg(1).u = 40;
	  utest::getReg(2).u = 15;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).u == 20);
		CHECK(utest::getReg(1).u == 40);
		CHECK(utest::getReg(2).u == 15);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
