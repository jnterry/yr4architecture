#include "opcode_base.hpp"

static_assert(Instruction::OFFSET_BITS == 10,
              "Max/min offset test cases rely on this, if changed test cases need to be updated");

TEST_CASE("fset", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::FSET;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard"){
		utest::getReg(0).f = 0;
		utest::getReg(1).f = 9841.0341d;
		utest::getReg(2).f = 7391.42343d;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 7391.42343d);
		CHECK(utest::getReg(1).f == 9841.0341d);
		CHECK(utest::getReg(2).f == 7391.42343d);
	}

	SECTION("Set with +ve offset"){
		utest::getReg(0).f = 0;
		utest::getReg(1).f = 1.23f;
		utest::getReg(2).f = 10.1f;
		utest::instructions[0].offset = 10;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 20.1f);
		CHECK(utest::getReg(1).f == 1.23f);
		CHECK(utest::getReg(2).f == 10.1f);
	}

	SECTION("Set with -ve offset"){
		utest::getReg(0).f = 0;
		utest::getReg(1).f = 1.23f;
		utest::getReg(2).f = 80.2f;
		utest::instructions[0].offset = -5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == 75.2f);
		CHECK(utest::getReg(1).f == 1.23f);
		CHECK(utest::getReg(2).f == 80.2f);
	}

	SECTION("Set with max offset"){
		utest::getReg(0).f = 0;
		utest::getReg(1).f = 4.5f;
		utest::getReg(2).f = -1234.5f;
		utest::instructions[0].offset = 511;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == -723.5f);
		CHECK(utest::getReg(1).f == 4.5f);
		CHECK(utest::getReg(2).f == -1234.5f);
	}

	SECTION("Set with min offset"){
		utest::getReg(0).f = 0;
		utest::getReg(1).f = 11.1f;
		utest::getReg(2).f = 1000.123f;
		utest::instructions[0].offset = -512;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).f == Approx(488.123));
		CHECK(utest::getReg(1).f == 11.1f);
		CHECK(utest::getReg(2).f == 1000.123f);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
