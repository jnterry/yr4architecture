#include "opcode_base.hpp"

TEST_CASE("sdiv", "[opcode]"){
	utest::initProcState();

	utest::instructions[0].opcode   = Instruction::SDIV;
	utest::instructions[0].b_is_reg = 1;
	utest::instructions[0].rdest    = Register::R00;
	utest::instructions[0].source_a = Register::R01;
	utest::instructions[0].source_b = Register::R02;

	SECTION("Standard div"){
		utest::getReg(0).s = 1;
		utest::getReg(1).s = 8;
		utest::getReg(2).s = 2;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 4);
		CHECK(utest::getReg(1).s == 8);
		CHECK(utest::getReg(2).s == 2);
	}

	SECTION("Truncation div"){
		utest::getReg(0).s = 1;
		utest::getReg(1).s = 8;
		utest::getReg(2).s = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == 2);
		CHECK(utest::getReg(1).s == 8);
		CHECK(utest::getReg(2).s == 3);
	}

	SECTION("Div by negative"){
		utest::getReg(0).s = 10;
		utest::getReg(1).s = 10;
		utest::getReg(2).s = -5;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s == -2);
		CHECK(utest::getReg(1).s == 10);
		CHECK(utest::getReg(2).s == -5);
	}

	SECTION("Self Div"){
	  utest::instructions[0].rdest    = Register::R00;
	  utest::instructions[0].source_a = Register::R00;
	  utest::instructions[0].source_b = Register::R00;
	  utest::getReg(0).s = 100;
	  utest::getReg(1).s = 10;
	  utest::getReg(2).s = 20;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  1);
		CHECK(utest::getReg(1).s == 10);
		CHECK(utest::getReg(2).s == 20);
	}

	SECTION("Div immediate"){
	  utest::instructions[0].b_is_reg  = false;
	  utest::instructions[0].immediate = 100;
	  utest::getReg(0).s = 123;
	  utest::getReg(1).s = 1000;
	  utest::getReg(2).s = 3;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==   10);
		CHECK(utest::getReg(1).s == 1000);
		CHECK(utest::getReg(2).s ==    3);
	}

	SECTION("Div register with offset"){
	  utest::instructions[0].offset = 4;
	  utest::getReg(0).s = 0;
	  utest::getReg(1).s = 15;
	  utest::getReg(2).s =  1;

		utest::runUntilHalt();

		CHECK(utest::getReg(0).s ==  3);
		CHECK(utest::getReg(1).s == 15);
		CHECK(utest::getReg(2).s ==  1);
	}

	for(int i = 3; i < Register::LAST_GP; ++i){
		CHECK(utest::getReg(i).u == 0);
	}
	CHECK(utest::getReg(Register::PC).u == 8);
}
