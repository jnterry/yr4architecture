#include <catch.hpp>
#include <parser.cpp>
#include <xen/core/memory/utilities.hpp>

TEST_CASE("Parse Constant", "[parser]"){
	Constant con;

	REQUIRE(parseConstant(0, "0", &con) == true);
	REQUIRE(con.negative  == false);
	REQUIRE(con.floating  == false);
	REQUIRE(con.value_int == 0);

	REQUIRE(parseConstant(0, "123", &con) == true);
	REQUIRE(con.negative  == false);
	REQUIRE(con.floating  == false);
	REQUIRE(con.value_int == 123);


	REQUIRE(parseConstant(0, "-0b1010", &con) == true);
	REQUIRE(con.negative  == true);
	REQUIRE(con.floating  == false);
	REQUIRE(con.value_int == 10);

	REQUIRE(parseConstant(0, "0xabcdef", &con) == true);
	REQUIRE(con.negative  == false);
	REQUIRE(con.floating  == false);
	REQUIRE(con.value_int == 0xabcdef);

	REQUIRE(parseConstant(0, "0xfF", &con) == true);
	REQUIRE(con.negative  == false);
	REQUIRE(con.floating  == false);
	REQUIRE(con.value_int == 0xfF);

	REQUIRE(parseConstant(0, "0.123", &con) == true);
	REQUIRE(con.negative    == false);
	REQUIRE(con.floating    == true);
	REQUIRE(con.value_float == Approx(0.123));

	REQUIRE(parseConstant(0, "-.98", &con) == true);
	REQUIRE(con.negative    == true);
	REQUIRE(con.floating    == true);
	REQUIRE(con.value_float == Approx(0.98));

	REQUIRE(parseConstant(0, "-1980.621", &con) == true);
	REQUIRE(con.negative    == true);
	REQUIRE(con.floating    == true);
	REQUIRE(con.value_float == Approx(1980.621));

	REQUIRE(parseConstant(0, "", &con) == false);
	REQUIRE(parseConstant(0, "a", &con) == false);
	REQUIRE(parseConstant(0, "+", &con) == false);
	REQUIRE(parseConstant(0, "-", &con) == false);
	REQUIRE(parseConstant(0, " ", &con) == false);
	REQUIRE(parseConstant(0, " 1234", &con) == false);
	REQUIRE(parseConstant(0, "123.", &con) == false);
	REQUIRE(parseConstant(0, "0x", &con) == false);
	REQUIRE(parseConstant(0, "0b", &con) == false);
}


TEST_CASE("Parse Register Offset", "[parser]"){
	const char* tokens[] = {
		"[",   // 0
		"r10", // 1
		"+",   // 2
		"0",   // 3
		"]"    // 4
	};

	AsmEntry entry;
	xen::clearToZero(&entry);

	SECTION("Basic"){
		xen::clearToZero(&entry);
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == true);
		REQUIRE(entry.instruction.value_register == Register::R10);
		REQUIRE(entry.instruction.value_offset   == 0);
	}

	SECTION("Positive offset"){
		tokens[3] = "10";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == true);
		REQUIRE(entry.instruction.value_register == Register::R10);
		REQUIRE(entry.instruction.value_offset   == 10);
	}

	SECTION("Negative offset"){
		tokens[2] = "-";
		tokens[3] = "123";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == true);
		REQUIRE(entry.instruction.value_register == Register::R10);
		REQUIRE(entry.instruction.value_offset   == -123);
	}

	SECTION("Bad opener"){
		tokens[0] = "@";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == false);
	}

	SECTION("Bad register name"){
		tokens[1] = "hello";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == false);
	}

	SECTION("Bad offset type"){
		tokens[2] = "#";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == false);
	}

	SECTION("Bad offset value"){
		tokens[3] = "abc";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == false);
	}

	SECTION("Bad closer"){
		tokens[4] = "?";
		REQUIRE(parseRegisterOffset(0, &entry, tokens) == false);
	}
}
